classdef ErrorMeasurement
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	ErrorMeasurement
%   
% A collection of error measuring functions.
%   There are many ways to measure the performance of a predictor.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
    
    properties
    end
    
    methods
        %crossValidation
        %function [error, predictorStore] = crossValidate(~,predictor,predictorStore,data, trainingSet)
            % TODO: As we should search for appropriate cross validator
            % predictors in the predictor store we postpone to implement
            % this.           
        %end
                
        function [error,errorPerDim] = testValidate(~, predictorNew, predictorStore,data, trainingSet, testSet)
            %Validation with separated test set
            %   We calculate mean error on a seperated test set which is
            %   not and will not be used in the training.
            
            [predictor] = getPredictorMacro(predictorNew, data, predictorStore, trainingSet);      
            
            [testInputs, testTargets] = data.getLabels(testSet);
            testOutputs = predictor{1}.predict(testInputs);
            
            [error,errorPerDim] = distME(testOutputs, testTargets);
        end
                
        function [totalCov] = totalCovarianceFromPredictor(~, predictorNew, predictorStore, data,trainingSet)
            %If we have a predictor which is capable to predict variances than
            %we can use the overall predicted variance as a validation value
            %for our model. The uncertainty evaluated on the FULL dataset.
            [predictor] = getPredictorMacro(predictorNew, data, predictorStore, trainingSet);
            allInputs = data.getData();
            [~,var] = predictor{1}.predict(allInputs);
            totalCov = trace(var);                        
            
        end
                
        function [totalCov] = calculateOverAllUncertainty(~, predictorNew, predictorStore, data,trainingSet, possibleNewInputs,testSet)
            %Measure error by overall uncertainty. The uncertainty
            %calculation can be evaluated on 2 different subset of our
            %data. If the last parameter (testSet) is given then we
            %evaluate on the fixed test set the uncertainty 
            %This function is used for 2 reasons:
            %   1. Calculate overall uncertainty as an error measurement. This
            %   time we only want to know what is the current uncertainty with
            %   the given training set.
            %   2. We want to estimate the overall uncertainty shrink IF we add
            %   a new object to the training set. For the overall uncertainty
            %   calculation we determine covariance matrices but with block
            %   matrix inverse calculation we can save a lot of work.
            % The function uses GPML book's 2.19 equation. please note, that the
            % notation is a bit different here and in the book, as the KXX
            % refers to the constant covariance matrix entries.
            %       INPUTS:
            %           - predictorNew: we get a predictor which can be trained        
            %           - predictorStore: previously trained predictors easiest
            %           access with getPredictorMacro function
            %           - data: the data object with which we actually work
            %           - trainingSet: the training set which is constant,
            %           stable and does not change in this function. (this is
            %           the baseline to which we'll add one-by-one the
            %           possiblyNewInputs elements)
            %           - possibleNewInputs: a vector with all the inputs which
            %           will be added to the training set and the overall
            %           uncertainty will be evaluated.
            %           - testSet: the indices for objects on which we
            %           should evaluate OUC.
            %       OUTPUTS:
            %           - totalCov: 
            %           a) one value, if the possibleNewInputs is an empty
            %           array, the overall uncertainty on the training set.
            %           b) a vector equal long to possibleNewInputs and
            %           containing the overalluncertainties corresponding to
            %           that                        
            
            %we can use this function only with gpml predictor.
            if (isa(predictorNew,'GPMLPredictor'))
            
                %myepsilon is a little value to regularize matrices in this
                %function. (theoretically it is connected to noise.)
                myepsilon = 1e-8;
                [readyPredictor] = getPredictorMacro( predictorNew, data, predictorStore, trainingSet);

                %initialize the OAU (overAllUncertainty)
                if isempty(possibleNewInputs)
                   %we will have OAUs for every output dimension which is the
                   %size of the hyperparameters cell array.
                   OAU = zeros(length(readyPredictor{1}.model.hyp),1); 
                else
                   OAU = zeros(length(readyPredictor{1}.model.hyp),length(possibleNewInputs)); 
                end
                %initialize (allocate memory) for covariance matrices
                KAll = cell(length(readyPredictor{1}.model.hyp),1);
                KXX = cell(length(readyPredictor{1}.model.hyp),1);
                inverseOfKXX = cell(length(readyPredictor{1}.model.hyp),1);

                %get the data to local variables for easier handling
                trainingInputs = data.getData(trainingSet);
                trainingSetSize = length(trainingSet);
                unknownIndeces = setdiff(1:data.sizeOfLabelingPool,trainingSet);
                unknownInputs = data.getData(unknownIndeces);            
                allInputs = [trainingInputs unknownInputs];

                %we can evaluate on 2 different types of data: the full
                %remaining set, and a required test set. What we know for sure
                %that we don't want to evaluate on any training set element.
                if (nargin<7) %there is no provided test set
                    evaluateIndices = (trainingSetSize+1):size(allInputs,2);
                else % if there is test set
                    evaluateIndices = zeros(1,length(testSet));
                    for i=1:length(testSet)
                        evaluateIndices(i) = find(unknownIndeces==testSet(i));
                    end
                    evaluateIndices = evaluateIndices + trainingSetSize;
                end
                %evaluateIndices are within allInputs.


                %and for every output dimension    
                for j=1:length(readyPredictor{1}.model.hyp)
                    %calculate the overallUncertainty
                    KAll{j} = feval(readyPredictor{1}.model.covfunc,readyPredictor{1}.model.hyp{j}.cov,allInputs');
                    KXX{j} = KAll{j}(1:trainingSetSize,1:trainingSetSize);
                    inverseOfKXX{j} = inv(KXX{j}+myepsilon*eye(size(KXX{j},1)));
                    if isempty(possibleNewInputs)
                        KXSX = KAll{j}(evaluateIndices,1:trainingSetSize);
                        KXXS = KAll{j}(1:trainingSetSize,evaluateIndices);
                        KXSXS = KAll{j}(evaluateIndices,evaluateIndices);                    
                        szigma = KXSXS - KXSX*inverseOfKXX{j}*KXXS;
                        OAU(j,1) = trace(szigma);
                    else
                        for i=1:length(possibleNewInputs)
                            %fprintf('%d ',i);
                            %get out the index in the all input matrices for
                            %the currently examined possible input.
                            %indexInAll could be called
                            %indexOfCurrentPossibleInputInAllInput too, but
                            %it's too long.
                            [indexInAll] = find(unknownIndeces==possibleNewInputs(i))+trainingSetSize;

                            %we will evaluate on the indices except the chosen
                            %new cell.
                            evaluateIndicesLocal = setdiff(evaluateIndices,indexInAll);

                            %extract the appropriate matrices from KAll.
                            KXSX = KAll{j}(evaluateIndicesLocal,[1:trainingSetSize indexInAll]); %#ok<PFBNS>
                            KXXS = KAll{j}([1:trainingSetSize indexInAll],evaluateIndicesLocal);
                            KXSXS = KAll{j}(evaluateIndicesLocal,evaluateIndicesLocal);
                            %build up the matrices for block inverse
                            %calculation.
                            %covariance Training - New
                            KTN = KAll{j}(1:trainingSetSize,indexInAll);
                            KNT = KAll{j}(indexInAll,1:trainingSetSize);
                            KNN = KAll{j}(indexInAll,indexInAll)+myepsilon;                        
                            inverseForCurrentPossibleKXX = matrixBlockInvertation(inverseOfKXX{j},KTN,KNT,KNN,1); %#ok<PFBNS>
                            szigma = KXSXS - KXSX*inverseForCurrentPossibleKXX*KXXS; 
                            OAU(j,i) = trace(szigma);
                        end
                    end
                end                                             
                %add up the uncertainties and this will be the entry
                %(the rank) for this new training set.
                totalCov = sum(OAU);
            else
                errordlg('The calculateOverAllUncertainty can be used only with GPMLPredictor!');
            end
        end
                        
    end
    
end

