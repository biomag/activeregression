classdef SIMCEPLabeling < Labeling
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	SIMCEPLabeling
%   
% Implementation for the labeling class which is
% used for testing our methods. It is very similar to Artificial Cell
% labeling as in fact it is a synthetic data class.
%   A minor difference can be noticed that we use directly the
%   calculated features and targets stored in SIMCEPData instead of
%   calling the calcFeatues function of that class.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    properties        
        cellData        
    end
    
    methods
        function obj = SIMCEPLabeling(cellData)
            obj.cellData = cellData;            
        end        
    end
    
    methods
        % Be careful this is a copied code from ArtificialCellsLabeling.
        % Only tested with mode 3.
        % OUPUTS:
        %   target must be in the format described in Predictor.
        function target = label(obj,actualCell,data,~,img)
            % The missing input parameter is inp, which is the input
            % features for the object.
            % We label the actualCell. cell in the CommonHandles. 
            %   Mode specifies the label method:
            %    - asking from the user command line   1) with picture
            %                                          2) without picture
            %    - filling the label with the ground truth.          

            mode = 3;   
            switch mode
                case 1
                    image(img)
                    fprintf('Please give the label(s) of the %d.',actualCell);
                    target = input(' cell:\n');
                case 2
                    fprintf('Please give the label(s) of the %d.',actualCell);
                    target = input(' cell:\n');
                case 3
                    target = obj.cellData.targets(:,actualCell);                    
                case 4
                    %get data from the data class to draw out the current(!) state of
                    %the regression plane.           
                    try
                        [~, targets] = data.getLabels();
                    catch                 
                    end                  

                    %drawing the border of the regPlane
                    plot([-100, 100, 100, -100, -100] , [100, 100, -100, -100, 100], 'k');
                    hold on;
                    if data.realLabels ~=0                
                        data.plotDataToRegressionPlane(data.realLabels,targets);
                    end
                 
                    plot([-200, -200, -100, -100, -200],[-50, 50, 50, -50 -50],'k');
                    data.plotDataToRegressionPlane(actualCell,[-150; 0]);
                    target = ginput(1);
                    hold off
                    target = convert2RowVector(target);
            end
        end
               
    end
end

