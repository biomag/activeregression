classdef CommonHandlesLabeling < Labeling
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	CommonHandlesLabeling
%   
% Implementation for the labeling class which is
% sused for testing our methods.
%   This implementation uses to label the CommonHandles.Regression
%   field in which the known labels are stored.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.        
    
    properties
        % Basic CommonHandles data.
        CommonHandles;
        classIdx;
        %improve performance by storing the indices of this class
        originalIndices;
    end
    
    methods
        function obj = CommonHandlesLabeling(CommonHandles, classIdx)
            obj.CommonHandles = CommonHandles;
            obj.classIdx = classIdx;
            obj.originalIndices = find(cellfun(@find,CommonHandles.TrainingSet.Class) == classIdx);
        end        
    end
    
    methods
        % At the moment we only imitating the labeling, the label process
        % was done before the AL testing started. That's why label function
        % uses the already labeled cells. 
        % This should be implemented differently in real cases!
        % OUPUTS:
        %   target must be in the format described in Predictor.
        function target = label(obj,actualCell,~,~,img)
            % The missing input parameter is inp, which are the input
            % features for the object.
            % We label the actualCell. cell in the CommonHandles. 
            %   Mode specifies the label method:
            %    - asking from the user command line   1) with picture
            %                                          2) without picture
            %    - filling the label from the ImagePosition, stored previously in CH.
            %    - filling it from new typed CommonHandles

            mode = 4;
            switch mode
                case 1
                    image(img)
                    fprintf('Please give the label(s) of the %d.',actualCell);
                    target = input(' cell:\n');
                case 2
                    fprintf('Please give the label(s) of the %d.',actualCell);
                    target = input(' cell:\n');
                case 3 % old CommonHandlesData
                    target = obj.CommonHandles.Regression{obj.classIdx}.ImagePosition{actualCell};
                    target=target';
                case 4
                    target = obj.CommonHandles.TrainingSet.RegPos{obj.originalIndices(actualCell)}';
            end
        end
    end
end

