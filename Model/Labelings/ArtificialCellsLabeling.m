classdef ArtificialCellsLabeling < Labeling
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	ArtificialCellsLabeling
%   
% Implementation for the labeling class which is
% used for testing our methods.
%   This implementation uses to label the cellData field of an
%   appropriate ArtificialCellsData class.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
    
    properties        
        cellData        
    end
    
    methods
        function obj = ArtificialCellsLabeling(cellData)
            obj.cellData = cellData;            
        end        
    end
    
    methods
        % At the moment we only imitating the labeling, we do not need true labeling as it is artificial data        
        % OUPUTS:
        %   target must be in the format described in Predictor.
        function target = label(obj,actualCell,data,~,img)
            % The missing input parameter is inp, which is the input
            % features for the object.
            % We label the actualCell. cell in the CommonHandles. 
            %   Mode specifies the label method:
            %    - asking from the user command line   1) with picture
            %                                          2) without picture
            %    - filling the label with the ground truth.          

            mode = 3;   
            switch mode
                case 1
                    image(img)
                    fprintf('Please give the label(s) of the %d.',actualCell);
                    target = input(' cell:\n');
                case 2
                    fprintf('Please give the label(s) of the %d.',actualCell);
                    target = input(' cell:\n');
                case 3
                    [~,target] = obj.cellData.calcFeatures(actualCell);                    
                case 4
                    %get data from the data class to draw out the current state of
                    %the regression plane.           
                    try
                        [~, targets] = data.getLabels();
                    catch             
                    end                  

                    %drawing the border of the regPlane
                    plot([-100, 100, 100, -100, -100] , [100, 100, -100, -100, 100], 'k');
                    hold on;
                    if data.realLabels ~=0                
                        data.plotDataToRegressionPlane(data.realLabels,targets);
                    end
                 
                    plot([-200, -200, -100, -100, -200],[-50, 50, 50, -50 -50],'k');
                    data.plotDataToRegressionPlane(actualCell,[-150; 0]);
                    target = ginput(1);
                    hold off
                    target = convert2RowVector(target);
            end
        end
               
    end
end

