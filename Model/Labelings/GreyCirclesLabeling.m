classdef GreyCirclesLabeling < Labeling
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	GreyCirclesLabeling
%   
% Implementation for the labeling class which is
% used for testing our methods.     
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.    
    
    properties                
    end        
    
    methods        
        % OUPUTS:
        %   target must be in the format described in Predictor.
        function target = label(~,actualCell,data,~,~)
            % The missing input parameter is inp, which is the input
            % features for the object.
            % We label the actualCell.           
            
            %Possible modes for labeling:
            %   1: label manually
            %   2: label by the features (the 2 targets are intensity and
            %   radius)
            
            mode = 1;
            
            switch mode
                
                case 1

                    %get data from the data class to draw out the current state of
                    %the regression plane.           
                    try
                        [~, targets] = data.getLabels();
                    catch e                
                    end


                    %drawing the border of the regPlane
                    plot([-100, 100, 100, -100, -100] , [100, 100, -100, -100, 100], 'k');
                    hold on;
                    if data.realLabels ~=0                
                        data.plotDataToRegressionPlane(data.realLabels,targets);
                    end

                    plot([-200, -200, -100, -100, -200],[-50, 50, 50, -50 -50],'k');
                    data.plotDataToRegressionPlane(actualCell,[-150; 0]);
                    target = ginput(1);
                    hold off
                    target = convert2RowVector(target);
                case 2
                    target = data.getData(actualCell);
                    target = convert2RowVector(target);
            end
        end
    end
end

