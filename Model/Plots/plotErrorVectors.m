function plotErrorVectors( outputALL,data, trainingSet, testSet, regPlaneBoundaries)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	plotErrorVectors
%   
% Provided all the outputs it draws out the current
% known errors. The known error is the difference between provided ground
% truth positions and predictions. Usually we know the ground truth for
% an object if it's in the training or in the test set. The criteria is that
% all the indices in training and test set must have its target stored in data.
%
% INPUTS: 
%   outputALL        a big matrix with predictions.
%   data             the data object with the inputs and targets
%   trainingSet      indices considered to be in training set. They'll be
%                    plot in orange.
%   testSet          as above from test set. They'll be pink.
%   regPlaneBoundaries the bounds for the regression plane
%
%   Constraints: the data.sizeOfLabelingPool must match the outputALL's 2nd
%   dimension.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    plot([regPlaneBoundaries(1,1) regPlaneBoundaries(1,1)  regPlaneBoundaries(1,2)  regPlaneBoundaries(1,2)  regPlaneBoundaries(1,1)],[ regPlaneBoundaries(2,1)  regPlaneBoundaries(2,2)  regPlaneBoundaries(2,2)  regPlaneBoundaries(2,1)  regPlaneBoundaries(2,1)],'k--');
    hold on;
    %for all the cells
    for i=1:data.sizeOfLabelingPool
        %if we're in the regplane                    
        if (ismember(i,trainingSet))
            plot(outputALL(1,i),outputALL(2,i),'*','Color',[1, 69/255, 0]);
            [~,gt] = data.getLabels(i);
            plot(gt(1),gt(2),'g*');
            plot([outputALL(1,i) gt(1)],[outputALL(2,i) gt(2)],'b');
        elseif (ismember(i,testSet))
            plot(outputALL(1,i),outputALL(2,i),'*','Color',[1, 69/255, 1]);
            [~,gt] = data.getLabels(i);
            plot(gt(1),gt(2),'g*');
            plot([outputALL(1,i) gt(1)],[outputALL(2,i) gt(2)],'k');                    
        end                    
    end

end

