function plotRegressionPlane( ALLimg,outputALL, regPlaneBoundaries)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	plotRegressionPlane
%   
% plotRegressionPlane draws out the current state of the regression plane.
% Only the regression plane is displayed objects predicted out of bounds
% cannot be found here.
%
%   INPUTS: 
%     ALLimg            cellarray containing the images for the objects.
%     outputALL         an array containing the predicted positions for the objects.
%     regPlaneBoundaries an array with the bound values for the
%                       regression plane.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    %the regression plane with additional 50 pixel border. The
    %50 pixel border is used to handle cases when the picture
    %is close to the borders (we don't have to bother with the
    %index out of range exceptions because there is always
    %enough space to put in the image). 
    regPlaneSize = 1000;
    %if half of the image can be larger than 50 then this
    %should be modified not to get errors.
    regPlaneBorder = 50;
    %initialize regPlane
    regPlane = uint8(zeros(regPlaneSize+regPlaneBorder*2,regPlaneSize+regPlaneBorder*2,3));

    for i=1:length(ALLimg)                    
        %check for images
        if ~(outputALL(2,i)>regPlaneBoundaries(2,2) || outputALL(2,i)<regPlaneBoundaries(2,1) || outputALL(1,i)<regPlaneBoundaries(1,1) || outputALL(1,i)>regPlaneBoundaries(1,2))
            regPlane = mergeMatricesByCenter(regPlane,ALLimg{i},[regPlaneSize-round( ((outputALL(2,i))-regPlaneBoundaries(2,1))./(regPlaneBoundaries(2,2)-regPlaneBoundaries(2,1)).*regPlaneSize )+regPlaneBorder,round( ((outputALL(1,i))-regPlaneBoundaries(1,1))./(regPlaneBoundaries(1,2)-regPlaneBoundaries(1,1)).*regPlaneSize )+regPlaneBorder ]);
        end
    end
    regPlane = regPlane(regPlaneBorder+1:regPlaneSize+regPlaneBorder,regPlaneBorder+1:regPlaneSize+regPlaneBorder,:);
    imshow(regPlane);

end

