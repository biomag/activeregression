function plotVarianceForObjects( d, selectedDim,nofDim ,varianceMax,trainingSet,testSet)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	plotVarianceForObjects
%   
% plotVarianceForObjects We plot the variance for each object. It is
% possible to plot the variance among the 1st and 2nd dimension.
%   The function plots the variance for objects. On X you can find the
%   index on Y you can see the variance.
%   INPUTS:
%       d: d is a vector containing the variance values for all dimension.
%       If you have m dimension and n objects then it is n*m long and the
%       first n element refers to the 1st dimension 2nd n elements to the
%       2nd etc.
%       selectedDim: dimension selection indicating which n elements you wish to
%       select to plot. This should be between 1 and m if you have m
%       dimension data.
%       nofDim: the number of dimensions (previously noted as m)
%       varianceMax: this is a nofDim long array indicating the maximum
%       variance for each dimension which ever occured.
%       trainingSet: array of indices of objects from training set
%       testSet: --"-- from test set   
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
        
    %i goes through the indices of our data
    for i=1:length(d)/nofDim
        currentElementVariance = d((length(d)/nofDim)*(selectedDim-1)+i);
        if (ismember(i,trainingSet))
            plot(i,currentElementVariance,'g*');
            hold on;
        elseif ismember(i,testSet)
            plot(i,currentElementVariance,'b*');
            hold on;
        else
            plot(i,currentElementVariance,'r*');
            hold on;
        end
    end
    axis([1 length(d)/2, 0, varianceMax(selectedDim)+0.2]);    
    hold off;   

end

