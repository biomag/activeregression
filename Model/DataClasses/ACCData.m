classdef ACCData < Data
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	ACCData
%   
% ACCData This data class is connected to Advanced Cell Classifier ACC,
% and designed especially to that enviroment. There are some major
% differences between the real usage of the Active Learning and the test
% usage so that this data class has also some major differences. This
% data class has very short life-time as the sampling pool can change
% very fast. There won't be corresponding labeling class for this data
% as the labeling is implemented in the ACC.  
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
        
    properties                                
        % stores the targets for our cells. If there are multiple targets,
        % then they are stored as columns. These targets are regression
        % targets and not the classes. Column are observations and it has
        % only as many elements as obj.sizeOfLabelingPool. 
        targets
        
        % mapping between the data and the sampling pool. Our data cannot
        % contain the full sampling pool, as the regression is done only in
        % specified class. (according to the hierarchical combination of
        % classification and regression) This mapping is a large array with
        % as many elements as the sizeOfLabelingPool. The ith element
        % mapping(i) of this array says where the data can be found in
        % the samplingPool.
        mapping
        
        % The data class stores for itself the CommonHandles in the state
        % when the class object is created. This where from all the class
        % methods reads out the neccessary data.
        CommonHandles
        
        % An extension field to realLabels. This field stores where the
        % training cell is located within the training set.
        indicesInTS
                
    end
    
    methods        
        function obj = ACCData(CommonHandles,selectedClass,outs)
        %Constructor. We must build up in the constructor the right data
        %class from the samplingPool. 
        %   Inputs:
        %       CommonHandles: from this we can extract the sampling pool
        %       (AllFeatures).
        %       selectedClass: we need to know on which class we want to do
        %       regression and select only those cells from the sampling
        %       pool and make the mapping.
        %       outs         : the already done classification predictions
        %       on all member of AllFeatures
            obj.bounds = '[0 1000; 0 1000]';
            %k will count elements in the sampling pool
            k = 1;
            % l will count the elements in the training set
            l = 1;
            obj.CommonHandles = CommonHandles;            
            obj.sizeOfLabelingPool = numel(outs(outs==selectedClass));
            obj.targets = zeros(2,obj.sizeOfLabelingPool);
            
            %go through the labeling pool
            for i = convert2RowVector(find(outs==selectedClass))
                % if the cell is predicted to the selectedClass
                obj.mapping(k) = i;                    
                % if it is part of the training set
                idxInTS = isInTrainingSet( CommonHandles.AllFeaturesMapping{i}.ImageName, CommonHandles.AllFeaturesMapping{i}.CellNumberInImage, 1, CommonHandles);
                if idxInTS && find(CommonHandles.TrainingSet.Class{idxInTS})==selectedClass
                    %store the index as element of the training set (the regressor will be taught on this)
                    obj.realLabels(l) = k;
                    obj.indicesInTS(l) = idxInTS;
                    l = l+1;                                                

                    obj.targets(:,k) = CommonHandles.TrainingSet.RegPos{idxInTS}';
                end
                k = k+1;                      
            end      
            %synchronize training set and sampling pool (add TS elements to sampling pool)
            for i=1:length(CommonHandles.TrainingSet.Class)
                if (find(CommonHandles.TrainingSet.Class{i})==selectedClass) && ~ismember(i,obj.indicesInTS)
                    obj.sizeOfLabelingPool = obj.sizeOfLabelingPool + 1;
                    obj.realLabels(l) = obj.sizeOfLabelingPool;
                    obj.indicesInTS(l) = i;                    
                    obj.CommonHandles.AllFeatures(end+1,:) = CommonHandles.TrainingSet.Features{i};
                    obj.CommonHandles.AllFeaturesMapping{end+1}.ImageName = CommonHandles.TrainingSet.ImageName{i};
                    obj.CommonHandles.AllFeaturesMapping{end}.CellNumberInImage = CommonHandles.TrainingSet.CellNumber{i};                    
                    classImgObj = getClassImgObjByTrainingSetID(CommonHandles,i);
                    obj.CommonHandles.AllFeaturesMapping{end}.CurrentPlateNumber = classImgObj.PlateName;
                    obj.mapping(k) = length(obj.CommonHandles.AllFeaturesMapping);
                    obj.targets(:,k) = CommonHandles.TrainingSet.RegPos{i}';
                    l=l+1;
                    k=k+1;
                end
            end
        end
                
        function obj = label(obj,~, ~)
        %ACC data cannot be labeled, because the data comes from the big
        %program and not the Active Controller controls the timing of the
        %data income
        end
               
        %Getting labels
        function [inputs, targets] = getLabels(obj, specialization)            
            if nargin>1
                if isequal(sort(convert2RowVector(intersect(specialization,obj.realLabels))),sort(convert2RowVector(specialization)))
                    selectedCells = convert2ColumnVector(specialization);                
                else
                    baseException = MException('MYEXC:nolabeledRequsted','Sorry not all the requested cells are labeled. Please check realLabels property.');
                    throw(baseException);   
                end
            elseif ~isequal(obj.realLabels,[])
                selectedCells = convert2ColumnVector(obj.realLabels);
            else
                baseException = MException('MYEXC:nolabeledany','Sorry there is no any labeled cells yet, so we cannot provide any labels.');
                throw(baseException);
            end
                           
            inputs = obj.CommonHandles.AllFeatures(obj.mapping(selectedCells),:)';
            targets = obj.targets(:,selectedCells);
             
        end
                
        %getData
        % we do not provide images.
        function [input, img] = getData(obj,specialization)
            if nargin>1
                specialization = convert2ColumnVector(specialization);
            else
                specialization = convert2ColumnVector(1:obj.sizeOfLabelingPool);
            end
            
            input = obj.CommonHandles.AllFeatures(obj.mapping(specialization),:)';
            img = [];
                        
        end
                
        function [imageName,cellNumber,plateNumber] = map2ACC(obj,index)
        %special function which maps back the data to the ACC's unique
        %identifier the image name and the cell number.
        %Input: an index in the data
        %Output: the corresponding ID-pair in ACC
            le = length(index);
            cellNumber = zeros(1,le);
            plateNumber = zeros(1,le);
            imageName = cell(1,le);
            for i=1:le
                imageName{i} = obj.CommonHandles.AllFeaturesMapping{obj.mapping(index(i))}.ImageName;
                cellNumber(i) = obj.CommonHandles.AllFeaturesMapping{obj.mapping(index(i))}.CellNumberInImage;
                plateNumber(i) = obj.CommonHandles.AllFeaturesMapping{obj.mapping(index(i))}.CurrentPlateNumber;
            end
        end
        
        
    end
    
end

