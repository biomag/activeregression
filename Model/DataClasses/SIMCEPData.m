classdef SIMCEPData < Data
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	SIMCEPData
%   
% SIMCEP data is data class for artificial data generated from SIMCEP.
% The simcep outputs binary and real-like images and also some features
% but not many. The output features are ground truth parameters for the
% generated cells such as the area of the cell number of subcellular
% particles etc. These can be used as targets in this artificial data
% class and also features.
% If the number of these features is not enough for us we can put the
% the output images into a cell analyzer program (e.g. CellProfiler),
% which can produce us many-many input features. See constructor
% function.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.        
    
    properties                              
        %Input: the input features for the cells in our data set.
        %this is a huge matrix the columns refers to observations. See
        %Predictor class' train and predict function for more doc.
        inputs
        
        %target the targets in the same format
        targets
        
        %origin: the origin is a cellarray which helps to retrieve the
        %origin of our objects (the origin means on which picture we can
        %find the object and on which is it's number on that picture so
        %that one element of this cellarray contains a structure. The
        %structure has 2 fields image and position. The position is also an
        %array with the 2 coordinates on the image (coordinates in matrix
        %indexing)
        origin
        
        %simcepFeatures for later use we store the full simcep output.
        simcepFeatures
        
        %We should store images of our objects to easily visualize them.
        %This is a cellarray with obj.sizeOfLabelingPool size and every
        %cell contains one image.
        images
                
        
    end
    
    methods        
        function obj = SIMCEPData(inputFeatures,groundTruthFeatures,bounds)
        %Constructor function
        %We initialize and fill out the input and target members of this
        %class so the data can be read out from there afterwards.
        %It might happen that we need additional input features to the
        %SIMCEP output ones so we have 2 different parameter for the input
        %features and the ground-truth features. The input features must be
        %in the format required in the input field of this class, the
        %ground truth features in the format given back by the SIMCEP
        %function's 3rd output parameter.
        %
        %   The inputFeatures must have 3 additional rows (the 1-3 row) to
        %   the really used ones.
        %   1st: the number of image from which the cell is coming
        %   2-3rd: the coordinates for the cell.
        %   
        %   These additional features are used to rebuild the connection
        %   between the ground truth and the inputFeatures
        %
        %   bounds is the theoretical bounds, this a string which describe
        %   a matrix in MATLAB syntax.
        
            %bounds set
            obj.bounds = bounds;
            
            %store simcep features
            obj.simcepFeatures = groundTruthFeatures;
            
            %SET here the output dimension of the regression. e.g.
            %Regresion plane is 2, regression line is 1.
            outDim = 2;                        
            
            
            %Used for A and B type data
            obj.inputs = zeros(size(inputFeatures,1),size(inputFeatures,2));            
            %E
            %obj.inputs = zeros(size(inputFeatures,1)+3,size(inputFeatures,2));
            %C
            %obj.inputs = zeros(size(inputFeatures,1)+1,size(inputFeatures,2));
            %F
            %obj.inputs = zeros(5,size(inputFeatures,2));
            %Used for G
            %obj.inputs = zeros(size(inputFeatures,1)+4,size(inputFeatures,2));
            %H
            %obj.inputs = zeros(6,size(inputFeatures,2));
            
            obj.targets = zeros(outDim,size(inputFeatures,2));
            obj.origin = cell(1,size(inputFeatures,2));
            
            %for every object 
            for i=1:size(inputFeatures,2)
                %fill in the original image
                indexOfPicture = inputFeatures(1,i);                               
                obj.origin{i}.image = indexOfPicture;
                %fill in the original position
                pos = inputFeatures(2:3,i);                
                obj.origin{i}.position = pos;
                %find the connection between ground truth and inputs
                indexOfCellInGroundTruth = obj.findClosestOne(groundTruthFeatures{indexOfPicture},pos);
                %fill in all the other input fields
                [obj.inputs(:,i),obj.targets(:,i)] = obj.calcFeatures(groundTruthFeatures{indexOfPicture},indexOfCellInGroundTruth,inputFeatures(:,i));
            end
            %obj.inputs(end+1,:) = obj.targets(2,:);
            obj.sizeOfLabelingPool = size(inputFeatures,2);
            
            %initialize images:
            %'Please give the locations for the images (e.g. anal3)'
            %this is burnt in, but you can change it to ask for folder by
            %changing the comments. Also be careful with the image
            %extension while listing.
            picturesFolder = uigetdir(pwd,'Specify a data path for the images of this data. (anal3 usually)');
            %picturesFolder = 'D:\�bel\SZBK\MatLab\SyntheticData\SIMCEP\ResultImages\LipidDroplets\anal3';%'D:\szkabel\SIMCEP\ResultImages\anal3';
            list = ls([picturesFolder filesep '*.png']);
            %read in all images to be able to cut out the parts with the
            %cells.
            img = cell(1,size(list,1));
            for i=1:size(list,1)
                img{i} = imread([picturesFolder filesep list(i,:)]);
            end
            
            obj.images = cell(1,length(obj.origin));
            
            for i=1:length(obj.origin)
                size1 = 60;
                if 1>floor(obj.origin{i}.position(1)-30)
                    sp1 = 1;
                    size1 = size1 + floor(obj.origin{i}.position(1)-30);
                else
                    sp1 = floor(obj.origin{i}.position(1)-30);
                end
                if sp1 + size1 > size(img{obj.origin{i}.image},1)
                    size1 = size(img{obj.origin{i}.image},1) - sp1;
                end
                size2 = 60;
                if 1>floor(obj.origin{i}.position(2)-30)
                    sp2 = 1;
                    size2 = size2 + floor(obj.origin{i}.position(2)-30);
                else
                    sp2 = floor(obj.origin{i}.position(2)-30);
                end
                if sp2 + size2 > size(img{obj.origin{i}.image},2)
                    size2 = size(img{obj.origin{i}.image},2) - sp2;
                end

                obj.images{i} = img{obj.origin{i}.image}(sp1:sp1+size1-1,sp2:sp2+size2-1,:);
            end

        end
                               
        function obj = label(obj,index,targets)            
            obj.targets(:,index) = targets;
            if (~ismember(index,obj.realLabels))
                obj.realLabels = [obj.realLabels, index];  
            end
        end
        
        function [input, target] = getLabels(obj,specialization)
            if nargin>1
                if isequal(sort(convert2RowVector(intersect(specialization,obj.realLabels))),sort(convert2RowVector(specialization)))
                    selectedCells = convert2ColumnVector(specialization);                
                else
                    baseException = MException('MYEXC:nolabeledRequsted','Sorry not all the requested cells are labeled. Please check realLabels property.');
                    throw(baseException);   
                end
            elseif ~isequal(obj.realLabels,[])
                selectedCells = convert2ColumnVector(obj.realLabels);
            else
                baseException = MException('MYEXC:nolabeledany','Sorry there is no any labeled cells yet, so we cannot provide any labels.');
                throw(baseException);
            end
            
            input = obj.inputs(4:end,selectedCells);
            target = obj.targets(:,selectedCells);
        end
        
        function [input, img] = getData(obj, specialization)
            if nargin>1
                specialization = convert2ColumnVector(specialization);
            else
                specialization = convert2ColumnVector(1:obj.sizeOfLabelingPool);
            end
            
            input = obj.inputs(4:end,specialization);
            
            %we give back the images
            img = cell(1,length(specialization));
            for i=1:length(specialization)
                img{i} = obj.images{i};
            end
            
        end
                        
        function index = findClosestOne(~,struct,positions)
        %local util function to find the best match for the position
        %the struct is the SIMCEP feature result struct for one picture the
        %positions is a vector with 2 elements the targeted positions
        %matrix-wise (meaning we're indexing by the standard matrix
        %indexing)
        %NOTES: 
        %   1) the cellProfiler uses the matrix indexing with flipped
        %   order and the SIMCEP uses the standard one. The flipping is
        %   done in the CellProfiler2MVCAL function so now the position is
        %   in matrix indexing.
        %   2) the SIMCEP stores the same coordinate for cytoplasm subcell
        %   and nuclei so we can use either of them
            dist = zeros(1,length(struct.cytoplasm.coords));
            for i=1:length(struct.cytoplasm.coords)
                dist(i) = pdist([convert2RowVector(struct.cytoplasm.coords{i}) ; convert2RowVector(positions)]);
            end
            [~,index] = min(dist);            
        end
                
        function [input,target] = calcFeatures(~,struct,indexOfCellInGroundTruth,inputVector)
        %calculate features function
        %   Here you can modify what to consider as input and target.
            
            %we simply do not modify the inputs (case A and B but leave this also uncommented for other cases)
            input = inputVector;
            
            %input additions in special cases:
%             Case E
%             input(end+1) = struct.subcell.secondPartFeatures{indexOfCellInGroundTruth}.totalIntensity;
%             input(end+1) = struct.subcell.secondPartFeatures{indexOfCellInGroundTruth}.meanCenterIntensity;
%             input(end+1) = struct.subcell.nofParts{indexOfCellInGroundTruth};
%             Case C                
%             if ((input(88)*input(107))==0)            
%                input(end+1) = 0;            
%             else
%                input(end+1) = input(26)./(input(88).*input(107));            
%             end
%             Case D
%             if (input(44)*input(63)==0)
%                input(end+1) = 0;
%             else
%                input(end+1) = input(4)./(input(44).*input(63));
%             end
%             Case F            
%             input(1) = struct.subcell.secondPartFeatures{indexOfCellInGroundTruth}.totalIntensity;           
%             input(2) = struct.subcell.secondPartFeatures{indexOfCellInGroundTruth}.meanCenterIntensity;
%             input(3) = struct.subcell.nofParts{indexOfCellInGroundTruth};
%             input(4) = struct.subcell.secondPartFeatures{indexOfCellInGroundTruth}.meanIntensity;
%             input(5) = struct.subcell.secondPartFeatures{indexOfCellInGroundTruth}.totalCenterIntensity;
%             input(6:end) = [];
%             Case G
%             input(end+1) = struct.subcell.secondPartFeatures{indexOfCellInGroundTruth}.totalIntensity;
%             input(end+1) = struct.subcell.secondPartFeatures{indexOfCellInGroundTruth}.meanCenterIntensity;
%             input(end+1) = struct.subcell.nofParts{indexOfCellInGroundTruth};
%             input(end+1) = input(end-2)./(input(end-1).*input(end));
%             Case H
%             input(1) = struct.subcell.secondPartFeatures{indexOfCellInGroundTruth}.totalIntensity;           
%             input(2) = struct.subcell.secondPartFeatures{indexOfCellInGroundTruth}.meanCenterIntensity;
%             input(3) = struct.subcell.nofParts{indexOfCellInGroundTruth};
%             input(4) = struct.subcell.secondPartFeatures{indexOfCellInGroundTruth}.meanIntensity;
%             input(5) = struct.subcell.secondPartFeatures{indexOfCellInGroundTruth}.totalCenterIntensity;
%             input(6) = input(1)./(input(2).*input(3));
%             input(7:end) = [];
            
            target(1,1) = mean(struct.subcell.partIntensity{indexOfCellInGroundTruth});
            target(2,1) = mean(struct.subcell.partRadius{indexOfCellInGroundTruth});
        end

    end
    
end



