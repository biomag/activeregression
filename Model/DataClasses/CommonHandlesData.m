classdef CommonHandlesData < Data
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	CommonHandlesData
%   
% This is an implementation of the data model used for
% AL strategies. The data model's requirements are described in Data
% class.
%   CommonHandles is a great structure used by Advanced Cell Classifier
%   or shortly: ACC which is the GUI for our scientists. In ACC they do
%   the labeling and this is the program which handles the images made
%   by the microscopes. Most of the data for this program are stored in
%   CommonHandles. For real cases we use this implementation of Data
%   model.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.        
    
    properties
        % The CommonHandles structure, we'll use this for
        % the function implementations.
        CommonHandles
        % ClassIdx class Index. In our concept the regression is the
        % specification of the classification, we do regression inside of a
        % class. First the classification method runs, and then in special
        % classes we do regression. ClassIdx stores the indeces of the
        % regression classes. In the current implementation it's only one
        % variable, if we'll have more than one regression class this code
        % also must be modified.
        classIdx        
        
        %The indices of the cells in the global training set for this
        %specific regression class.
        originalIndices
    end
    
    methods
        %Constructor
        function obj = CommonHandlesData(CH,classIdx)
            obj.CommonHandles = CH;
            obj.classIdx = classIdx;
            obj.bounds = '[0 1000; 0 1000]';
            obj.originalIndices = find(cellfun(@find,CH.TrainingSet.Class) == classIdx);
            %The labeling pool in the automatic case is the number of the
            %samples in that class.
            obj.sizeOfLabelingPool = length(obj.originalIndices);
        end
        
        %We put the new information into the data.
        function obj = label(obj,actualCell, target)
            
            obj.CommonHandles.TrainingSet.RegPos{obj.originalIndices(actualCell)} = target';
            %Regression{obj.classIdx}.ImagePosition{actualCell}=target';           
            if (~ismember(actualCell,obj.realLabels))
                obj.realLabels = [obj.realLabels, actualCell];  
            end
        end
                
        function [inputs, targets] = getLabels(obj, specialization)            
            if nargin>1
                if isequal(sort(convert2RowVector(intersect(specialization,obj.realLabels))),sort(convert2RowVector(specialization)))
                    selectedCells = convert2ColumnVector(specialization);                
                else
                    baseException = MException('MYEXC:nolabeledRequsted','Sorry not all the requested cells are labeled. Please check realLabels property.');
                    throw(baseException);   
                end
            elseif ~isequal(obj.realLabels,[])
                selectedCells = convert2ColumnVector(obj.realLabels);
            else
                baseException = MException('MYEXC:nolabeledany','Sorry there is no any labeled cells yet, so we cannot provide the any labels.');
                throw(baseException);
            end
            [inputs, targets] = getDataFromCH( obj.CommonHandles, obj.originalIndices, selectedCells);
        end
        
        % Currently I have data from the labeled set, from the
        % commonHandles regression field. This must be modified if we
        % extend the model to the real case.
        function [input, img] = getData(obj,specialization)
            if nargin>1
                specialization = convert2ColumnVector(specialization);
            else
                specialization = convert2ColumnVector(1:obj.sizeOfLabelingPool);
            end
            
            input = getDataFromCH( obj.CommonHandles, obj.originalIndices, specialization);
            img = cell(1,length(specialization));
            for i=1:length(specialization)
                classImgs = obj.CommonHandles.ClassImages(obj.CommonHandles.Classes{obj.classIdx}.Name);
                img{i} = classImgs.Images{specialization(i)}.Image;
            end
        end
    end
    
end

