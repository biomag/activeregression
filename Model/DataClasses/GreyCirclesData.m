classdef GreyCirclesData < Data
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	GreyCirclesData
%   
% This is an implementation of the data model used for
% AL strategies. The data model's requirements are described in Data
% class.
%   This is the first class to do some training by anyone who wants to
%   try it.    
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.    

    properties
        %circleData is a huge cell Array every cell in it contains a
        %structure which describe circle with the color intensity and the
        %radius. The fields are .c and .r referring to color and radius.        
        circleData
        
        %stores the targets for our circles. If there are multiple targets,
        %then they are stored as columns. Here the targets will come from
        %the user immediately.
        targets
        
        %The constructor function also stores images of the generated
        %cells.
        images                
    end
    
    methods
        %Constructor
        function obj = GreyCirclesData(nofSamples)
            [obj.circleData, obj.images] = generateArtificialCell2(nofSamples);                        
            obj.sizeOfLabelingPool = nofSamples;
            obj.bounds = '[0 1; 0 1]';
        end
        
        %We put the new information into the data.
        function obj = label(obj,actualCell, target)
            
            obj.targets(:,actualCell)=convert2ColumnVector(target); 
            if (~ismember(actualCell,obj.realLabels))
                obj.realLabels = [obj.realLabels, actualCell];  
            end
        end
               
        %Getting labels
        function [inputs, targets] = getLabels(obj, specialization)            
            if nargin>1
                if isequal(sort(convert2RowVector(intersect(specialization,obj.realLabels))),sort(convert2RowVector(specialization)))
                    selectedCells = convert2ColumnVector(specialization);                
                else
                    baseException = MException('MYEXC:nolabeledRequsted','Sorry not all the requested cells are labeled. Please check realLabels property.');
                    throw(baseException);   
                end
            elseif ~isequal(obj.realLabels,[])
                selectedCells = convert2ColumnVector(obj.realLabels);
            else
                baseException = MException('MYEXC:nolabeledany','Sorry there is no any labeled cells yet, so we cannot provide the any labels.');
                throw(baseException);
            end

            inputs = zeros(2,length(selectedCells));
            targets = zeros(2,length(selectedCells));
            for i=1:length(selectedCells)
                inputs(1,i) = obj.circleData{selectedCells(i)}.c;
                inputs(2,i) = obj.circleData{selectedCells(i)}.r;                
                targets(:,i) = obj.targets(:,selectedCells(i));
            end            
             
        end
                
        %getData
        function [input, img] = getData(obj,specialization)
            if nargin>1
                specialization = convert2ColumnVector(specialization);
            else
                specialization = convert2ColumnVector(1:obj.sizeOfLabelingPool);
            end
                
            input = zeros(2,length(specialization));            
            for i=1:length(specialization)
                input(1,i) = obj.circleData{specialization(i)}.c;
                input(2,i) = obj.circleData{specialization(i)}.r;                                
            end
            
            img = cell(1,length(specialization));
            for i=1:length(specialization)
                img{i} = obj.images{i};
            end
        end
                
        %plot data to regression plane
        %this function helps to visualize the results of the regression
        %process. It gets the inputs, and targets and based on that it
        %draws out the regression plane.
        function plotDataToRegressionPlane(obj,indeces, targets)
            alfa = linspace(0,2*pi,300);
            zooming = 20;
            for i=1:length(indeces)
                fill(obj.circleData{indeces(i)}.r*zooming.*cos(alfa)+targets(1,i),obj.circleData{indeces(i)}.r*zooming.*sin(alfa)+targets(2,i),[obj.circleData{indeces(i)}.c obj.circleData{indeces(i)}.c obj.circleData{indeces(i)}.c]);
                hold on;
                plot(obj.circleData{indeces(i)}.r*zooming.*cos(alfa)+targets(1,i),obj.circleData{indeces(i)}.r*zooming.*sin(alfa)+targets(2,i),'k');
            end            
        end
        
         %We convert our artificial data to ACC format.
        % ACC format:
        %   - the data is found in 3 folders usually called anal1 anal2 and
        %   anal3.
        %       In anal1 there are the segmented images.
        %       In anal3 there are the raw images.
        %       In anal2 there are text files describing the features for
        %       each image.
        %     This function does not make difference between the raw and
        %     segmented images. We use again the input calculator function.
        % In ACC format on one images usually you can find more than one
        % object. We'll put 9 objects on each image.
        % 
        % INPUTS:
        %        - nofObjectsOnOnePicture should be a square number!
        %        - plateName - how you call your plate - string
        function convertDataToACC(obj,nofObjectsOnOnePicture,plateName)
                        
                osslash = filesep;                            
            
            %some index arrays for plot
            [A,B] = meshgrid(50:100:50+(sqrt(nofObjectsOnOnePicture)-1)*100);
            A = reshape(A,1,nofObjectsOnOnePicture);
            B = reshape(B,1,nofObjectsOnOnePicture);
            mkdir(plateName);
            cd(plateName);
            mkdir('anal1');
            mkdir('anal2');
            mkdir('anal3');
            whitebg('y');
            
           for i=1:ceil(obj.sizeOfLabelingPool/nofObjectsOnOnePicture)               
                file = fopen(['anal2' osslash plateName '_' num2str(i) '.txt'],'w');
                obj.plotDataToRegressionPlane((i-1)*nofObjectsOnOnePicture+1 : min(i*nofObjectsOnOnePicture,obj.sizeOfLabelingPool)  , [A;B]);
                hold off;
                h = gcf();
                f =get(h,'CurrentAxes');
                F = getframe(f);
                I = frame2im(F);
                imwrite(I,['anal1' osslash plateName '_' num2str(i) '.png']);
                imwrite(I,['anal3' osslash plateName '_' num2str(i) '.png']);
                
                inputs = zeros(2,min(i*nofObjectsOnOnePicture,obj.sizeOfLabelingPool)-(i-1)*nofObjectsOnOnePicture);
                k=1;
                for j=(i-1)*nofObjectsOnOnePicture+1 : min(i*nofObjectsOnOnePicture,obj.sizeOfLabelingPool)
                    inputs(1,k) = obj.circleData{j}.c;
                    inputs(2,k) = obj.circleData{j}.r;
                    k=k+1;
                end
                for j=1:size(inputs,2)
                   fprintf(file,'%f %f ',A(j)/500*435,(B(size(inputs,2)-j+1))/500*344);% sqrt(nofObjectsOnOnePicture)*100-
                   for k=1:size(inputs,1)
                       fprintf(file,'%f ', inputs(k,j));
                   end
                   fprintf(file,'\n');
                end
                fclose(file);
           end
           cd('..');
        end
        
                
    end
    
end

