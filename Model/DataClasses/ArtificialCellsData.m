classdef ArtificialCellsData < Data
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	ArtificialCellsData
%   
% ArtificialCellsData This is an implementation of the data model used for
% AL strategies. The data model's requirements are described in Data
% class.
%   We generate some artificial cells and store them in
%   this dataClass.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
    
    properties
        %cellData is a huge cell Array every cell in it contains a
        %structure which describe one artificial cell, such as the minor
        %and major axes, positions, colors etc.
        cellData
        
        %stores the targets for our cells. If there are multiple targets,
        %then they are stored as columns.
        targets
        
        %The constructor function also stores images of the generated
        %cells.
        images 
        
    end
    
    methods
        %Constructor
        function obj = ArtificialCellsData(nofSamples)
            [obj.cellData, obj.images] = generateArtificialCell1(nofSamples);            
            
            obj.sizeOfLabelingPool = nofSamples;
            obj.bounds = '[0 1;0 1]';
        end
        
        %We put the new information into the data.
        function obj = label(obj,actualCell, target)            
            obj.targets(:,actualCell)=target; 
            if (~ismember(actualCell,obj.realLabels))
                obj.realLabels = [obj.realLabels, actualCell];  
            end
        end
               
        %Getting labels
        function [inputs, targets] = getLabels(obj, specialization)            
            if nargin>1
                if isequal(sort(convert2RowVector(intersect(specialization,obj.realLabels))),sort(convert2RowVector(specialization)))
                    selectedCells = convert2ColumnVector(specialization);                
                else
                    baseException = MException('MYEXC:nolabeledRequsted','Sorry not all the requested cells are labeled. Please check realLabels property.');
                    throw(baseException);   
                end
            elseif ~isequal(obj.realLabels,[])
                selectedCells = convert2ColumnVector(obj.realLabels);
            else
                baseException = MException('MYEXC:nolabeledany','Sorry there is no any labeled cells yet, so we cannot provide the any labels.');
                throw(baseException);
            end
                           
            [inputs, ~] = obj.calcFeatures(selectedCells);
            targets = obj.targets(:,selectedCells);
             
        end
                
        %getData
        function [input, img] = getData(obj,specialization)
            if nargin>1
                specialization = convert2ColumnVector(specialization);
            else
                specialization = convert2ColumnVector(1:obj.sizeOfLabelingPool);
            end
            
            input = obj.calcFeatures(specialization);
            
            img = cell(1,length(specialization));
            for i=1:length(specialization)
                img{i} = obj.images{i};
            end
        end
        
        % ************************************             
        % here we can MODIFY what to consider as the TARGET, and
        % also what we consider as input. Please don't forget to modify
        % bounds at the constructor if you change the theoretical bounds
        % for this data.

        % Current implementation:
        %       INPUTS are the parameters of the genereated cells
        %       + 1 which is referring to the variance of the dot
        %       positions. (it is the mean distance from the center of gravity)
        %       TARGETS: 
        %           1. the dotAccumulation generated target
        %           2. intensity of the nucleus

        % **************************************     
        function [inputs, targets] = calcFeatures(obj, selectedCells)
            inputs = zeros(17,length(selectedCells));
            targets = zeros(2,length(selectedCells));
            
            for i=1:length(selectedCells)
                inputs(1,i) = obj.cellData{selectedCells(i)}.cytoA;
                inputs(2,i) = obj.cellData{selectedCells(i)}.cytoB;
                inputs(3,i) = obj.cellData{selectedCells(i)}.cytoColorR;
                inputs(4,i) = obj.cellData{selectedCells(i)}.cytoColorG;
                inputs(5,i) = obj.cellData{selectedCells(i)}.cytoColorB;
                inputs(6,i) = obj.cellData{selectedCells(i)}.nuclA;
                inputs(7,i) = obj.cellData{selectedCells(i)}.nuclB;
                inputs(8,i) = obj.cellData{selectedCells(i)}.nuclAng;
                inputs(9,i) = obj.cellData{selectedCells(i)}.nuclOffset;
                inputs(10,i) = obj.cellData{selectedCells(i)}.nuclColorR;
                inputs(11,i) = obj.cellData{selectedCells(i)}.nuclColorG;
                inputs(12,i) = obj.cellData{selectedCells(i)}.nuclColorB;
                inputs(13,i) = obj.cellData{selectedCells(i)}.nofParticles;
                inputs(14,i) = obj.cellData{selectedCells(i)}.dotColorR;
                inputs(15,i) = obj.cellData{selectedCells(i)}.dotColorG;
                inputs(16,i) = obj.cellData{selectedCells(i)}.dotColorB;
                
                %Calculating the 17th input for this cell: this is the
                %variance of the dots positions.
                if obj.cellData{selectedCells(i)}.nofParticles ~=0
                    Xmean = mean(obj.cellData{selectedCells(i)}.dotX);
                    Ymean = mean(obj.cellData{selectedCells(i)}.dotY);
                    distances = zeros(1,obj.cellData{selectedCells(i)}.nofParticles);
                    for j = 1:obj.cellData{selectedCells(i)}.nofParticles
                        distances(j) = pdist([obj.cellData{selectedCells(i)}.dotX(j) obj.cellData{selectedCells(i)}.dotY(j) ; Xmean Ymean ]);
                    end
                    inputs(17,i) = mean(distances);
                else
                    inputs(17,i) = 0;
                end
                %inputs(18,i) = obj.cellData{selectedCells(i)}.dotAccumulation;
                
                % Target calculation
                
                targets(1,i) = obj.cellData{selectedCells(i)}.dotAccumulation;
                %targets(2,i) = (obj.cellData{selectedCells(i)}.nuclColorR + obj.cellData{selectedCells(i)}.nuclColorG + obj.cellData{selectedCells(i)}.nuclColorB) / 3;
                targets(2,i) = (obj.cellData{selectedCells(i)}.dotColorR + obj.cellData{selectedCells(i)}.dotColorG + obj.cellData{selectedCells(i)}.dotColorB) / 3;
                %targets(1,i) = obj.cellData{selectedCells(i)}.dotAccumulation;
                
            end
        end
        
        
        %plot data to regression plane
        %this function helps to visualize the results of the regression
        %process. It gets the inputs, and targets and based on that it
        %draws out the regression plane.
        function plotDataToRegressionPlane(obj,indeces, targets)
            alfa = linspace(0,2*pi,300);
            zooming = 30;            
            for i=1:length(indeces)
                a = obj.cellData{indeces(i)}.cytoA;
                b = obj.cellData{indeces(i)}.cytoB;
                cColorR = obj.cellData{indeces(i)}.cytoColorR;
                cColorG = obj.cellData{indeces(i)}.cytoColorG;
                cColorB = obj.cellData{indeces(i)}.cytoColorB;
                fill(zooming.*a*cos(alfa)+targets(1,i),zooming.*b*sin(alfa)+targets(2,i),[cColorR, cColorG, cColorB]);
                hold on;
                
                phi = obj.cellData{indeces(i)}.nuclAng;
                rotate = [cos(phi),-sin(phi);
                          sin(phi), cos(phi)];
                      
                c = obj.cellData{indeces(i)}.nuclA;
                d = obj.cellData{indeces(i)}.nuclB;
                rotatedEllipse = rotate*[c*cos(alfa);d*sin(alfa)];
                
                rOffset = obj.cellData{indeces(i)}.nuclOffset;
                xOffset = rOffset*cos(phi);
                yOffset = rOffset*sin(phi);
                nColorR = obj.cellData{indeces(i)}.nuclColorR;
                nColorG = obj.cellData{indeces(i)}.nuclColorG;
                nColorB = obj.cellData{indeces(i)}.nuclColorB;
                
                fill(zooming.*rotatedEllipse(1,:)+xOffset+targets(1,i),zooming.*rotatedEllipse(2,:)+yOffset+targets(2,i), [nColorR, nColorG, nColorB]);
                
                dotR = obj.cellData{indeces(i)}.dotR;
                nofDots = obj.cellData{indeces(i)}.nofParticles;
                dotX = obj.cellData{indeces(i)}.dotX;
                dotY = obj.cellData{indeces(i)}.dotY;
                oColorR = obj.cellData{indeces(i)}.dotColorR;
                oColorG = obj.cellData{indeces(i)}.dotColorG;
                oColorB = obj.cellData{indeces(i)}.dotColorB;
                for j=1:nofDots        
                    fill((dotR(j)*cos(alfa)+dotX(j)).*zooming+targets(1,i), (dotR(j)*sin(alfa)+dotY(j)).*zooming+targets(2,i),[oColorR, oColorG, oColorB]);
                end                                   
            end            
        end
        
        %We convert our artificial data to ACC format.
        % ACC format:
        %   - the data is found in 3 folders usually called anal1 anal2 and
        %   anal3.
        %       In anal1 there are the segmented images.
        %       In anal3 there are the raw images.
        %       In anal2 there are text files describing the features for
        %       each image.
        %     This function does not make difference between the raw and
        %     segmented images. We use again the input calculator function.
        % In ACC format on one images usually you can find more than one
        % object. We'll put 9 objects on each image.
        % 
        % One image is a picture of a well. The ACC for predictions uses
        % the image file name for determine the well of which the picture
        % was taken. This function makes a 384 well-plate meaning. (24x16)
        % A-P rows and 1-16 numbers the columns.
        % 
        % INPUTS:
        %        - nofObjectsOnOnePicture should be a square number!
        %        - plateName - how you call your plate - string        
        function convertDataToACC(obj,nofObjectsOnOnePicture,plateName)
            
            if ispc
                osslash = '\';
            else
                osslash = '/';
            end
            
            row = 'A';
            col = 1;
            imageField = 5;
            
            %some index arrays for plot
            [A,B] = meshgrid(50:100:50+(sqrt(nofObjectsOnOnePicture)-1)*100);
            A = reshape(A,1,nofObjectsOnOnePicture);
            B = reshape(B,1,nofObjectsOnOnePicture);
            mkdir(plateName);
            cd(plateName);
            mkdir('anal1');
            mkdir('anal2');
            mkdir('anal3');
            whitebg('k');
            
           for i=1:ceil(obj.sizeOfLabelingPool/nofObjectsOnOnePicture)
                identificationString = sprintf('%c%0.2d_s%0.2d',row,col,imageField);
                file = fopen(['anal2' osslash plateName '_w' identificationString '.txt'],'w');
                obj.plotDataToRegressionPlane((i-1)*nofObjectsOnOnePicture+1 : min(i*nofObjectsOnOnePicture,obj.sizeOfLabelingPool)  , [A;B]);
                hold off;
                h = gcf();
                f =get(h,'CurrentAxes');
                F = getframe(f);
                I = frame2im(F);
                imwrite(I,['anal1' osslash plateName '_w' identificationString '.png']);
                imwrite(I,['anal3' osslash plateName '_w' identificationString '.png']);
                [inputs,~] = obj.calcFeatures((i-1)*nofObjectsOnOnePicture+1 : min(i*nofObjectsOnOnePicture,obj.sizeOfLabelingPool));
                for j=1:size(inputs,2)
                   fprintf(file,'%f %f ',A(j)/300*435,(B(size(inputs,2)-j+1))/300*344);% sqrt(nofObjectsOnOnePicture)*100-
                   for k=1:size(inputs,1)
                       fprintf(file,'%f ', inputs(k,j));
                   end
                   fprintf(file,'\n');
                end
                fclose(file);
              
                if col==24
                    row = row+1;
                    col = 1;
                elseif col == 'P'
                    imageField = imageField+1;
                else
                    col = col+1;
                end
           end
           cd('..');
        end
        
    end
    
end

