classdef Plots < handle
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	Plots
%   
% Collection of plots, which means many visualization method to
% check the active learning runs.
%   The plot class is able to plot many different types of plots and it
%   can also save them in a picture. We can turn on and off all the
%   types of plots with the properties of this class. Possible property
%   values:
%       0: do not plot this type
%       1: do plot
%       2: do plot and save
%
%   This class is designed to have only few function which does all the
%   required plots and with switches you can turn on and off which
%   plots to require. The switches are properties of this class.
%   The detailed explanation for the plots are in the properties part.
%
%   During several statistical runs we might wish to set all the plots
%   to zero (not to plot). This functionality can be done with
%   setAll2Value function.

%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
        
    properties
        %Plot types
        %variance for objects contains 2 figures (as this system used mostly with 2D regression, but later it should be modified to work with arbitrary output dimension)
        %The figure plots all the data (including test, training and
        %reamining set) and the variance for the selected object among
        %the selected dimension. Elements from trainingSet displayed in
        %green, elements from the test set are blue the others are red.
        varianceForObjects = 0;
        
        %error vectors contains error for the training set. We check
        %the prediction and compare with the given ground truth. The
        %ground truth is in green the prediction is purple.
        errorVectors = 2;
        
        %if we have a test set we also know ground truth for the elements
        %in the test set so we can plot the error vectors. (additionally we
        %also plot here the error vectors on the training set)
        errorVectorsTest = 2;
        
        %varianceChangeOnTestSetOverTrainingSetSize: we plot how the
        %trainingSet uncertainty changes as the training set is
        %increasing.
        varianceChange = 2;
        
        %we plot how the error changes on the seperated test set
        errorChange = 2;
        
        %we put some pictures to an image to illustrate the regression
        %plane. We draw out the state of the regression
        %plane with the pictures of our cells. This works with SIMCEP
        %data only.
        regressionPlane = 2;
        
        %we plot the regression plane but without pictures the objects
        %are represented with starts.
        regressionPlaneStar = 0;
        
        %choicePlot illustrating the last active choice and its
        %properties. We plot the previous regression (star) plane and
        %mark the chosen cell then plot the received ground truth and
        %the new prediction. We also write to this figure the UC values
        %for the object before and after it was selected for labeling.
        choicePlot = 2;
        
        %history. We store information about the predicted positions.
        %This plot shows how the positions changed for one cell. You can
        %make many types of plot here by changing the subsampling from all
        %the points. (subsampling is very useful as there can be a lots of
        %points after a long time and the diagram becomes very confusing
        %even with historical coloring)
        historyAll = 0;
        historyTest = 0;
        historyTestSub = 0;
        subsetForTest;
        
        %Final plots: these plots are used after multiple AL runs to
        %evaluate the effectiveness of the runs.
        
        %finalMeanTestError Every run has an error curve which shows
        %the mean error on the test set over the size of the training
        %set. This curve is the average of them.
        finalMeanTestError = 0;
        
        %finalMeanOverAllUncertaintyByCovariance The same as above but
        %for the over all uncertatinty calculated by function written
        %by us (BIOMAG)
        finalMeanOverAllUncertaintyByCovariance = 0;
        
        %as above but the uncertainty is calculated by the gp function.
        finalMeanOverAllUncertaintyByPredictor = 0;
        
        %as above but the uncertainty is calculated by the gp function.
        finalMeanTestOverAllUncertaintyByCovariance = 0;        
        
        
        %Private values
        figure1forVariance;
        figure2forVariance;
        figureforErrorVectors;
        figureforErrorVectorsTest;
        figureforVarianceOnTestSet;
        figureforErrorOnTestSet;
        figureforRegressionPlane;
        figureforRegressionPlaneStar;
        figureforChoicePlot;
        figureforHistoryAll;
        figureforHistoryTest;
        figureforHistoryTestSub;
        
        %regPlaneBoundaries the array which knows the theoretical bounds
        %for the regression plane
        regPlaneBoundaries;        
        
        %a 2 element array which stores the maximum variance on each
        %dimension. It is used for setting the limits for the plot.
        varianceMax
    end
    
    methods
        
        function obj = Plots()
            %Constructor which initialize the plots
            obj.figure1forVariance = figure('name','Variance among 1st dimension');
            obj.figure2forVariance = figure('name','Variance among 2nd dimension');
            obj.figureforErrorVectors = figure('name','Error vectors in the training set');
            obj.figureforErrorVectorsTest = figure('name','Error vectors in the test (and training) set');
            obj.figureforVarianceOnTestSet = figure('name','Variance change on test set over training set size');
            obj.figureforErrorOnTestSet = figure('name','Error change on test set over training set size');
            obj.figureforRegressionPlane = figure('name','Regression plane');
            obj.figureforRegressionPlaneStar = figure('name','Regression plane stars');
            obj.figureforChoicePlot = figure('name','Choice plot');
            obj.figureforHistoryAll = figure('name','History all objects');
            obj.figureforHistoryTest = figure('name','History on test set');
            obj.figureforHistoryTestSub = figure('name','History on 10 elements subset of test set');
                        
            obj.subsetForTest = [];
            obj.varianceMax = [];
        end
        
        function setAll2Value(obj,v)
            obj.varianceForObjects = v;
            obj.errorVectors = v;
            obj.errorVectorsTest = v;
            obj.varianceChange = v;
            obj.errorChange = v;
            obj.regressionPlane = v;
            obj.regressionPlaneStar = v;
            obj.choicePlot = v;
            obj.historyAll = v;
            obj.historyTest = v;
            obj.historyTestSub = v;
        end
        
        function doPlots(obj,AC,ind)
            %doPlots get the ALController object from outside and an index
            %which corresponds to the state of the active learning cycle.
            %(the index is needed to give name to the saved figures)
            %If we need to train a new predictor then we store it in the
            %predictorStore.
            
            %we get out also the bounds property from ACs data.
            obj.regPlaneBoundaries = str2num(AC.data.bounds); %#ok<ST2NM> We need to use str2num as we work on a matrix.            
            
            %There are many things which should be computed here because
            %many plots use the same data.
            [readyPredictor] = getPredictorMacro(AC.predictor, AC.data, AC.predictorStore, AC.trainingSet);
            
            %If the predictor is not capable to predict variance
            %(uncertainty) then we should zero out the plots which needs
            %this information.
            if (~readyPredictor{1}.isDistribution)
                obj.varianceForObjects = 0;
                obj.varianceChange = 0;
            end
            
            [ALLinput, ALLimg] = AC.data.getData();
            %we resize images if they're too big
            for i=1:length(ALLimg)
                if ( size(ALLimg{i},1)>60 || size(ALLimg{i},2)>60 )
                    maxSize = max(size(ALLimg{i},1),size(ALLimg{i},2));
                    ALLimg{i} = imresize(ALLimg{i},[size(ALLimg{i},1)./maxSize.*60,size(ALLimg{i},1)./maxSize.*60]);
                end
            end
            [outputALL,varALL] = readyPredictor{1}.predict(ALLinput);
            d = diag(varALL);
            [~,groundTruth] = AC.data.getLabels(AC.trainingSet);
            
            %get the previous predictor from the store
            %big P stands for previous which means that what was the
            %prediction before we selected the last training object.
            [readyPredictorP] = getPredictorMacro(AC.predictor, AC.data, AC.predictorStore, AC.trainingSet(1:end-1));
            [outputALLP,varALLP] = readyPredictorP{1}.predict(ALLinput);
            dP = diag(varALLP);
            
            %% *** PLOTS ***
            
            %% plot variance among 2 dimension
            if (obj.varianceForObjects > 0)
                
                %check for the limits we suppose that the first maximum is
                %the biggest all the time.
                if isempty(obj.varianceMax)
                    AC.plots.varianceMax(1) = max(d(1:length(d)/2));
                    AC.plots.varianceMax(2) = max(d(length(d)/2+1:end));
                end
                                
                set(0,'currentfigure',obj.figure1forVariance);                
                fprintf('Plot: variance for object (%d) \n',obj.varianceForObjects);
                plotVarianceForObjects( d, 1,2 ,AC.plots.varianceMax,AC.trainingSet,AC.testSet);
                obj.saveAction(obj.varianceForObjects,'varianceForObjects','1D',ind);               
                
                set(0,'currentfigure',obj.figure2forVariance);                       
                plotVarianceForObjects( d, 2,2 ,AC.plots.varianceMax,AC.trainingSet,AC.testSet);
                obj.saveAction(obj.varianceForObjects,'varianceForObjects','2D',ind);                
                
            end
            
            %plotGaussianProcessMeansAndVariances(allInputs,predictor);
            
            %% plot error vectors in the training Set
            if (obj.errorVectors>0)
                fprintf('Plot: errorVectors on training set (%d) \n',obj.errorVectors);
                set(0,'currentfigure',obj.figureforErrorVectors);                
                plot([obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,1)],[obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,1)],'k--');
                hold on;
                
                for i=1:length(AC.trainingSet)
                    plot([groundTruth(1,i), outputALL(1,AC.trainingSet(i))],[groundTruth(2,i), outputALL(2,AC.trainingSet(i))],'b');
                    plot(groundTruth(1,i),groundTruth(2,i),'g*');
                    plot(outputALL(1,AC.trainingSet(i)),outputALL(2,AC.trainingSet(i)),'*','Color',[148/255 0 211/255]);
                    hold on;
                end
                obj.setLimitsForPlot(0.1);
                obj.saveAction(obj.errorVectors,'errorVectors','',ind);                
                hold off;
                
            end
            
            %% Plot error vectors in test set (also the training set is included here)
            if obj.errorVectorsTest>0
                fprintf('Plot: errorVectors on test set (%d) \n',obj.errorVectorsTest);
                set(0,'currentfigure',obj.figureforErrorVectorsTest);                
                plot([obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,1)],[obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,1)],'k--');
                hold on;
                %for all the cells
                for i=1:AC.data.sizeOfLabelingPool
                    %if we're in the regplane                    
                    if (ismember(i,AC.trainingSet))
                        plot(outputALL(1,i),outputALL(2,i),'*','Color',[1, 69/255, 0]);
                        [~,gt] = AC.data.getLabels(i);
                        plot(gt(1),gt(2),'g*');
                        plot([outputALL(1,i) gt(1)],[outputALL(2,i) gt(2)],'b');
                    elseif (ismember(i,AC.testSet))
                        plot(outputALL(1,i),outputALL(2,i),'*','Color',[1, 69/255, 1]);
                        [~,gt] = AC.data.getLabels(i);
                        plot(gt(1),gt(2),'g*');
                        plot([outputALL(1,i) gt(1)],[outputALL(2,i) gt(2)],'k');                    
                    end                    
                end
                obj.setLimitsForPlot(0.2);
                obj.saveAction(obj.errorVectorsTest,'errorVectorsTest','',ind);                                
                hold off;                
            end
            
            %% plot variance change
            if (obj.varianceChange > 0)
                fprintf('Plot: variance change on the test set (%d) \n',obj.varianceChange);
                set(0,'currentfigure',obj.figureforVarianceOnTestSet);
                plot(AC.testUncertaintyByCovariance);
                obj.saveAction(obj.varianceChange,'varianceOnTestSet','',ind);                
                hold off;
            end
            
            %% error change
            if (obj.errorChange > 0)
                set(0,'currentfigure',obj.figureforErrorOnTestSet);
                plot(AC.testError);
                obj.saveAction(obj.errorChange,'errorOnTestSet','1D',ind);                
                hold off;
            end
                        
            %% regressionplane plot. This works currently only with SIMCEP
            %data.
            if obj.regressionPlane>0                
                
                %the regression plane with additional 50 pixel border. The
                %50 pixel border is used to handle cases when the picture
                %is close to the borders (we don't have to bother with the
                %index out of range exceptions because there is always
                %enough space to put in the image). 
                regPlaneSize = 1000;
                %if half of the image can be larger than 50 then this
                %should be modified not to get errors.
                regPlaneBorder = 50;
                %initialize regPlane
                regPlane = uint8(zeros(regPlaneSize+regPlaneBorder*2,regPlaneSize+regPlaneBorder*2,3));
                
                for i=1:length(ALLimg)                    
                    %check for images
                    if ~(outputALL(2,i)>obj.regPlaneBoundaries(2,2) || outputALL(2,i)<obj.regPlaneBoundaries(2,1) || outputALL(1,i)<obj.regPlaneBoundaries(1,1) || outputALL(1,i)>obj.regPlaneBoundaries(1,2))
                        regPlane = mergeMatricesByCenter(regPlane,ALLimg{i},[regPlaneSize-round( ((outputALL(2,i))-obj.regPlaneBoundaries(2,1))./(obj.regPlaneBoundaries(2,2)-obj.regPlaneBoundaries(2,1)).*regPlaneSize )+regPlaneBorder,round( ((outputALL(1,i))-obj.regPlaneBoundaries(1,1))./(obj.regPlaneBoundaries(1,2)-obj.regPlaneBoundaries(1,1)).*regPlaneSize )+regPlaneBorder ]);
                    end
                end
                regPlane = regPlane(regPlaneBorder+1:regPlaneSize+regPlaneBorder,regPlaneBorder+1:regPlaneSize+regPlaneBorder,:);
                set(0,'currentfigure',obj.figureforRegressionPlane);
                imshow(regPlane);
                
                obj.saveAction(obj.regressionPlane,'regressionPlane','',ind);                
                
            end
            
            %% regression plane with stars
            if obj.regressionPlaneStar>0
                set(0,'currentfigure',obj.figureforRegressionPlaneStar);                
                plot([obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,1)],[obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,1)],'k--');
                hold on;
                %for all the cells
                for i=1:AC.data.sizeOfLabelingPool
                    %if we're in the regplane
                    if outputALL(2,i)>obj.regPlaneBoundaries(2,2) || outputALL(2,i)<obj.regPlaneBoundaries(2,1) || outputALL(1,i)<obj.regPlaneBoundaries(1,1) || outputALL(1,i)>obj.regPlaneBoundaries(1,2)
                        if (ismember(i,AC.trainingSet))
                            plot(outputALL(1,i),outputALL(2,i),'*','Color',[1, 69/255, 0]);
                            [~,gt] = AC.data.getLabels(i);
                            plot(gt(1),gt(2),'g*');
                            plot([outputALL(1,i) gt(1)],[outputALL(2,i) gt(2)],'b');
                        else
                            plot(outputALL(1,i),outputALL(2,i),'r*');
                        end
                    %if we're not: the difference is in colors.
                    else
                        if (ismember(i,AC.trainingSet))
                            plot(outputALL(1,i),outputALL(2,i),'*','Color',[1, 69/255, 1]);
                            [~,gt] = AC.data.getLabels(i);
                            plot(gt(1),gt(2),'g*');
                            plot([outputALL(1,i) gt(1)],[outputALL(2,i) gt(2)],'b');
                        else
                            plot(outputALL(1,i),outputALL(2,i),'b*');
                        end
                    end
                end                
                obj.setLimitsForPlot(0.2);
                obj.saveAction(obj.regressionPlaneStar,'regressionPlane','star',ind);                
                hold off;                
                
            end                        
            
            %% choice plot
            if obj.choicePlot>0
                set(0,'currentfigure',obj.figureforChoicePlot);                
                plot([obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,1)],[obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,1)],'k--');
                hold on;
                for i=1:AC.data.sizeOfLabelingPool
                    %if we're out the regplane
                    if outputALLP(2,i)>obj.regPlaneBoundaries(2,2) || outputALLP(2,i)<obj.regPlaneBoundaries(2,1) || outputALLP(1,i)<obj.regPlaneBoundaries(1,1) || outputALLP(1,i)>obj.regPlaneBoundaries(1,2)
                        if (ismember(i,AC.trainingSet))
                            %if that was the last object to label
                            if i==AC.trainingSet(end)
                                plot(outputALLP(1,i),outputALLP(2,i),'*','Color',[0/255,1, 1]);
                                [~,gt] = AC.data.getLabels(i);
                                plot(gt(1),gt(2),'k*');
                                plot([outputALLP(1,i) gt(1)],[outputALLP(2,i) gt(2)],'k');
                                plot(outputALL(1,i),outputALL(2,i),'*','Color',[101/255, 24/255, 160/255]);
                                plot([outputALL(1,i) gt(1)],[outputALL(2,i) gt(2)],'k');
                            else
                                plot(outputALLP(1,i),outputALLP(2,i),'*','Color',[1, 69/255, 0]);
                                [~,gt] = AC.data.getLabels(i);
                                plot(gt(1),gt(2),'*','Color',[200/255,1,200/255]);
                                plot([outputALLP(1,i) gt(1)],[outputALLP(2,i) gt(2)],'b');
                            end
                        else
                            plot(outputALLP(1,i),outputALLP(2,i),'Color',[1,200/255,200/255]);
                        end
                    else %we're in the bounds
                        if (ismember(i,AC.trainingSet))
                            if i==AC.trainingSet(end)
                                plot(outputALLP(1,i),outputALLP(2,i),'*','Color',[0, 1, 1]);
                                [~,gt] = AC.data.getLabels(i);
                                plot(gt(1),gt(2),'k*');
                                plot([outputALLP(1,i) gt(1)],[outputALLP(2,i) gt(2)],'k');
                                plot(outputALL(1,i),outputALL(2,i),'*','Color',[101/255, 24/255, 160/255]);
                                plot([outputALL(1,i) gt(1)],[outputALL(2,i) gt(2)],'k');
                            else
                                plot(outputALLP(1,i),outputALLP(2,i),'*','Color',[1, 69/255, 1]);
                                [~,gt] = AC.data.getLabels(i);
                                plot(gt(1),gt(2),'*','Color',[200/255,1,200/255]);
                                plot([outputALLP(1,i) gt(1)],[outputALLP(2,i) gt(2)],'b');
                            end
                        else
                            plot(outputALLP(1,i),outputALLP(2,i),'*','Color',[200/255,200/255,1]);
                        end
                    end
                end
                
                if (readyPredictor{1}.isDistribution)
                    title(['Variance before:' num2str(dP(AC.trainingSet(end))) ', ' num2str(dP(length(dP)/2+AC.trainingSet(end))) ' after: ' num2str(d(AC.trainingSet(end))) ', ' num2str(d(length(d)/2+AC.trainingSet(end))) ]);
                else
                    title('Choice plot');
                end
                
                obj.setLimitsForPlot(0.5);
                obj.saveAction(obj.choicePlot,'choicePlot','',ind);
                hold off;                
            end
            
            %% plot history
            if (obj.historyAll > 0)
                fprintf('Plot: history plot on all data (%d) \n',obj.historyAll);
                set(0,'currentfigure',obj.figureforHistoryAll);
                %close and reopen of figure because it was incredibly slow without this.                
                close(gcf);
                obj.figureforHistoryAll = figure('name','History all objects');
                %plot reg plane bounds
                plot([obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,1)],[obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,1)],'k--');
                hold on;
                plotPredictionHistory(AC.predictedPositions, 1:AC.data.sizeOfLabelingPool, AC.trainingSet,AC.nofStartCells, AC.testSet,1,0,100);
                obj.setLimitsForPlot(0.5);
                obj.saveAction(obj.historyAll,'history','all',ind);                                
                hold off;
            end
                        
            if (obj.historyTest > 0)
                fprintf('Plot: history on test data (%d) \n',obj.historyTest);
                set(0,'currentfigure',obj.figureforHistoryTest);
                close(gcf);
                obj.figureforHistoryTest = figure('name','History on test set');                
                %plot reg plane bounds
                plot([obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,1)],[obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,1)],'k--');
                hold on;
                plotPredictionHistory(AC.predictedPositions, AC.testSet, AC.trainingSet,AC.nofStartCells, AC.testSet,0,0,200);
                obj.setLimitsForPlot(0.5);
                obj.saveAction(obj.historyTest,'history','test',ind);                
                hold off;
            end
            
            if (obj.historyTestSub > 0)
                fprintf('Plot: history on subset of test data (%d) \n',obj.historyTestSub);
                set(0,'currentfigure',obj.figureforHistoryTestSub);                
                close(gcf);
                obj.figureforHistoryTestSub = figure('name','History on 10 elements subset of test set');
                %plot reg plane bounds
                plot([obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,1) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,2) obj.regPlaneBoundaries(1,1)],[obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,2) obj.regPlaneBoundaries(2,1) obj.regPlaneBoundaries(2,1)],'k--');
                hold on;
                if (isempty(obj.subsetForTest))
                    selection = randperm(length(AC.testSet));
                    AC.plots.subsetForTest = selection;
                else
                    selection = obj.subsetForTest;
                end
                plotPredictionHistory(AC.predictedPositions, AC.testSet(selection(1:10)), AC.trainingSet,AC.nofStartCells, AC.testSet,0,1,1000);
                obj.setLimitsForPlot(0.5);
                obj.saveAction(obj.historyTestSub,'history','testsub',ind);                
                hold off;
            end
            
        end
        
        %% Final plots
        function finalPlots(obj,AC)
            if obj.finalMeanTestError > 0
                figure('name','Mean of mean errors over multiple runs.');
                plot(mean(AC.multipleTestError));
                if obj.finalMeanTestError > 1
                    if (~exist(['..' filesep 'lastExperimentResults' filesep 'finalPlots'],'dir'))
                        mkdir(['..' filesep 'lastExperimentResults' filesep 'finalPlots']);
                    end
                    saveas(gcf,['..' filesep 'lastExperimentResults' filesep 'finalPlots' filesep 'meanTestError.jpg'])
                else
                    pause(0.01);
                end
            end
            if obj.finalMeanOverAllUncertaintyByCovariance>0
                figure('name','Mean of OAUC over multiple runs.');
                plot(mean(AC.multipleUncertaintyByCovariance));
                if obj.finalMeanOverAllUncertaintyByCovariance > 1
                    if (~exist(['..' filesep 'lastExperimentResults' filesep 'finalPlots'],'dir'))
                        mkdir(['..' filesep 'lastExperimentResults' filesep 'finalPlots']);
                    end
                    saveas(gcf,['..' filesep 'lastExperimentResults' filesep 'finalPlots' filesep 'meanOAUCCov.jpg']);
                else
                    pause(0.01);
                end
            end
            if obj.finalMeanOverAllUncertaintyByPredictor>0
                figure('name','Mean of OAUC over multiple runs.');
                plot(mean(AC.multipleUncertaintyByPredictor));
                if obj.finalMeanOverAllUncertaintyByPredictor > 1
                    if (~exist(['..' filesep 'lastExperimentResults' filesep 'finalPlots'],'dir'))
                        mkdir(['..' filesep 'lastExperimentResults' filesep 'finalPlots']);
                    end
                    saveas(gcf,['..' filesep 'lastExperimentResults' filesep 'finalPlots' filesep 'meanOAUCPred.jpg']);
                else
                    pause(0.01);
                end
            end
            if obj.finalMeanTestOverAllUncertaintyByCovariance>0
                figure('name','Mean of OAUC over multiple runs.');
                plot(mean(AC.multipleTestUncertaintyByCovariance));
                if obj.finalMeanTestOverAllUncertaintyByCovariance > 1
                    if (~exist(['..' filesep 'lastExperimentResults' filesep 'finalPlots'],'dir'))
                        mkdir(['..' filesep 'lastExperimentResults' filesep 'finalPlots']);
                    end
                    saveas(gcf,['..' filesep 'lastExperimentResults' filesep 'finalPlots' filesep 'meanOAUCTestCov.jpg']);
                else
                    pause(0.01);
                end
            end
        end
        
        %% closeall function
        function closeall(obj)
        %If we don't need anymore our figures we can close all the left
        %ones.
            if ishandle(obj.figure1forVariance)
                close(obj.figure1forVariance);
            end
            if ishandle(obj.figure2forVariance)
                close(obj.figure2forVariance);
            end        
            if ishandle(obj.figureforErrorVectors)
                close(obj.figureforErrorVectors);
            end 
            if ishandle(obj.figureforErrorVectorsTest)
                close(obj.figureforErrorVectorsTest);
            end
            if ishandle(obj.figureforVarianceOnTestSet)
                close(obj.figureforVarianceOnTestSet);
            end
            if ishandle(obj.figureforErrorOnTestSet)
                close(obj.figureforErrorOnTestSet);
            end
            if ishandle(obj.figureforRegressionPlane)
                close(obj.figureforRegressionPlane);
            end
            if ishandle(obj.figureforRegressionPlaneStar)
                close(obj.figureforRegressionPlaneStar);
            end
            if ishandle(obj.figureforChoicePlot)
                close(obj.figureforChoicePlot);
            end
            if ishandle(obj.figureforHistoryAll)
                close(obj.figureforHistoryAll);
            end
            if ishandle(obj.figureforHistoryTest)
                close(obj.figureforHistoryTest);
            end
            if ishandle(obj.figureforHistoryTestSub)
                close(obj.figureforHistoryTestSub);
            end
        end
                
        function setLimitsForPlot(obj,percentage)
        %setLimitsForPlot based on the regression plane boundaries stored in
        %obj.regPlaneBoundaries we set fixed limits for the plots so that
        %they look like much better if we make a video out of the pictures.
        %The limits are the reg plane bounds extended with percentage
        %of the size in each direction. (it's given in real numbers
        %obviously and not in percents) It works on the actual fig.
        
           %we cut off the image for better view in video and to have
           %fixed positions
           regPlaneSizeInX = obj.regPlaneBoundaries(1,2)-obj.regPlaneBoundaries(1,1);
           regPlaneSizeInY = obj.regPlaneBoundaries(2,2)-obj.regPlaneBoundaries(2,1);
           axis([obj.regPlaneBoundaries(1,1)-regPlaneSizeInX.*percentage,obj.regPlaneBoundaries(1,2)+regPlaneSizeInX.*percentage,obj.regPlaneBoundaries(2,1)-regPlaneSizeInY.*percentage,obj.regPlaneBoundaries(2,2)+regPlaneSizeInY.*percentage]);
        end
                
        function saveAction(~, bool,dir, pref, ind)
        %saveAction The save procedure for any figure. It saves the actual
        %figure if the bool is at least 2 to the
        %../lastExperimentResults/[dir] folder with [pref] prefix and named 
        %[ind].jpg. (the ind number will be 3 digits long)
        %if we not require save then we pause the program for 0.01 second
        %which is enough time for the plot to refesh itself thus the plot
        %can be seen.
        
            %save if required
            if (bool > 1)
                %make directory if it's not ready yet
                if (~exist(['..' filesep 'lastExperimentResults' filesep dir],'dir'))
                    mkdir(['..' filesep 'lastExperimentResults' filesep dir]);
                end
                saveas(gcf,['..' filesep 'lastExperimentResults' filesep dir filesep pref '_' num2str(ind,'%3d')  '.jpg']);
            else
                pause(0.01);
            end                                        
        end
                
    end
    
end

