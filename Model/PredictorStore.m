classdef PredictorStore < handle
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	PredictorStore
%   
% It is a container class which stores predictors and
% also the indeces of the objects on which the predictor was trained.
%   This class is used for speeding, as if we already trained a
%   predictor for the specific training set we will not train another
%   one.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
        
    properties
        %this is a cellarray with as many elements as the
        %predictors cellarray has, and stores the arrays of the training
        %sets on which the corresponding predictor was trained.
        mapping
        
        %a cellarray with the previously trained predictors.
        predictors
                
    end
    
    methods        
        function [isAlreadyTrained, predictor] = getPredictor(obj,trainingSet, isDistribution)
        % Investigates the store whether there is an appropriate predictor
        % in it. Appropriate means a predictor trained on the trainingSet
        % input, and it must match the requirement for the distributivity
        % condition. Without the isDistribution parameter it gives back both
        % the distributive and non distributive ones.
        % isAlreadyTrained output variable indicates whether the store has
        % the required predictor. If it's true then the predictor is in the
        % predictor cellarray. (It is a cellarray because if there are
        % multiple appropriate predictors we give back all of them)
            isAlreadyTrained = 0;
            j=1;
            predictor = cell(1,length(obj.mapping));
            if nargin>2
                distributionNotImportant = 0;
            else
                distributionNotImportant = 1;                
            end
            for i=1:length(obj.mapping)
                if isequal(convert2RowVector(sort(obj.mapping{i})),convert2RowVector(sort(trainingSet))) && (distributionNotImportant || obj.predictors{i}.isDistribution == isDistribution)
                    isAlreadyTrained = 1;
                    predictor{j} = obj.predictors{i};
                    j = j+1;
                end
            end
            for i=j:length(obj.mapping)
                predictor(end) = [];
            end
        end
        
        %If we did not found an appropriate predictor with the previous
        %function we can add a new one to the predictorStore.         
        function addPredictor(obj,trainingSet,predictor)
            obj.mapping{end+1} = trainingSet;
            obj.predictors{end+1} = predictor;
        end
    end
    
end

