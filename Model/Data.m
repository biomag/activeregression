classdef (Abstract) Data
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	Data
%   
%DATA Interface for all data source we use in our AL strategies
%   The Data class is abstract, it much more like an interface in fact.
%   All the methods in it must be implemented for different data
%   sources just like: synthetic data, or CommonHandles
%       What we require from 'data' is:
%          1. It must have a large labeling pool, from which we can
%          choose objects to predict or to show it to the oracle.
%          2. All the objects must be identified with a unique number
%          with which we can make references to each objects.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
        
    properties
        %The size of the data. This is the number of data objects.
        sizeOfLabelingPool
        
        % RealLabels. The already labeled cells. This is an array with the
        % indeces of the cells for which we have labels.    
        realLabels
        
        % For plots and sometimes for other purposes we need to know the
        % theoretical bounds for our regression plane. Usually anybody who
        % use it should have this information if we label by human then the
        % labeling screen has some bounds (if you don't want to handle it
        % as infinite space) if we label on artifical data then the
        % generated ground truth also has limits. The format is string but
        % the string is a matrix with MATLAB syntax. Each row has 2
        % elements the minimum and maximum for the rowth dimension. It is
        % converted later to matrix.
        bounds
    end
    
    methods (Abstract)
        % This function is for labeling. This is with which we extend our
        % information, we put new labels into the data. Index is the index
        % of the cell you intend to label and targets are the corresponding
        % targets.
        obj = label(obj,index,targets)
        
        %Get the already labeled objects data: the input and the targets.
        %We can filter the result data with specialization array which
        %contains indeces. We only give back the labeled objects which are
        %in the specialization array. Only labeled data can be queried
        %here.
        [input, target] = getLabels(obj,specialization)
        
        %Get input data from the labeling pool. With specialization you can
        %filter from it.
        [input, img] = getData(obj, specialization)
    end
    
end

