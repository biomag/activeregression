classdef ALalgorithm < handle
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	ALalgorithm
%   
%ALALGORITHM Interface for our Active Learning methods.
%   An Active learning method is basically a sample choosing algorithm,
%   which tries to select the best next training sample from the
%   iterative cycle of active learning.
%
% NOTE: The class must have a constructor which waits for only one
% parameter a cellarray, which follows the format given back by the
% getParameters static method.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
    
    
    properties                        
    end
    
    methods (Abstract)
        
        %nextCell Implements the query strategy
        % This function is responsible for selecting the next cell to be
        % labelled from the set of all cells based on their features.
        %
        % INPUTS:
        %   predictor    a brand new predictor which can be trained if the
        %                appropriate predictor is not found in the
        %                predictor store. 
        %   predictorStore a container of the previously trained
        %                predictors.
        %   data         the data object which contains the labels and
        %                inputs
        %   trainingSet  array of indeces which objects we used to train
        %                previous predictor. We are allowed to train
        %                predictor only from this set. There are other
        %                labeled objects in data but they are
        %                reserved for testing our method.
        %   testSet      the indeces of other labeled objects which cannot
        %                be used for training. (not neccessary to use)
        %   
        % OUTPUTS:
        %   nextCell     an array of indeces in ranking order. The first
        %                cell is the most informative according to the
        %                ALalgorithm and so on.
        %   predictorStore if the ALalgorithm needed new predictor to
        %                train than it was stored in the predictorStore and
        %                we give back for further usage.
        [nextCell] = nextCell(obj,predictor,predictorStore,data,trainingSet,testSet)
        
        %reset Reinitialize learners
        %   This is a reinitialization method, for AL algorithms. If we
        %   want to start a totally new cylce with the same algorithm we do
        %   not need to construct another one, but we can use this function
        %   to reset all the parameters in the algorithm.
        reset(obj)
        
        %name Fetch the name and properties of the class
        % This method is quite similar to the toString method but it gets
        % the class's name, with the most important extra features.
        [string] = name(obj)
        
        % getParameterValues Query for the values of user parameters
        % This functions returns the actual parameters of the active
        % learner in a similar format to getParameters function. See the
        % output of fetchUIControlValues.
        % 
        % OUTPUTS:
        %   paramarray       a cellarray with the parameter values. The order 
        %                    of the parameters is the same as in the
        %                    getParameters function.
        %
        %   See also generateUIControls, fetchUIControlValues, fillOutUIControls
        paramarray = getParameterValues(obj);
    end    
    
    methods (Abstract, Static)
        % getParameters To define the user-tunable parameters
        % This function is used to define the user-definable parameters for
        % the module. ACC is going to create an approriate GUI for asking
        % the parameters from the users defined here. The output must
        % follow the convention of the matlab_settings_gui (located in
        % ACC\Utils\ActiveRegression\Utils\matlab_settings_gui) or
        % alternatively at https://github.com/szkabel/matlab_settings_gui.
        %
        % The single output parameter should be matching with the first
        % input of generateUIControls function.
        %
        %   See also generateUIControls, fetchUIControlValues, fillOutUIControls
        paramarray = getParameters();
    end
    
    methods (Static)        
        function [bool,msg] = checkParameters(~)
        % checkParameters To check the user provided parameters
        %  Checks for a parameter array wheter it is a valid constructor
        %  parameter list in the form described above. It has a default
        %  constant true implementation but it can be overwritten in the
        %  child classes.
        %
        %  INPUT:
        %   paramcell       the parameter values to be checked. Matches
        %                   with the output of fetchUIControlValues.
        %
        %  OUTPUTS:
        %   bool            a boolean integer indicating the validness of
        %                   the parameters
        %   msg             an error msg which will be displayed to the
        %                   user if the parameters are not correct.
        %
        %  We note that the int typed parameters are checked to be numbers
        %  (can be double also!), and the ENUMS can get only values which
        %  are listed in teh values array. Any other checks must be
        %  implemented here.
        %
        %   See also generateUIControls, fetchUIControlValues, fillOutUIControls
            bool = 1;
            msg = '';
        end
    end
    
end

