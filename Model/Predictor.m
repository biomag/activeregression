classdef (Abstract) Predictor
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	Predictor
%   
% Interface for all Predictors (Regression models)
%
%   The Predictor Class represents the Machine Learning Models in our
%   framework. It is Abstract, and defines the member function headers that
%   are required for all the children classes (Predictor implementations).
%   As this is a regression based framework, the output variables are
%   always continuous in our case. To maintain generality, the methods must
%   be able to provide multiple outputs (e.g. 2D regression). If a
%   Predictor is not naturally extended to multiple output D then
%   independent models are built for the separate output dimensions.
%
%   NOTE: Predictor classes must implement a default constructor to work
%   without errors in the framework.

%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


    properties        
        %We need to know of all types of predictors whether they are
        %capable to predict discrete points or they can give us variances
        %(i.e. uncertainty for regression).
        %This functionality is stored in the isDistribution variable.
        %   TYPE: bool
        isDistribution = false;
             
        %Ususally a predictor needs a model to store information
        %gained from the trainnig examples, based on which it can make the
        %predictions
        %   TYPE: arbitrary, defined by the model itself
        model = [];
                
        %It can be essential to the predictor to know the theoretical
        %bounds of the data, i.e. what is the expected region of outputs.
        %   TYPE: String, containing a MATLAB syntax matrix with size n by 2.
        %         Each row corresponds to an output dimension, first column
        %         is the minimum value, second column is maximum of the
        %         range
        databounds = '[0 1000; 0 1000]';
    end
    
    methods (Abstract)                
        %TRAIN Function to train models, i.e. estimate model parameters, or
        %hyperparameters. Encapsulates the process of learning.
		%
		% IMPORTANT NOTE: the class is not a handle (not reference based),
		% i.e. simply calling this function for an instance of the class
		% will not 'make the model to be trained'. The output of this class
		% has to be given back as value for the instance.
		%        
        % INPUTS:
        %   inputs:  An array containing numerical values, representing the
        %            features (independent variables) of the training data.
        %            Columns refers to observations, rows refer to
        %            variables.      
        %   targets: An array containing numerical values, representing the
        %            targets (dependent variables) if the training data.
        %            Columns refer to observations, rows to output variables.
        %            The number of columns must match with the inputs input
        %            variable.       
        %
        % @ref: see also the Predictor function, which is the interface for
        % predicting the data
        obj = train(obj,inputs,targets)                   		
        
        %PREDICTOR Function to predict the models, i.e. to apply the
        %trained model on previously unseen data.
		%        
        %   INPUT:
        %       inputs: Numerical array similar to the inputs variable of
        %               the train member function.
        %
        %   OUTPUTS:
        %       outputs: Numerical array similar to the targets input
        %                variablee of the train member function. These
        %                values are the 'deterministic' output of the
        %                predictor, i.e. they do not give us
        %                probabilities/distribution (uncertainties). If the
        %                predictor is capable to predict distribution then
        %                these variables refer to the means of the
        %                distribution, and the second output argument is
        %                the variance for the distriubtion.
        %       'variances': Optional output, if the predictor is capable
        %                of providing distributions, indicated by its
        %                isDistribution member variable. If provided then
        %                it is a full covariance matrix, for each output
        %                dimension. If the output dimension is d and the
        %                number of the new samples is n, then the variances
        %                matrix is an (n*d)x(n*d) matrix. It contains
        %                non-zero elements in the diagonal (n*n) blocks.
        %                These (n*d) blocks are the covariance matrices
        %                between the new samples for each dimension.
        %                Sometimes these covariance matrices are estimated
        %                with diagonal matrices supposing that the
        %                covariance between different samples is 0. ==>
        %                only the variance values will be displayed in
        %                these cases in the diagonal.
        %
        % @ref: see also the Train function, which is the interface for
        % training models.
        [outputs,variances] = predict(obj, inputs)		
                
        %This is essentially a toString function. It must accomplish the
        %requirement that it provides all the information of the Predictor,
        %not only it's name but all the parameters used. (mostly all type
        %information of the model, the used covariance functions, training
        %ratios etc., but we don't need the trained hyperparameter values.)
        %These properties can be long so for better format results this
        %function prints out the requested data to the provided file handle
        %which should be opened for writing or appending beforehand.
        %
        %   INPUT:
        %       f:  A file handle opened for writing or appending.
        %                
        print2file(obj,f);
    end
    
    methods        
        function obj = setParameters(obj,~)        
        %If the model provides parameters of itself, then with this
        %function the appropriate fields should be filled. 
        %   INPUTS:
        %       paramcell:  A structure corresponding to the description
        %                    variable provided as the output of
        %                    getParameters member function. For more info
        %                    plaes check fetchUIControlValues
        end
        
        function paramcell = getParameterValues(obj)
        %If the model requires user-defined parameters then this function
        %provides the parameters that were used to instantiate the
        %Predictor. This is essential if we would like to train a similar
        %predictor from scratch (e.g. in cross validation).
        %   INPUT:
        %       obj:        The object itself
        %
        %   OUTPUT:
        %       paramcell   A cellarray exactly as long as the
        %                   getParameters function's output.        
            paramcell = cell(1,length(eval([ class(obj) '.getParameters'] )));
        end
    end
    
    methods (Static)        
        function [bool,msg] = checkParameters(~)
        %For stability reasons the predictors must be able to verify that a
        %given parameter list is valid or not. This function is only
        %static (not abstract) and provides a general true implementation
        %but this can be (and suggested to be in case of using parameters)
        %overwritten by the specific predictors.                
        %
        % INPUT:
        %   paramcell   A cellarray corresponding to getParameters, see the
        %               output of fetchUIControlValues.
        %
        % OUTPUT:
        %   bool        Boolean value indicating if the parameters are
        %               CORRECT (true) or not (false).
        %   msg         If the parameters are not correct then in the msg
        %               string the function should provide information
        %               about the problem
            bool = 1;
            msg = '';
        end        
    
        function paramarray = getParameters()
        %Each method can specify what parameters it needs in a cellaray
        %format. See generateUIcontrols (Located in Utils\matlab_settings_gui)
        %for further info on the format.
        %  NOTE: In case of requesting user defined parameters you MUST
        %  translate the output format of fetchUIControlValues to
        %  actual properties of this class in the setParameters function.
        %  
            paramarray = {};
        end
        
    end
    
end