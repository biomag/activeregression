classdef Labeling
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	WekaMLP
%   
% LABELING With this class we provide an interface to label objects.    
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
    
    properties
    end
    
    methods (Abstract)
        % The label function which gives back the targets, the information
        % about the object.
        % Parameters: 
        %       idx is the unique identifier for the cell in the data model            
        %       inp are the features for it and img is the picture of the
        %       object. The fourth parameter could be neglected.
        %       data is the appropriate data class in which we can have
        %       some information for the labeling. e.g: it is good to see
        %       where were the previous training examples to have
        %       comparison
        targ = label(obj,idx,data,inp,img)
    end
    
end

