classdef QueryByCommittee < ALalgorithm
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	QueryByCommittee
%   
% QueryByCommittee is an AL algorithm. It targets the predictions error
%with training more then one predictor on each training set.
%   This method splits the current training set into partitions, which
%   are overlapping, but not the same. On these partitions we train
%   predictors and as the training was different the predictions will
%   be different too on each cell in the labeling pool. We'll get as
%   many predictions for each cell, as many partitions we used. The
%   ranking of the cell in this AL algorithm is based on the variation
%   of these predictions. The predictors trained on the different
%   partitions compose the Committee, they are the Committee Members.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.            
    
    properties
        %# of CommitteeMembers
        nofCommitteeMembers
    end
    
    methods (Static)
        function paramarray = getParameters()
            paramarray{1}.name = 'Size of commiittee';
            paramarray{1}.type = 'int';            
        end
        function [bool,msg] = checkParameters(paramcell)
            if isempty(paramcell{1}) %the fetchUIalready tried to convert
                bool = 0;
                msg = 'The first parameter has to be a number!';
            elseif (rem(paramcell{1},1)~=0)
                bool = 0;
                msg = 'The first parameter has to be integer!';
            elseif (numel(paramcell{1})~=1)
                bool = 0;
                msg = 'The first parameter has to be scalar!';
            elseif (paramcell{1}<2)

                bool = 0;
                msg = 'The size of committee must be at least 2!';                
            else
                bool = 1;
                msg = '';
            end
        end
    end
    
    methods
        function [obj] = QueryByCommittee(paramcell)
            obj.nofCommitteeMembers = paramcell{1};
        end
        
        function paramarray = getParameterValues(obj)
            paramarray{1} = obj.nofCommitteeMembers;
        end
        
        function [nextCell] = nextCell(obj,predictor,predictorStore,data,trainingSet,~)
            % ***** Initialization part
            
            % Making a random order of the cells in the training set. 
            order = randperm(length(trainingSet));
            % memory allocation
            outputs = cell(1,obj.nofCommitteeMembers);
            %determine outDimension from data.
            [~, target] = data.getLabels();
            oneCellPrediction = zeros(size(target,1),obj.nofCommitteeMembers);
            %Initalizing dispersion
            d=zeros(1,data.sizeOfLabelingPool);
            % All the inputs, the unlabeled and labeled objects.
            allInp = data.getData();

            
            % END OF INIT *****

            %We make nofComMembers piece model.
            for i=1:obj.nofCommitteeMembers
                %We make portions of the training data.
                cellToPredict = Portion(trainingSet(order),obj.nofCommitteeMembers,i);                
                
                %Making a model.
                [ trainedPredictor] = getPredictorMacro(predictor, data, predictorStore, cellToPredict);                             

                %We predict all the cells using this net.                
                outputs{i} = trainedPredictor{1}.predict(allInp);
            end          

            %Now we calculate the variance of the committee members.
            for i=1:data.sizeOfLabelingPool
                               
                for j=1:obj.nofCommitteeMembers
                    oneCellPrediction(:,j) = outputs{j}(:,i);
                end                
                Mean = repmat(mean(oneCellPrediction,2),1,obj.nofCommitteeMembers);                

                d(i) = distRMSE(oneCellPrediction,Mean);                
            end

            %Finally we specify the next_cell, choosing the biggest dispersion.

            [~, nextCell] = sort(d,'descend');
            

        end
        
        function reset(~)
        end
        
        function [string] = name(obj)
            string = strcat(class(obj),num2str(obj.nofCommitteeMembers));
        end
    end
    
end

