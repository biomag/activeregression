classdef OutOfBounds < ALalgorithm
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	OutOfBounds
%   
% OutOfBounds This ALalgorithm targets the extreme cells, those ones,
% which are predicted out of the regression plane (space).
%   Before the AL cycle, we provide a labeling enviroment for the expert
%   called the regression plane (well it could be a line or a cube too,
%   but usually a regression plane) and we expect all the labels to be
%   in this regression plane. The nature of the regression methods does
%   not garantee to have all the predicted labels within these bounds
%   and in some way it is interesting to target the objects which are
%   predicted out of this pre-given space. Once all the predictions are
%   inside the regression plane we can choose the ones which are close
%   to the edges.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
        
    properties
        %This is an N by 2 array containing the bounds of our regression
        %plane. N is the number of output dimensions, the first column is
        %the minimum value for the specific dimension the second one is the
        %maximum. e.g.: in the ACC regression plane (version used in 2015.
        %06.) the bounds array is [0 1000; 0 1000] as it is an 1000x1000
        %pixels regression plane.
        bounds
        
        % The first dimension of the bounds array indicates the
        % outDimension which can be stored.
        outDimension
    end
    
    methods (Static)
        function paramarray = getParameters()            
            paramarray = {};
        end
    end
    
    methods                               
        function [obj] = OutOfBounds(bounds)            
        % the bounds will be given in a string format so it must be
        % converted to an array described in the properties. The string
        % must describe in matlab syntax the elements of the bounds matrix.
            if ~isempty(bounds)
                bounds = str2num(bounds{1}); %#ok<ST2NM> We need to convert 2 matrix.
                obj.bounds = bounds;
                obj.outDimension = size(bounds,1);
            else
                obj.bounds = [0 1000; 0 1000];
                obj.outDimension = 2;
            end
        end
                
        function [nextCell] = nextCell(obj,predictor,predictorStore,data,trainingSet,~)
        % The ranking method:
        %   First we targets the cells which are out of the bounds, and
        %   from them those one gets the highest ranks, which are the
        %   further away from the edges. The distance is measured in
        %   Manhattan distance. We ensure to be the cells out of bounds the
        %   first in the ranking by adding a value more than the maximum
        %   Manhattan distance in the regression plane to them. Then for
        %   a cell inside the regression plane the base of the ranking
        %   is the Manhattan distance from the corner which is the closest
        %   to it.
            %getting input features for all data
            allInp = data.getData();
            %get access to a predictor trained on the current training set
            [readyPredictor] = getPredictorMacro( predictor, data, predictorStore, trainingSet);
            %predict all the cells with this predictor
            outputs = readyPredictor{1}.predict(allInp);
                        
            baseOfTheRankDistances = zeros(1,data.sizeOfLabelingPool);                        
                       
            for i=1:data.sizeOfLabelingPool
                isThisObjectOutOfBound = 0;                
                distanceFromTheEdges = 0;
                for k=1:obj.outDimension
                    if outputs(k,i)<obj.bounds(k,1)
                        isThisObjectOutOfBound = 1;
                        distanceFromTheEdges = distanceFromTheEdges + (obj.bounds(k,1)-outputs(k,i));
                    elseif outputs(k,i)>obj.bounds(k,2)
                        isThisObjectOutOfBound = 1;
                        distanceFromTheEdges = distanceFromTheEdges + (outputs(k,i)-obj.bounds(k,2));
                    end
                end
                if isThisObjectOutOfBound                                        
                    baseOfTheRankDistances(i) = distanceFromTheEdges + sum(obj.bounds(:,2)-obj.bounds(:,1));
                else
                    %The object is inside the regression plane                                        
                    distFromTheEdges = zeros(obj.outDimension,2);
                    %Calculate the the distance of the object from each
                    %bound. The distFromTheEdges array is an outDimension
                    %by 2 array storing for every output dimension the
                    %upper and lower bounds. (Manhatten distance)
                    for k=1:obj.outDimension
                        distFromTheEdges(k,1) = outputs(k,i) - obj.bounds(k,1);                        
                        distFromTheEdges(k,2) = obj.bounds(k,2) - outputs(k,i);                        
                    end
                    
                    %If the regression plane has n dimensions it has 2^n
                    %corners. We calculate the manhatten distance from
                    %every corner and choose the minimum one. This minimum
                    %will be the base of the ranking.
                    distFromCorners = zeros(1,(2^obj.outDimension));
                    for j=0:(2^obj.outDimension)-1
                        distFromOneCorner = 0;
                        str = dec2bin(j,obj.outDimension);
                        for k=1:obj.outDimension
                            distFromOneCorner = distFromOneCorner + distFromTheEdges(k,str2double(str(k))+1);
                        end
                        distFromCorners(j+1) = distFromOneCorner;
                    end
                    baseOfTheRankDistances(i) = max(distFromCorners);
                end
            end
            
            [~,nextCell] = sort(baseOfTheRankDistances,'descend');
                                    
        end
        
        function reset(~)
        end
        
        function [string] = name(obj)
            s = num2str(obj.bounds);
            s(:,end+1)=' ';
            for i=1:size(s,1)-1
                if (i==1)
                    s(i,end)= '-';
                else
                    s(i,end)= '-';
                end
            end            
            string = strcat(class(obj),'_',strrep(reshape(s',[1 size(s,2)*2]),'  ','_'));
        end
        
        function paramarray = getParameterValues(~)
            paramarray = cell(0);
        end
    end
    
end

