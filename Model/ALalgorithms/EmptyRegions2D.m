classdef EmptyRegions2D < ALalgorithm
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	EmptyRegions2D
%
% This is an AL algorithm targeting the variance of the objects.
% We are trying to find all type of objects, or in other words the
% objects which are not similar to the previously seen ones. WARNING: it
% works only in 2D!!!
%   Based on the current training set we train a predictor and run it
%   on the full labeling pool, and by this getting prediction for all
%   cells. We put a gaussian distribution for all these predictions
%   points (using the point as the mean of the distribution). This is
%   called Kernel Density estimation and we use kde function.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
 
    properties
    end
    
    methods (Static)
        function paramarray = getParameters()
            paramarray = cell(0);
        end
    end
    
    methods
        function obj = EmptyRegions2D(~)
        end
        function paramarray = getParameterValues(~)
            paramarray = cell(0);
        end
        function [nextCell] = nextCell(~,predictor,predictorStore,data,trainingSet,~)
             % ***** INIT part
            %memory allocation
            d = zeros(1,data.sizeOfLabelingPool);
            
            %Get all data from the pool
            allInp = data.getData();            
            
            % END of INIT part *****
            
            % get a predictor for the current training set
            [ trainedPredictor] = getPredictorMacro( predictor, data, predictorStore, trainingSet);                        
            outputs = trainedPredictor{1}.predict(allInp);                        

            %Specify the bounds of the predictied labels. => this will specify the
            %space in which we'll make the KDE
            MIN = min(outputs,[],2);
            MAX = max(outputs, [],2);
            
            %Getting the labels of the training data.
            [~, targets] = data.getLabels();

            % Making KDE
            try
                [~,density,~,~]=kde2d(targets', 1024, MIN', MAX');
                imagesc(density);
            catch exc
                getReport(exc)
                fprintf('Making KDE was unsuccesful during the %d. run.\n',j);
                
                density=zeros(1024);
            end

            %Finding the least dense position.
            [~,MIN_POS,~,~] = Limits(density);

            %Convert the density "space" to the target space
            MIN_POS(1) = MIN(1) + ( ( MAX(1) - MIN(1)) / 1024 * MIN_POS(1) );
            MIN_POS(2) = MIN(2) + ( ( MAX(2) - MIN(2)) / 1024 * MIN_POS(2) );

            %Calculating the minimum distance between the minimum position and
            %predicted values.
            for i=1:data.sizeOfLabelingPool                
                d(i) = pdist([(outputs(:,i))';MIN_POS]);
            end

            %Sorting
            [~,nextCell] = sort(d);
        end    
        
        function reset(~)
        end
        
        function [string] = name(obj)
            string = class(obj);
        end
    end
    
end

