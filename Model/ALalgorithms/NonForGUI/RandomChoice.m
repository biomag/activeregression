classdef RandomChoice < ALalgorithm
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	RandomChoice
%   
% RandomChoice is an AL algorithm. Chooses the next cell randomly.
%   RandomChoice is the reference for the AL algorithms, our true AL
%   algorithms should be better than this, as random choice is a
%   passive learning method.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
        
    properties
    end
    
    methods (Static)
        function paramarray = getParameters()
            paramarray = cell(0);
        end
    end
    
    methods
        function obj = RandomChoice(~)
        end
        
        function [nextCell] = nextCell(~,~,~,data,~,~)
            nextCell = randperm(data.sizeOfLabelingPool);
        end
        
        function paramarray = getParameterValues(~)
            paramarray = cell(0);
        end
        
        function reset(~)
        end
        
        function [string] = name(obj)
            string = class(obj);
        end
    end    
    
end

