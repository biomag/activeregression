classdef BiggestVariance < ALalgorithm
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	BiggestVariance
%     
% BiggestVariance is an AL algorithm. Stores previous predictions and
% choose the cell which showed the greatest variance in the past.
%   Committee members is a common AL strategy. This algorithm is quite
%   similar to that, because we train multiple predictors and based on
%   their predictions we make a ranklist. The only difference is that
%   the 'committee members' are not trained on the same splitted
%   training set portion, but during the AL process on the expanding
%   training set. <== The idea here was to save the predictions what
%   we've done before.
%   Because our knowledge is growing during the AL process we should
%   not consider all of the predictions with equal importance. That is
%   why we introduced the forgetting types or memory weightings.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.  
    
    properties
      %previousPredictions: which stores the results.
      %   This is a 3 dimensional array, first 2 dimension is the same as one
      %   output of our model (for example the target variable), and 3
      %   dimension is because we'd like to store more than one result.
      prevPred  
      
      % ForgettingType: 
      % This is the memory weighting type variable. It's possible values:
      %   'NONE': do not forget
      %   '10WINDOW': remember only the last 10 one.
      %   'LINEAR': forget by a linear function.
      %   'EXPONENTIAL': forget by an exponential function.
      forgetting
    end
    
    methods (Static)
        function paramarray = getParameters()
            paramarray{1}.name = 'Forgetting';
            paramarray{1}.type = 'enum';
            paramarray{1}.values = {'NONE','10WINDOW','LINEAR','EXPONENTIAL'};
        end
    end
    
    methods
        function obj = BiggestVariance(paramcell)
            obj.forgetting = paramcell{1};            
        end
        
        function paramarray = getParameterValues(obj);
            paramarray{1} = obj.forgetting;
        end
        
        function [nextCell] = nextCell(obj,predictor,predictorStore,data,trainingSet,~)
            
            % All the inputs, the unlabeled and labeled objects.
            allInp = data.getData();
            
            % If the previous predictions are empty we should fill it with
            % 2 predictions to have some base of the choosing.
            if size(obj.prevPred) == 0
                % Divide the dataSet into 2 part, because if we'd train on
                % the same data set, then all the predictions would be the
                % same.
                for i=1:2                    
                   trainSet = Portion(trainingSet,2,i);                   
                   [readyPredictor] = getPredictorMacro( predictor, data, predictorStore, trainSet);                   
                   allPred = readyPredictor{1}.predict(allInp);
                   obj.prevPred(:,:,i) = allPred;
                end                                                                
            end                
            
            %Here we have at least 2 predictions from the past.
            [readyPredictor] = getPredictorMacro( predictor, data, predictorStore, trainingSet);
                        
            obj.prevPred(:,:,end+1) = readyPredictor{1}.predict(allInp);
            
            % ***** INITIALIZATION PART *****
            
            %In j we store the number of previous predictions
            j = size(obj.prevPred,3);           
            
            %The output dimension
            outDim = size(obj.prevPred,1);
            
            %Initialize the temporary arrays for calculations.                       
            d = zeros(1,data.sizeOfLabelingPool);
            
             switch obj.forgetting
                case 'NONE'
                    start=1;
                case '10WINDOW'
                    if j>10
                        start=j-10;
                    else
                        start=1;
                    end
                case 'LINEAR'
                    start=1;
                case 'EXPONENTIAL'                        
                    start=1;
            end

            clearvars variance;
            clearvars X;
            oneCellPrediction = zeros (outDim,j-start+1);
            X = zeros(1,j-start+1);
            
            % ***** INITALIZATION PART ENDS *****
            
            %For all the cells calculate the variance of the previous
            %predictions. Forgetting included.
            for i=1:data.sizeOfLabelingPool                       
                %Transforming data from prevPred to calculate the variance.
                %onceCellPrediction stores outputs, but in better form.
                for k=start:j
                    oneCellPrediction(:,k)=obj.prevPred(:,i,k);
                end                
                Mean = repmat(mean(oneCellPrediction,2),1,size(oneCellPrediction,2));

                %Counting the error with different method.                
                if strcmp(obj.forgetting,'NONE') || strcmp(obj.forgetting,'10WINDOW')
                    d(i) = distRMSE(oneCellPrediction,Mean);
                elseif strcmp(obj.forgetting,'LINEAR')
                    % Linear obj.forgetting                   
                    for k=1:size(oneCellPrediction,2)
                        X(k) = pdist([oneCellPrediction(:,k)';Mean(:,k)'])*(k/j);
                    end
                    d(i) = sqrt(mean(X.^2));
                elseif strcmp(obj.forgetting,'EXPONENTIAL')
                    % exponential obj.forgetting                   
                    for k=1:size(oneCellPrediction,2)
                        X(k) = pdist([oneCellPrediction(:,k)';Mean(:,k)'])*exp(k-j);
                    end
                    d(i) = sqrt(mean(X.^2));
                end
            end
            
            [~, nextCell] = sort(d,'descend');
                                                
        end
        
        function reset(obj)
            obj.prevPred = [];
        end
        
        function [string] = name(obj)
            string = strcat(class(obj),obj.forgetting);
        end
    end
    
end

