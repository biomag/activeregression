classdef OverAllUncertaintySampling < ALalgorithm
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	OverAllUncertaintySampling
%
% OverAllUncertaintySampling
%   In Gaussian process the predicted variance is independent from the
%   actual position of the sample. It's only dependent from what we
%   consider as training and non-training sample. Using this feature we
%   can build up an active learning algorithm.
%       For every possible training set we can evaluate an overall
%       uncertainty value. This uncertainty value can be used as a
%       ranking value for the training set. Having a current training
%       set we wish to expand it with one object. The chosen object
%       should decrease the overall uncertainty value the most thus
%       we'll get the best next training set according to our ranking.
%       
%       To calculate the overall uncertainty value we need a covariance
%       function which has it's hyperparameters optimized on the
%       previous training set. The hyperparameters are optimized
%       separately on each output dimension, so we're adding up the
%       uncertainty values calculated with each hyperparameter.
% 
%       NOTE: This AL works only with GPML predictor. If we wish to use
%       another predictor than we should somehow modify the interfaces
%       to build up a standard form for the covariance functions, and
%       its hyperparameters.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.   
    
    properties  
        %the error measurement property is an ErrorMeasurement object. It
        %has many different function to calculate errors. As the error can
        %also be considered as a ranking for a specific training set, the
        %overallUncertaintySampling function is also placed in this class.
        errorMeasurement;
    end
              
    methods   
        
        %Constructor which requires an ErrorMeasurement object.
        function obj = OverAllUncertaintySampling(eM)
            if ~isempty(eM)
                obj.errorMeasurement = eM;
            else
                obj.errorMeasurement = ErrorMeasurement();
            end
        end
        
        function [nextCell] = nextCell(obj,predictor,predictorStore,data,trainingSet,testSet)                 
            if (isa(predictor,'GPMLPredictor'))
                                                                               
                %for every non training object
                nonTrainingObjects = setdiff(1:data.sizeOfLabelingPool,trainingSet);
                %the test set objects cannot be chosen either
                nonTrainingObjects = setdiff(nonTrainingObjects,testSet);
                fprintf('\n OverAllUncertaintyCalculation: \n');                                                                    
                OUCValues = obj.errorMeasurement.calculateOverAllUncertainty(predictor, predictorStore, data,trainingSet,nonTrainingObjects);                       
                fprintf('\n');
                
                %sort the OUCValues and select the 
                [~, ranking] = sort(OUCValues);
                
                nextCell = nonTrainingObjects(ranking);
                                
                
            else
                errordlg('The overAllUncertaintySampling can be used only with GPMLPredictor');
            end
            
            
        end
        
        function reset(obj)
            %empty function we do not need to reset anything.
        end
        
        function [string] = name(obj)
            string = class(obj);
        end
        
        function paramarray = getParameterValues(~)
            paramarray = cell(0);
        end
    end
    
    methods (Static)
        function paramarray = getParameters()
            paramarray = {};
        end
    end
    
end

