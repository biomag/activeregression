classdef UncertaintyBased < ALalgorithm
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	UncertaintyBased
%   
% This AL algorithm REQUIRES predictor with
% distribution. The base of the ranking is the variance parameter of the
% predicted distribution. The biggest variance will be the first in the
% ranking and so on.
%   We consider the variance of the distribution as measurement of the
%   uncertatinty, and we are interested in objects about which the
%   predictor is uncertain.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
        
    
    properties
        %There are many ways to compute one value from the variances given
        %in each dimension. We can take the mean of them, we can sum them,
        %we can choose the minimum or the maximum, etc. Values for this
        %member should be:
        %   MEAN
        %   SUM
        %   MIN
        %   MAX
        %   MUL
        type
    end
    
    methods (Static)
        function paramarray = getParameters()
            paramarray{1}.name = 'Combination of uncertainties';
            paramarray{1}.type = 'enum';
            paramarray{1}.values = {'MEAN','SUM','MIN','MAX','MUL'};
        end
    end
    
    methods
        function [obj] = UncertaintyBased(paramcell)
            obj.type = paramcell{1};
        end
        
        function paramarray = getParameterValues(obj)
            paramarray{1} = obj.type;
        end
        
        function [nextCell] = nextCell(obj,predictor,predictorStore,data,trainingSet,~)
            %Gettin the input for all the objects.
            allInp = data.getData();
            
            % getting access to a predictor trained on the current
            % trainingSet.
            if predictor.isDistribution == 1
                [readyPredictor] = getPredictorMacro( predictor, data, predictorStore, trainingSet, 1);
            else
                baseException = MException('MYEXC:distributionNeeded','Sorry, to use Uncertainty Sampling AL algorithm you need to have a predictor which can provide you distributions. e.g. GPMLpredictor');
                throw(baseException);
            end
            
            %Predict all the objects with this predictor.
            [outputs, vars] = readyPredictor{1}.predict(allInp);
            
            %Determining the output dimension from the outputs.
            outDimension = size(outputs,1);
            % Initialization for the used variables
            varValuesForOneObject = zeros(1,outDimension);            
            % This will store the joint base of the rank for all the cells.
            % The MEAN, SUM, MAX, MIN etc methods collects the information
            % to this array.
            baseOfTheRank = zeros(1,data.sizeOfLabelingPool);                        

            %For every cell           
            for i=1:data.sizeOfLabelingPool
                %Calculate the var values
                for j=1:outDimension
                    varValuesForOneObject(j) = vars((j-1)*data.sizeOfLabelingPool+i,(j-1)*data.sizeOfLabelingPool+i);
                end
                switch obj.type
                    case 'MEAN'                    
                        baseOfTheRank(i) = mean(varValuesForOneObject);
                    case 'SUM'                    
                        baseOfTheRank(i) = sum(varValuesForOneObject);
                    case 'MIN'                    
                        baseOfTheRank(i) = min(varValuesForOneObject);
                    case 'MAX'                    
                        baseOfTheRank(i) = max(varValuesForOneObject);
                    case 'MUL'
                        baseOfTheRank(i) = prod(varValuesForOneObject);
                end
            end
            
            [~,nextCell] = sort(baseOfTheRank,'descend');            
        end
        
        function reset(obj) %#ok<MANU> Because reset is in the interface and we should implement it, but this AL algorithm does not need any reset.
        end
        
        function [string] = name(obj)
            string = strcat(class(obj),obj.type);
        end
    end
    
end

