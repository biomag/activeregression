classdef GreatestDistance < ALalgorithm
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	GreatestDistance
%
% This is an AL algorithm targeting the most various
% objects. We try to find all types of objects.
%   As the AL cycle moves on, more and more training sample is
%   available from the regression space. But for sure the dense of the
%   training samples are not the same in all regions of this space. We
%   try to find objects which are far away from the dense regions,
%   expecting that these objects are unfamiliar to the previously seen
%   ones. Method for this: train a predictor, predict all the cells in
%   the pool. For all predictions calculate the distance of the
%   predicted point from the training points. The base of the rank is
%   the root mean of these distances.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.    
    
    properties
    end
    
    methods (Static)
        function paramarray = getParameters()
            paramarray = cell(0);
        end
    end
    
    methods
        function obj = GreatestDistance(~)
        end
        
        function paramarray = getParameterValues(~)
            paramarray = cell(0);
        end        
        
        function [nextCell] = nextCell(obj,predictor,predictorStore,data,trainingSet,~)          
            % ***** INIT part
            %memory allocation
            RMSofDist = zeros(data.sizeOfLabelingPool,1);
            X = zeros(1, length(trainingSet));
            %Get all data from the pool
            allInp = data.getData();            
            
            % END of INIT part *****
            
            %get a predictor to the current training set
            [ trainedPredictor] = getPredictorMacro( predictor, data, predictorStore, trainingSet);                        
            output = trainedPredictor{1}.predict(allInp);            

            [~, target] = data.getLabels();
            % For every cell            
            
            for i=1:data.sizeOfLabelingPool;             
                %The distance from every labeled cell
                for k=1:size(data.realLabels,1)
                    X(k)=pdist([target(k)';output(i)']);
                end
                RMSofDist(i) = sqrt(mean(X.^2));                
            end


            [~,nextCell] = sort(RMSofDist,'descend');           

        end
        
        function reset(obj)
        end
        
        function [string] = name(obj)
            string = strcat(class(obj));
        end
    end
    
end

