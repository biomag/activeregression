function [ model ] = TrainGPML(inputs,targets )
%TRAINGPML It is a trainer function for the gpml.
%   The gpml does not need trainig explicitly, but we can optimize
%   parameters beforehand, so it can perform better results.
%   Also many input parameters had to be chosen, which are also initialized
%   here.
%   The INPUTS: inputs and targets are match the requirements written in
%   TrainModel. (columns are the observations), but the minimize function
%   uses the transposed format.
%   We can decide in this code if we want to do normalization on the
%   inputs. (on the targets we do normalization all the time)
%   This can be set with normalization field's value (in model structure)
%   at the start of the code. The value is not a boolean but an indicator
%   for the normalization method. For the values meaning you can check
%   normalizeData function in the utils.

model.normalization = 0;

if model.normalization
    [inputNormalizedTransposed,model.ranges] = normalizeData(inputs',model.normalization);
    inputs = inputNormalizedTransposed';    
end

global myInitialization;
global myHyps;

if ~exist('myInitialization','var') || isempty(myInitialization) || myInitialization ~= 1239462,
    myHyps = cell(size(targets,1));
    for i=1:size(targets,1)
        hyp.cov = [1;1];
        hyp.mean = zeros(size(inputs,1)+1,1);
        hyp.lik = log(0.1);
        myHyps{i} = hyp;
    end
    myInitialization = 1239462;
end

% Defining functions.
meanfunc = {@meanSum, {@meanLinear, @meanConst}};
covfunc =  @covNNone;%@covSEiso;%
likfunc = @likGauss;

%Define some hyperparameters. (but these will be changed when we optimize)
%hyp.cov = [1;1];
%hyp.mean = zeros(size(inputs,1)+1,1);
%hyp.lik = log(0.1);


%Normalize targets
targetMean = mean(targets,2);
targetVariance = std(targets,[],2);
targets = (targets-repmat(targetMean,1,size(targets,2))) ./ repmat(targetVariance,1,size(targets,2));

for i=1:size(targets,1)
    %model.hyp{i} = hyp;
    hyp = myHyps{i};
    model.hyp{i} = minimize(hyp, @gp, -100, @infExact, meanfunc, covfunc, likfunc, inputs', targets(i,:)');    
    %model.hyp{i} = hyp;
    myHyps{i} = model.hyp{i};
end

model.meanfunc = meanfunc;
model.covfunc = covfunc;
model.likfunc = likfunc;
model.targetMean = targetMean;
model.targetVariance = targetVariance;
model.inputs = inputs';
model.targets = targets';


end

