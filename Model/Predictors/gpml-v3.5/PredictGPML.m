function [ outputs, variances ] = PredictGPML( model, inputs )
%PredictGPML prediction function for a 'trained' GPML model.
%   The training could be done with function: TrainGPML.
%   We suppose that we use the same parameters here as with which we
%   optimized the parameters in the training function: these parameters are
%   included in the model.
%   Inputs, outputs, variances are in a format that is described in the
%   Predictor class.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


    % check whether we trained our model with normalized inputs and if we
    % did, use the same normalization on our data.
    if model.normalization
        [inputNormalizedTransposed,model.ranges] = normalizeData(inputs',model.normalization,model.ranges);
        inputs = inputNormalizedTransposed';    
    end

    varianceOutputSize = size(inputs,2)*size(model.targets,2);
    if varianceOutputSize>10000 %This is also a rather strong limit, but it's faster then
        variances = sparse(varianceOutputSize,varianceOutputSize);
    else
        variances = zeros(varianceOutputSize);
    end
    for i=1:size(model.targets,2)
        [outputs(:,i), singleVar(:,i),~,~] = gp(model.hyp{i}, @infExact, model.meanfunc, model.covfunc, model.likfunc, model.inputs, model.targets(:,i), inputs');
    end
    outputs = outputs';
    for i=0:size(model.targets,2)-1
        for j=1:size(inputs,2)
            variances(i*size(inputs,2)+j,i*size(inputs,2)+j) = singleVar(j,i+1);
        end
    end
    
    %Retarnsform the normalization
    outputs = (outputs.*repmat(model.targetVariance,1,size(outputs,2)))+repmat(model.targetMean,1,size(outputs,2));

end

