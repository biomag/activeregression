classdef MulanEnsembleOfChains < Predictor
% AUTHOR:	Abel Szkalisity
% DATE: 	Oct 10, 2019
% NAME: 	MulanEnsembleOfChains
%
% A bridge for Mulan Ensemble of Regression Chains method. More detailed
% description can be found in the following publication:
%
%   Spyromitros-Xioufis, Eleftherios, et al. "Multi-target regression via
%   input space expansion: treating targets as inputs." Machine Learning 104.1 (2016): 55-98.
% 
%   The code was downloaded from: http://mulan.sourceforge.net/doc/
%   
%   Ensemble of Regression chains is a problem-transformation based multi-target
%   regression model, for further info check: 
%   Borchani, Hanen, et al. "A survey on multi-output regression." Wiley
%   Interdisciplinary Reviews: Data Mining and Knowledge Discovery 5.5
%   (2015): 216-233.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    
    properties
        %Properties which are essential for the training.
        
        %This class stores the required weka classes within the model
        %property, that is coming from the parent class.
        
        %The input dimension, number of features
        featureSize;
        
        %The stack needs a base and a meta learner type, which are strings
        %according to Weka Classifiers
        
        %The index of the selected learner for learning the base level
        baseLearner = 1;
        
        %The number of models
        nofModels = 2;
                
        %The dimension of the output to be stored also for prediction
        outDim
    end
    
    properties (Constant)
        %CellArray with the proper Weka access path (package + class name)
        %and displayed name for the user
        availableLearners = listWekaRegressors();
    end
    
    methods                
        function obj = MulanEnsembleOfChains()
            %Default constructor as required.
            fprintf('You have initialized a new Ensemble of Regression Chains Learner (Mulan)\n');
            obj.isDistribution = 0;
        end
        
        function obj = train(obj, inputs, targets)
            %The train function as required in Predictor parent class
            
            disp('Train with Ensemble of Regression Chains Mulan');
            
            %note: the attributes are indexed from 0.
            %get out sizes to variables
            featureS = size(inputs,1);
            obj.outDim = size(targets,1);
            N = size(inputs,2);            
            %construct weka algorithm for each output dimenstion                      
            %init attributes (specify size and type)
            attributes = obj.initAttributes(featureS,obj.outDim);
            
            %make trainingData (Instances) objects                       
            trainingData = weka.core.Instances('training_data', attributes, N);
            
            weight = 1.0;            
            for j=1:N
                mergedFeatureTargetVector = [inputs(:,j) ; targets(:,j)];
                observation = weka.core.DenseInstance(weight,mergedFeatureTargetVector);
                observation.setDataset(trainingData);
                trainingData.add(observation);
            end
            
            %Mulan specific lines
            labelsMetaData = mulan.data.LabelsMetaDataImpl();            
            
            for i=1:obj.outDim
                labelsMetaData.addRootNode(mulan.data.LabelNodeImpl(['Target_' num2str(i)]));
            end
            
            multiLabelInstance = mulan.data.MultiLabelInstances(trainingData,labelsMetaData);

            baseLearnerWeka = eval([MulanEnsembleOfChains.availableLearners{obj.baseLearner,2} ';']);
            
            samplingMethod = javaMethod('valueOf','mulan.regressor.transformation.EnsembleOfRegressorChains$SamplingMethod','WithReplacement');
            model = mulan.regressor.transformation.EnsembleOfRegressorChains(baseLearnerWeka,obj.nofModels,samplingMethod);
            
            %The actual training process
            model.build(multiLabelInstance);
            
            obj.model = model;
            obj.featureSize = featureS;
            
        end        
        
        function [outputs,variances] = predict(obj, inputs)
            disp('Predict with Mulan Ensemble of Regression Chains.');
            
            %attributes are the same as the training inputs
            attributes = obj.initAttributes(obj.featureSize,obj.outDim);
            %N here is the number of unknown inputs
            N = size(inputs,2);            
            
            testData = weka.core.Instances('test_data', attributes, N);
            
            %add the elements to the training set
            weight = 1.0;
            for j=1:N
                mergedFeatureTestVector = [inputs(:,j) ; zeros(obj.outDim,1)];
                observation = weka.core.DenseInstance(weight,mergedFeatureTestVector);
                observation.setDataset(testData);
                testData.add(observation);
            end
            
            outputs = zeros(obj.outDim,N);
            for j=1:N
                MLO = obj.model.makePrediction(testData.instance(j-1));
                outputs(:,j) = MLO.getPvalues();
            end        
            
            if nargout>1
                warning(['The ' class(obj) ' predictor is called with 2 output parameters. The second one is meaningless as ' class(obj) ' cannot provide variance.']);
                variances = 0;
            end            
        end
        
        function print2file(obj,f)
            fprintf(f,'Mulan Ensemble of Regression Chains.\n');            
            fprintf(f,['It is capable to predict distributions: ' num2str(obj.isDistribution) '\n']);
            fprintf(f,'The used base learner was: %s.\n',MulanEnsembleOfChains.availableLearners{obj.baseLearner,1});
            fprintf(f,'The number of chains were: %d.\n',obj.nofModels);            
            fprintf(f,'------\n');                                    
        end                
        
        function attributes = initAttributes(~,featureSize,outDim)
            %init attributes (specify size and type)
            
            if nargin<3, outDim = 0; end                
            attributes = weka.core.FastVector(featureSize+outDim);
            for i=1:featureSize
                attributes.addElement(weka.core.Attribute(['Feature_' num2str(i)])); %an Attribute with one parameter is a numeric parameter
            end                        
            for j=1:outDim
                attributes.addElement(weka.core.Attribute(['Target_' num2str(j)]));
            end
        end
        
        function obj = setParameters(obj,paramcell)                                   
            %Overload of setParameters as described in the Predictor
            %class.            
            obj.baseLearner = find(~cellfun(@isempty,strfind(MulanEnsembleOfChains.availableLearners(:,1),paramcell{1})));                         
            obj.nofModels = int8(str2double(paramcell{2}));
        end
        
        function paramcell = getParameterValues(obj)
            %Overload of getParameterValues as described in the Predictor
            %class.            
            paramcell{1} = MulanEnsembleOfChains.availableLearners{obj.baseLearner,1};
            paramcell{2} = num2str(obj.nofModels);            
        end
        
    end
    
    methods (Static)
        function paramarray = getParameters()
            paramarray{1}.name = 'Learner:';
            paramarray{1}.type = 'enum';
            paramarray{1}.values = MulanEnsembleOfChains.availableLearners(:,1);
            paramarray{1}.default = 1;
            
            paramarray{2}.name = 'Number of models:';
            paramarray{2}.type = 'enum';
            paramarray{2}.values = {'1','2'};
            paramarray{2}.default = 2;
        end 
    end
    
end

