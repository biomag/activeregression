classdef WekaREPTree < Predictor
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	WekaREPTree
%   
% Bridge to Weka implementation of Fast Decision Tree
% 
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    
    properties
        %Properties which are essential for the training.
        
        %This class stores the required weka classes within the model
        %property.
        
        %The input dimension, number of features.
        featureSize;
        
        %k: the number of neighbours used for the classification
        k = 10;
    end
    
    methods        
        function obj = WekaREPTree()        
            fprintf('You have initialized a new Fast decision tree learner. (Weka)\n');
            obj.isDistribution = 0;
        end
        
        function obj = train(obj, inputs, targets)        
            disp('Train with decision tree weka');                                                                                                                                  
            
            %note: the attributes are indexed from 0.
            %get out sizes to variables
            featureS = size(inputs,1);
            outDim = size(targets,1);
            N = size(inputs,2);
            model = cell(1,size(targets,1));
            %construct weka algorithm for each output dimenstion
            for i=1:length(model)
                model{i} = weka.classifiers.trees.REPTree();
            end 
            %init attributes (specify size and type)
            attributes = obj.initAttributes(featureS);
            
            %make trainingData (Instances) objects
            trainingData = cell(1,outDim);
            
            for i=1:outDim
                trainingData{i} = weka.core.Instances('training_data', attributes, N);
                trainingData{i}.setClassIndex(trainingData{i}.numAttributes() - 1); % the class is the last one (indexed from 0)

                %add the elements to the training set
                weight = 1.0;
                for j=1:N
                    mergedFeatureTargetVector = [inputs(:,j) ; targets(i,j)];
                    observation = weka.core.DenseInstance(weight,mergedFeatureTargetVector);
                    observation.setDataset(trainingData{i});
                    trainingData{i}.add(observation);
                end
                
                %train the model
                model{i}.buildClassifier(trainingData{i});
            end
            
            obj.model = model;
            obj.featureSize = featureS;
            
        end        
        
        function [outputs,variances] = predict(obj, inputs)
            disp('Predict with Weka REPTree.');                        
            
            %attributes are the same as the training inputs
            attributes = obj.initAttributes(obj.featureSize);
            %N here is the number of unknown inputs
            N = size(inputs,2);
            outDim = length(obj.model);            
            
            testData = weka.core.Instances('test_data', attributes, N);
            testData.setClassIndex(testData.numAttributes() - 1); % the class is the last one (indexed from 0)
            
            %add the elements to the training set
            weight = 1.0;
            for j=1:N
                observation = weka.core.DenseInstance(weight,inputs(:,j));
                observation.setDataset(testData);
                testData.add(observation);
            end
            
            outputs = zeros(outDim,N);
            for i=1:outDim                                                
                %train the model
                for j=1:N
                    outputs(i,j) = obj.model{i}.distributionForInstance(testData.instance(j-1));
                end
            end            
            
            if nargout>1
                warning(['The ' class(obj) ' predictor is called with 2 output parameters. The second one is meaningless as ' class(obj) ' cannot provide variance.']);
                variances = 0;
            end            
        end
        
        function print2file(obj,f)
            fprintf(f,'Weka REPTree.\n');
            fprintf(f,['It is capable to predict distributions: ' num2str(obj.isDistribution) '\n']);
            fprintf(f,'------\n');                                    
        end                
        
        function attributes = initAttributes(~,featureSize)
            %init attributes (specify size and type)
            attributes = weka.core.FastVector(featureSize+1);            
            for i=1:featureSize
                attributes.addElement(weka.core.Attribute(['feature' num2str(i)])); %an Attribute with one parameter is a numeric parameter
            end            
            attributes.addElement(weka.core.Attribute('Class'));
        end
        
    end        
    
end

