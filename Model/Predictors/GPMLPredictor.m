classdef GPMLPredictor < Predictor
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	GPMLPredictor
%
% Gaussian Processes implementation of the Predictor interface
%      This is a wrapper function for the Gaussian Processes function of
%      Carl Edward Rasmussen and Hannes Nickisch. Their code is located in
%      the gpml-v3.5 folder and distributed under the FreeBSD License.
%       
%      We use the gp function (from the GPML package) for predicting, but
%      for that it needs quite a lot of functions and parameters. These
%      are:
%           hyp: for hyperparameters
%           inf: the inferece function
%           meanfunc: mean function
%           covfunc: covariance function
%           likfunc: likelihodd function
%
%       There are lots of possibilities in the GPML package to specify the
%       parameters above, and it is not evident which to use, although it
%       has huge effect on the results.
%
%       In principle, GP is a paramter-free method, it can also provide
%       reasonable results without any training. However, the above
%       specified functions require many hypermarameters (especially
%       specific covarience functions) which can be optimized for the given
%       data, and this is done in the train function of this class.
%
%           More fields of the model struct:
%           targetMean: the mean of the targets (required to retrieve the
%           predicted outputs to the targeted space /because the targets
%           are normalized/)
%           targetVariance: because of the same reasons as above
%           targets: the normalized targets in GPML format! Transposed.
%           inputs: the inputs, in GPML format. Normalized according to the
%           user input.
%           ranges: in case of input normalization it stores the values
%           required to perform the exact same transform on the test data
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    
    properties
        %Properties which are essential for the training.
        
        %The normalization. Check for norm values the /util/normalizeData
        norm;
        
        %Mean function for GP
        meanf;
        
        %Covariance function for GP
        covf; 
        
        %Likelyhood function for GP
        likf;
    end
    
    methods        
        function obj = GPMLPredictor()  
        %Default constructor as required
            fprintf('You have initialized a new GPML predictor\n');
            obj.isDistribution = 1;
            obj.norm = 0;
            obj.model.meanfunc = @meanConst;%{@meanSum, {@meanLinear, @meanConst}};%@meanNN;%
            obj.model.covfunc = @covSEard;%@covSEiso;%@covNNone;%@covSEfact;%
            obj.model.likfunc = @likGauss;            
        end
        
        function obj = train(obj, inputs, targets)
        %TRAIN It is a trainer function for the gpml.
        %   The gpml does not need trainig explicitly, but we can optimize
        %   parameters beforehand, so it can perform better results.
        %   Also many input parameters had to be chosen, which are copied
        %   here from the constructor through properties of this class.
        %   The INPUTS: inputs and targets are match the requirements written in
        %   TrainModel. (columns are the observations), but the minimize function
        %   uses the transposed format.        
            disp('Train with GPML-v3.5');

            model = obj.model;
            model.normalization = obj.norm;
            
            if model.normalization
                [inputNormalizedTransposed,model.ranges] = normalizeData(inputs',model.normalization);
                inputs = inputNormalizedTransposed';    
            end
            
            %Normalize targets
            targetMean = mean(targets,2);
            targetVariance = std(targets,[],2);
            targets = (targets-repmat(targetMean,1,size(targets,2))) ./ repmat(targetVariance,1,size(targets,2));
           
            %Initialize hyperparameters
            if ~isfield(obj.model,'hyp')
                hyp = cell(size(targets,1),1);
                for i=1:size(targets,1)
                    hyp{i} = obj.initHypParameters(inputs,targets,i,targetMean,targetVariance);                    
                end                
            else
                hyp = obj.model.hyp;
            end

            % Defining functions.
            meanfunc = obj.model.meanfunc;
            covfunc = obj.model.covfunc; 
            likfunc = obj.model.likfunc;           
            
%#### *** OPTIMIZATION CODE FROM HERE *** ####

            for i=1:size(targets,1)
                [model.hyp{i},~] = minimize(hyp{i}, @gp, -100, @infExact, meanfunc, covfunc, likfunc, inputs', targets(i,:)');
                if ~isstruct(model.hyp{i}) % it may happen that the optimization fails
                    model.hyp{i} = hyp{i}; %Fallback to initialization
                end
            end
            
%#### *** OPTIMIZATION CODE FROM HERE *** ####

            model.targetMean = targetMean;
            model.targetVariance = targetVariance;
            model.inputs = inputs';
            model.targets = targets';
            
            obj.model = model;
        end        
        
        function [outputs,variances] = predict(obj, inputs)
        %The implementation of the predict function is implemented in a
        %separate function.
        
            disp('Predict with GPML.');            
            [outputs,variances] = PredictGPML(obj.model,inputs);                                    
        end
        
        function print2file(obj,f)
            fprintf(f,'GPML predictor by Rasmussen and Nickisch.\n');
            fprintf(f,['It is capable to predict distributions: ' num2str(obj.isDistribution) '\n']);
            fprintf(f,'------\n');
            fprintf(f,'Normalization for the input data:\n');
            switch obj.norm
                case 0
                    fprintf(f,'0: No normalization\n');
                case 1
                    fprintf(f,'1: (Default) each attribute is normalized to have a zero mean and 1 mean/median absolute deviation (mad)\n'); 
                case 2
                    fprintf(f,'2: (Standardization) Each attribute is normalized to fall in the range [0,1].\n');
                case 3
                    fprintf(f,'3: Each attribute is normalized to have zero mean and 1 standard deviation\n');
            end
            fprintf(f,'Used functions:\n');
            fprintf(f,['Mean function: ' obj.model.meanfunc '\n']);
            fprintf(f,['Covariance function: ' obj.model.covfunc '\n']);
            fprintf(f,['Likelihood function: ' obj.model.likfunc '\n']);
            
            fself = fopen([class(obj) '.m'],'r');            
            while ~strcmp(fgetl(fself),'%#### *** OPTIMIZATION CODE FROM HERE *** ####'), end
            fprintf(f,'\n Optimization code used:\n\n');
            currLine = fgetl(fself);
            while ~strcmp(currLine,'%#### *** OPTIMIZATION CODE FROM HERE *** ####')
                fprintf(f,'%s\n',currLine);
                currLine = fgetl(fself);
            end
        end                
        
        function [hyp] = initHypParameters(obj,inputs,targets,i,targetMean,targetVariance)            
            D = size(inputs,1);
            
            %for covSEfact <- this was removed for simplicity
            %hyp.cov = zeros(D*1 - 1*(1-1)/2 + 1,1);
            
            % Covariance functions
            initCov = estimateInitP(inputs,targets,min(10,size(inputs,2)-1));
            regPlaneBounds = str2num(obj.databounds); %#ok<ST2NM> We need to use str2num because we're working with matrix and not scalar
            expectedVariance = (1/12)*(regPlaneBounds(i,2)-regPlaneBounds(i,1)).^2;
            
            if strcmp(obj.model.covfunc,'covSEard')
                hyp.cov = zeros(D+1,1);                
                hyp.cov(end) =  log(sqrt(expectedVariance));
                hyp.cov(1:end-1) = initCov;
            elseif strcmp(obj.model.covfunc,'covSEiso') || strcmp(obj.model.covfunc,'covNNone')
                hyp.cov(1) = mean(initCov);
                hyp.cov(2) = log(sqrt(expectedVariance));            
            end
            
            if strcmp(obj.model.meanfunc,'meanConst')
                hyp.mean = ((regPlaneBounds(i,2)+regPlaneBounds(i,1))/2-targetMean(i))/targetVariance(i);
            elseif strcmp(obj.model.meanfunc,'meanLinear')
                hyp.mean = pinv(inputs*inputs')*inputs*targets(i,:)'; %linear regression estimation
            elseif strcmp(obj.model.meanfunc,'meanNN')
                hyp.mean = [];
            end            
            
            hyp.lik = log(0.1);                            
        end
        
        function obj = setParameters(obj,paramcell)
            %TODO:            
            if strcmp(paramcell{1},'on')
                obj.norm = 1;
            else
                obj.norm = 0;
            end
            obj.model.meanfunc = paramcell{2}; %@meanConst;%{@meanSum, {@meanLinear, @meanConst}};%@meanNN;%
            obj.model.covfunc  = paramcell{3}; %@covSEiso;%@covNNone;%@covSEfact;%
            obj.model.likfunc  = paramcell{4};
        end
        
        function paramcell = getParameterValues(obj)
            if obj.norm
                paramcell{1} = 'on';
            else
                paramcell{1} = 'off';
            end
            paramcell{2} = obj.model.meanfunc;
            paramcell{3} = obj.model.covfunc;
            paramcell{4} = obj.model.likfunc;
        end
    end
    
    methods (Static)                
        function paramarray = getParameters()
            %TODO: make it work with other methods too (mean and cov params are not ok)
            paramarray{1}.name = 'Normalization:';
            paramarray{1}.type = 'enum';
            paramarray{1}.values = {'on','off'};
            paramarray{1}.default = 1;                        
            
            paramarray{2}.name = 'Mean function:';
            paramarray{2}.type = 'enum';
            paramarray{2}.values = {'meanConst','meanLinear','meanNN'};
            paramarray{2}.default = 1;                                            
            
            paramarray{3}.name = 'Covariance function:';
            paramarray{3}.type = 'enum';
            paramarray{3}.values = {'covSEard','covSEiso','covNNone'};
            paramarray{3}.default = 1;                                            
                            
            paramarray{4}.name = 'Likelihood function:';
            paramarray{4}.type = 'enum';
            paramarray{4}.values = {'likGauss'};
            paramarray{4}.default = 1;
        end
    end
    
    
    
end

