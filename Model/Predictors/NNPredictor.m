classdef NNPredictor < Predictor
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	NNPredictor
% 
% Neural networks implementation for the predictor
% interface. This functionality is bridged from Matlab's Deep Learning
% Toolbox (formerly Neural Network Toolbox)
%
%   Neural networks are efficient learning algorithms and they are
%   widespread. They are capable to classification and regression too,
%   but it cannot predict distributions for regression. (In regression
%   they are only points in the space predicted).
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    
    properties
        numHiddenNeurons;
        trainRatio;
        testRatio;
        valRatio;
        transferFunction;
        constRows = [];
    end
    
    methods
        function obj = NNPredictor(numHiddenNeurons,  trainRatio, valRatio, testRatio)
            fprintf('You have instantiated a new Neural Network predictor\n');
            
            if nargin > 4
                error('MYEXC:NeuralNetworkTooManyInputs', ...
                    'NNPredictor has at most 4 input arguments.');
            end
            
            % fill in values to ensure that NN also has 'default' constructor
            if nargin<1                
                numHiddenNeurons = [70 40 20 4];
            end
            if nargin <2
                trainRatio = 0.7;
            end
            if nargin <3
                valRatio = 0.2;
            end
            if nargin < 4
                testRatio = 0.1;                                                   
            end
            
            obj.isDistribution = 0;
            obj.numHiddenNeurons = numHiddenNeurons;
            obj.trainRatio = trainRatio;
            obj.testRatio = testRatio;
            obj.valRatio = valRatio;            
            obj.transferFunction = 'logsig';
        end
        
        function obj = train(obj,inputs,targets)
            disp('Train with Neural Networks');
            obj.model = fitnet(obj.numHiddenNeurons);
            %obj.model.trainParam.showWindow = false;
            
            %Seems that it doesn't work with constant rows.
            
            obj.constRows = all(inputs == repmat(inputs(:,1),1,size(inputs,2)),2);
            %After test it seems that adding random noise helps more than
            %removing them.
            for i=find(obj.constRows)
                inputs(i,:) = inputs(i,:) + normrnd(0,0.1,1,size(inputs,2));
            end
            
            %Train the model
            obj.model = train(obj.model,inputs,targets,'useGPU','yes');            
        end
        
        function [outputs,var] = predict(obj,inputs)
             disp('Predict with Neural Networks');
                          
             outputs = sim(obj.model,inputs);             
             if nargout>1
                 warning('The Neural Networks predictor is called with 2 output parameters. The second one is meaningless as NN cannot provide variance.');
                 var = 0;
             end
        end
        
        function print2file(obj,f)
            fprintf(f,'MathWorks Neural Network Predictor.\n');
            fprintf(f,['It is capable to predict distributions: ' num2str(obj.isDistribution) '\n']);
            fprintf(f,'------\n');
            fprintf(f,['Number of hidden neurons: ' num2str(obj.numHiddenNeurons) '\n']);
            fprintf(f,['Train ratio: ' num2str(obj.trainRatio) '\n']);
            fprintf(f,['Test ratio: ' num2str(obj.testRatio) '\n']);
            fprintf(f,['Validation ratio: ' num2str(obj.valRatio) '\n']);
            fprintf(f,['The used transfer function is: ' obj.transferFunction '\n']);
        end
        
        function obj = setParameters(obj,paramcell)
            obj.numHiddenNeurons = paramcell{1};
        end
        
        function paramcell = getParameterValues(obj)
             paramcell{1} = obj.numHiddenNeurons;
        end
                
    end
    
    methods (Static)
        function [bool,msg] = checkParameters(paramcell)            
            [bool,msg] = checkNumber(paramcell{1},1,0,[1 Inf],'The hidden layer size(s)');
        end
        
        function paramarray = getParameters()
            paramarray{1}.name = '# of hidden neurons:';
            paramarray{1}.type = 'int';
            paramarray{1}.default = [70 40 20 4];                        
        end
    end
    
end

