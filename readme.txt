README for Active Regression module of Advanced Cell Classifier (ACC).

The module is written in ObjectOriented way. The most important classes:

Predictor is the interface (in Matlab: Abstract Class) for the models. (e.g. NeuralNetworks, Linear Regression etc.).

Steps of adding a new regression model to the module:

1. Create a class that is a child of Predictor

2. Implement the abstract functions of the Predictor superclass, namely:
	* To work properly with ACC you should give a parameter-less constructor to the class.

	* train(obj,inputs, targets): function for training the model (e.g fit the parameters to the data in Linear regression, determine support vectors in SMO etc.)
	Store the parameters, vectors etc. in the obj variable. The obj.model property is reserved for that, but you can do it alternatively.
	
	* predict(obj,inputs): after training a model, this function performs the prediction for the requested inputs. Here the previously set parameters are
	available in obj.model (or the other field where you had put it)
	
	* print2file: a function like toString. This is needed for documentation purposes when Active Learning tests are run.

    * Check also for the parameter setting abstract functions in the Predictor class.
	
3. Place your class into the Model/Predictors folder. Your model will be displayed as an option in ACC's regression plane module and you can test it.