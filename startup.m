% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	startup
%   
% STARTUP script for the whole MVC program.
% Note: if you use this software separately from ACC then to use the Mulan
% framework:
%   - download the mulan.jar and clus.jar files from the internet (e.g. http://mulan.sourceforge.net/)
%   - add these jars also to your java class path with the javaaddpath
%   command.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

% Adding all the paths to the search folder.
osslash = '\';

addpath(genpath(pwd)); 
javaclasspath([pwd '/Model/Predictors/weka.jar']);
%addpath(genpath(['..' osslash 'ACC' osslash 'sac']));
%load(['..' filesep 'data' filesep 'SIMCEPData50ConRad.mat' ]);
%load(strcat('data',osslash,'150818_ViljaFlipped_NewTest5.mat'));
%load(strcat('data',osslash,'LipidToXVilja_141111.mat'));
%load(strcat('data',osslash,'ACData_150530.mat'));


%addpath(strcat(pwd,osslash,'Data'));
%addpath(strcat(pwd,osslash,'Model'));
%addpath(strcat(pwd,osslash,'Utils'));
%addpath(strcat(pwd,osslash,'Results'));
%addpath(strcat(pwd,osslash,'Model',osslash,'DataClasses'));
%addpath(strcat(pwd,osslash,'Model',osslash,'Predictors'));

