% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	demo
%   
% Demo script to show the working of AL simulation software.
%
% Generate data: one can write other implementations of the abstract Data
% class to customize this.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

% Generate 100 artificial cells
ACD = ArtificialCellsData(100);

% Make an instance of the controller. See documentation for parmeters
AC = ALController('GPML',2,ACD,'CommitteeMembers',{3},10,5);

%Run 3 independent experiment. The results are saved to the
%../lastExperimentResults folder.
AC.runExperiment(3);