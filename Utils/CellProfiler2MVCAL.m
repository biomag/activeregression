     function [inputs,featureNames] = CellProfiler2MVCAL(handles)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	CellProfiler2MVCAL
%   
% prepare CellProfiler result to use in MVC AL strategies.
% This function converts CellProfiler's result to simple input matrix.
% This function is quite specific for a given CellProfiler pipeline, but it
% can be modified to use for other purposes
%   INPUT: handles: the result file from CellProfiler.
%   OUTPUTS: inputs: the converted inputs which can be used to build up
%   data in MVCAL.
%           featureNames: cellarray of feature names. There are features
%           with the same name that is because these are collections of
%           measurements on one specific property of the image. For
%           detailed description of features see the corresponding field of
%           the handles (which comes as an input)
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    %k is the counter for the columns in the inputs
    k=1;
    
    %strs are cell arrays with the extracted feature 'classes' which you
    %want to use.
    strCells = {'Intensity_OrigBlue','Intensity_OrigRed','Intensity_OrigGreen','AreaShape','Texture_5_OrigGreen'};
    %strCells = {'Intensity_OrigGreen','AreaShape','Texture_5_OrigGreen'};
    strMeanSpots = {'Intensity_OrigGreen','Intensity_OrigBlue','Intensity_OrigRed','AreaShape','Distance'};
    %strMeanSpots = {'Intensity_OrigGreen','AreaShape','Distance'};

    %First we'll go through all the images
    for i=1:length(handles.Measurements.Cells.Location)
        %Then we go through all the cells in one image
        for j=1:size(handles.Measurements.Cells.Location{i},1)            
            inputs(1,k) = i; %#ok<*AGROW> % it is difficult to calculate beforehand the size of the inputs                        
            inputs(2:3,k) = handles.Measurements.Cells.Location{i}(j,[2 1])';
            
            %it is enough to fill out the featureNames only once
            if i==1
                featureNames{1} = 'OriginalImageIndex';            
                featureNames{2} = 'Location1';
                featureNames{3} = 'Location2';
            end
            
            %m counts where we are in the columns
            m = 4;
            %add total cell features
            for l=1:length(strCells)
                s = eval( ['handles.Measurements.Cells. ' strCells{l} '{' num2str(i) '}' ] );
                inputs(m:m+size(s,2)-1,k) = s(j,:)';
                if i==1
                    for ii=1:size(s,2)
                        featureNames(m+ii-1) = cellstr(['Cells.' strCells{l} eval(['handles.Measurements.Cells. ' strCells{l} 'Features{' num2str(ii) '}' ])]);
                    end
                end
                m = m+size(s,2);      
            end            
            %add subcell features
            for l=1:length(strMeanSpots)
                s = eval( ['handles.Measurements.MeanSpots. ' strMeanSpots{l} '{' num2str(i) '}' ] );
                inputs(m:m+size(s,2)-1,k) = s(j,:)';                                
                if i==1
                    for ii=1:size(s,2)
                        featureNames(m+ii-1) = cellstr(['MeanSpots.' strMeanSpots{l} eval(['handles.Measurements.MeanSpots. ' strMeanSpots{l} 'Features{' num2str(ii) '}' ])]);
                    end
                end
                m = m+size(s,2);                
            end
            %add number of dots for each cell.
            inputs(m,k) = sum(handles.Measurements.Spots.Parent{i}==j);
            if i==1
                featureNames{m} = 'NumberOfDots';
            end
            k = k+1;                        
        end
    end
end