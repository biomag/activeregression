function [ predictions ] = crossValidate( nofFolds, predictor, inputs, targets, showInfo)
% AUTHOR:   Abel Szkalisity
% DATE:     August 25, 2017
% NAME:     crossValidate
% 
% Performs cross validation with a general predictor and given data.
% N always refers to the number of instances (training samples) that we
% cross validate
%
% INPUT:
%   nofFolds    Number of folds in the cross validation
%   predictor   A predictor with appropriate train and predict functions.
%               See class Predictor.
%   inputs      k by N matrix, the inputs (features), columns are
%               observations rows are variables
%   targets     d by N matrix, the targets, columns are observations d is
%               the output dimension
%   showInfo    Bool to indicate if we want to put a waitbar for the user
%
% OUTPUT:
%   predictions A d by N matrix that is filled with the cross-validated
%               predictions (each value is predicted by a model that was trained not using the)
%   
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

%init    
    d = size(targets,1);
    N = size(inputs,2);
    predictions = zeros(d,N);    
    ordering = randperm(N);
    
    i = 0;
    infoStr = sprintf('Performing %d-fold cross validation with %s. (%d/%d)',...
        nofFolds,...
        class(predictor),...
        i, nofFolds);
    if showInfo, waitHandle = waitbar(0,infoStr); end
    
    for i=1:nofFolds
        infoStr = sprintf('Performing %d-fold cross validation with %s. (%d/%d)',...
        nofFolds,...
        class(predictor),...
        i, nofFolds);
        if showInfo, waitbar(i/nofFolds,waitHandle,infoStr); end
        
        [trainIndices,testIndices] = Portion(ordering,nofFolds,i);
        if length(testIndices) < 2 || length(trainIndices) < 2
            if ishandle(waitHandle), close(waitHandle); end
            error('ACC:crossValidate','Cross Validation could not be performed due to the lack of samples. Please provide more training samples.');
        end
        
        trainFeatures = inputs(:,trainIndices);
        testFeatures = inputs(:,testIndices);
        trainTargets = targets(:,trainIndices);
        currPredictor = predictor.train(trainFeatures,trainTargets);
        predictions(:,testIndices) = currPredictor.predict(testFeatures);
    end         

    if showInfo && ishandle(waitHandle), close(waitHandle); end

end

