function [ figur, leg ] = addMethod2Plot(deviation,leg,experiment,fit)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	addMethod2Plot
%   
% This makes plot and legend from an AL experiment.
%   
%   INPUTs:
%      varargin: array of cellarrays
%           the cellarrays should contain vectors and we calculate their
%           mean
%       fit is a boolean indicating whether we need a fit or not.
%   OUTPUT:
%       figur: a figure with the results (every line is connected to a
%       cellarray element)

%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    colors='kbgrcmykbgrcmy';

    if ~fit
       leg{end+1}=experiment.alAlgorithm.name();
    else
       leg{end+1} = experiment.alAlgorithm.name();
       leg{end+1} =  experiment.alAlgorithm.name();
    end
        
    if deviation
        avg = median(experiment.multipleTestError);
        dev = std(experiment.multipleTestError);
        figur = errorbar(avg,dev,colors(length(leg)+1));
        hold on;
    elseif fit
        figur = 0;
        plot1OverX(mean(experiment.multipleTestError), 1, colors(length(leg)/2+1));
    else
        figur = plot(mean(experiment.multipleTestError),colors(length(leg)+1));        
        hold on;        
    end

    title('Errors from 10 run');
    xlabel('Number of labeled cells by the method');
    ylabel('MEAN ERROR');           
    legend(leg);            
    %hold off;
    
end 