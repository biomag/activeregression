function [C,perDimension] = distME(A,B)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	distME
%   
%distRMSE NEW TASK: computing average error between A and B.
%
%         n    dist(a(i),b(i))
%        szum ----------------  , where a(i) is the ith column of the A matrix
%        i=0          n             dist is the euclidean distance of the 2 vector
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

X = zeros(size(A,2),1);
XperDim = zeros(size(A,1),size(A,2));

for i=1:size(A, 2)
    X(i) = pdist([A(:,i)';B(:,i)']);
    XperDim(:,i) = abs(A(:,i)-B(:,i));
end
  C = (mean(X));
  perDimension = mean(XperDim,2);

end

