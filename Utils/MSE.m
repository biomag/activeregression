function [ me ] = MSE( A,B )
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	MSE
%   
% MSE Mean Squared Error: it computes the mean squared error between the
% parameters A and B.
%   A and B must have the same dimension. The function simply extract the
%   corresponding values take the square of them and gives back the mean.
%   If A,B are one dimensional 'me' (the result) will be scalar in other
%   cases it will be a vector or a matrix with the values. To handle more
%   than one dimensional error computations see distME and distRMSE
%   functions.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

me = mean((A-B).^2);

end

