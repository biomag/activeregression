function [ parameters, fittedCurve] = plotXSquare( inputFunctionValues, doPlot, colorLetter )
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	plotXSquare
%
% PlotXSquare is a function which gives back the best parameters to fit a
% a*x^2 + b * x + c function to the targeted values. It can also plot the
% fitted curve. 
%   It is a simple wrapped polyfit.
%   INPUTS:
%       inputFunctionValues is an array representing the function values.
%       doPlot is a boolean which indicates whether we'd like to plot the fitting.
%       colorLetter: is a character which describes in which color we'd
%       like to plot the curve. (both the fitted and the original one)
%   OUTPUTS:
%       parameters is an array with the parameters.
%       fittedCurve are points of the fitted curve calculated at the same
%       points as we have as example points from the targeted function
%       (inputsFunctionValues)
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


x = 1:length(inputFunctionValues);

p = polyfit(x,inputFunctionValues,2);
parameters = p;

fittedCurve = polyval(parameters,x);

if doPlot
    plot(inputFunctionValues,colorLetter);
    hold on;
    plot(x,fittedCurve,colorLetter);
end

end

