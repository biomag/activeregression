function [ parameters, fittedCurve] = plotExpX( inputFunctionValues, doPlot, colorLetter )
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	plotExpX
%
% This is a function which gives back the best parameters to fit an a*e^b*x
%function to the targeted values. It can also plot the fitted curve.
%   We transform the inputFunctionValues to be closely linear fit a line on
%   that with polyfit and than transform back. y = a*e^bx
%   INPUTS:
%       inputFunctionValues is an array representing the function values.
%       doPlot is a boolean which indicates whether we'd like to plot the fitting.
%       colorLetter: is a character which describes in which color we'd
%       like to plot the curve. (both the fitted and the original one)
%   OUTPUTS:
%       parameters is an array the first element is the 'a' parameter the
%       second is the 'b' (offset).
%       fittedCurve are points of the fitted curve calculated at the same
%       points as we have as example points from the targeted function
%       (inputsFunctionValues)
%   See also: Plot1OverX.m
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.



x = 1:length(inputFunctionValues);

p = polyfit(x,log(inputFunctionValues),1);
parameters(1) = exp(p(2));
parameters(2) = p(1);

fittedCurve = parameters(1)*exp(parameters(2)*x);

if doPlot
    plot(inputFunctionValues,colorLetter);
    hold on;
    plot(x,fittedCurve,colorLetter);   
end

end

