function [ params, fittedCurve, curveName, leg ] = optimalFit( inputFunctionValues, doPlot)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	optimalFit
%
% This function calculates several curve fitting and gives back
% the best fitted curve.
%   The best fitted curve can be measured with SSE or MSE this function
%   considers the MSE.
%   INPUTS:
%       inputFunctionValues: the target points from the function to which
%       we try to fit another one.
%       doPlot: a boolean indicating whether you want to make plots.
%   OUTPUTS:
%       params: the parameters of the best fitting curve.
%       fittedCurve: the best fitting curve values at the same points you
%       provided in the inputFunctionValues input.
%       curveName: it is the name of the best fitting function. You can
%       check the formulas in the list below.
%   
% LIST OF curve names and formulas:
%       The parameters are a,b,c ... are the corresponding elements of the
%       params array like a = p(1), b = p(2), ...
%       Linear regression means, that it was fitted with a simpler
%       'transform and polyfit' method all the others are fitted with
%       nlinfit matlab function.
%
%       - Exponential with linear regression: 
%       (linear regression means that it was transformed to lna - b*x = lny
%       form and on that we made a simple linear regression using polyfit
%       to determine the parameters 'a' and 'b')
%          y = a*e^(b*x)
%      
%       - 1/x with linear regression
%          y = a/x + b
%       
%       - x^2 with linear regression (polyfit 2nd order)
%          y = a*x^2 + b*x + c
%
%       - exp with offset
%          y = a*e^(b*x)+ c
%
%       - exp
%          y = a*e^(b*x)
%           
%       - 1/x with x offset
%          y = a / (x-b) + c
%
%       - exp with x and y offset
%          y = a*e^(b*(x-c)) + d;
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    leg = {};

    [params, fittedCurve] = plotExpX(inputFunctionValues, doPlot, 'y');
    bestMSE = MSE(fittedCurve, inputFunctionValues);
    curveName = 'Exponential with linear regression';
    leg{end+1}=num2str(bestMSE);
    leg{end+1}='exp with lin reg';
    
    [p, FC] = plot1OverX(inputFunctionValues, doPlot, 'r');
    newMSE = MSE(FC, inputFunctionValues);
    if newMSE<bestMSE
        params = p;
        fittedCurve = FC;
        curveName = '1/x with linear regression';
        bestMSE = newMSE;
    end
    leg{end+1}=num2str(newMSE);
    leg{end+1}='1/x with lin reg';
    
    
    [p, FC] = plotXSquare(inputFunctionValues, doPlot, 'g');
    newMSE = MSE(FC, inputFunctionValues);
    if newMSE<bestMSE
        params = p;
        fittedCurve = FC;
        curveName = 'x^2 with linear regression';
        bestMSE = newMSE;
    end
    leg{end+1}=num2str(newMSE);
    leg{end+1}='x^2 with lin reg';
    
    modelFunc = @(p,x)(p(1)*exp(p(2)*x)+p(3));
    p0 = [0 0 0];
    [p, FC] = fitCurveByModelFunction(inputFunctionValues, modelFunc, p0, doPlot, 'm');
    newMSE = MSE(FC, inputFunctionValues);
    if newMSE<bestMSE
        params = p;
        fittedCurve = FC;
        curveName = 'exp with offset';
        bestMSE = newMSE;
    end
    leg{end+1}=num2str(newMSE);
    leg{end+1}='exp with offset';
    
    modelFunc = @(p,x)(p(1)*exp(p(2)*x));
    p0 = [0 0];
    [p, FC] = fitCurveByModelFunction(inputFunctionValues, modelFunc, p0, doPlot, 'c');
    newMSE = MSE(FC, inputFunctionValues);
    if newMSE<bestMSE
        params = p;
        fittedCurve = FC;
        curveName = 'exp';
        bestMSE = newMSE;
    end
    leg{end+1}=num2str(newMSE);
    leg{end+1}='exp';
    
    modelFunc = @(p,x)(p(1)./(x-p(2))+p(3));
    p0 = [0 0 0];
    [p, FC] = fitCurveByModelFunction(inputFunctionValues, modelFunc, p0, doPlot, 'b');
    newMSE = MSE(FC, inputFunctionValues);
    if newMSE<bestMSE
        params = p;
        fittedCurve = FC;
        curveName = '1/x with x offset';
        bestMSE = newMSE;
    end
    leg{end+1}=num2str(newMSE);
    leg{end+1}='1/x with x offset';
    
    modelFunc = @(p,x)(p(1)*exp(p(2)*(x-p(3)))+p(4));
    p0 = [0 0 1 0];
    [p, FC] = fitCurveByModelFunction(inputFunctionValues, modelFunc, p0, doPlot, 'k');
    newMSE = MSE(FC, inputFunctionValues);
    if newMSE<bestMSE
        params = p;
        fittedCurve = FC;
        curveName = 'exp with x and y offset';
        bestMSE = newMSE;
    end
    leg{end+1}=num2str(newMSE);
    leg{end+1}='exp with x-y offset';
    
    
plot(inputFunctionValues,'k');
legend(leg);
fprintf('The best fitting curve is: %s\nMSE=%d\n',curveName,bestMSE);


end

