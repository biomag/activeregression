function [ parameters, fittedCurve, mse] = fitCurveByModelFunction( inputFunctionValues, modelfun, param0, doPlot, colorLetter,evolution,startpoint)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	fitCurveByModelFunction
%   
% This is a wrapper function to use nlinfit which is
% a non-linear regression function. The optimization is made by iterative
% least squares.
%   It gives back the parameter vector and plots the FunctionValues and the
%   fitted curve with different colours
%
% INPUTS:
%   inputsFunctionValues  it's an array representing the sample points
%                    to which we'd like fit the function
%   modelfun         the function with unknown parameters, this is what we're
%                    fitting.
%   param0           is the vector with initial parameters for the optimization.
%   doPlot           it a boolean indicating we'd like to plot our method or not
%   colorLetter      this is a character describing a color
%                    'k','b','r','g','c','m','y' usually. Pls. do not use 'k' if you use
%                    evolution because that is used for other purpose.
%   evolution        this is a boolean indicating whether we'd like to see
%                    fittings 'evolution': meaning that we fit curves to the first
%                    little part of the function and then put one more point to the
%                    fitting and so on.
%   startpoint       is the index from which we'd like to start the fitting
%                    process. e.g. if you think that the first 10 points are not
%                    interesting for you then set this value to 11.
%
% OUTPUTS:
%   parameters       The values of the fitted parameters
%   fittedCurve      The evaluated model function at the given input points
%                    with the fitted parameters
%   mse              The mean squared error of the fitting
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

if evolution
    for j=startpoint+1:length(inputFunctionValues)
        hold off;        
        y = j+1:length(inputFunctionValues);
        f = plot(y,inputFunctionValues(j+1:end),'k');      
        hold on;
        [parameters(j,:),fitted,~] = fitCurveByModelFunction( inputFunctionValues(startpoint:j), modelfun, param0, 1, colorLetter,0,startpoint);       
        fittedCurveResidual = modelfun(parameters(j,:),y);
        fittedCurve(j,:) = [fitted,fittedCurveResidual];
        mse(j) = MSE(fittedCurve(j,:), inputFunctionValues(startpoint:end));
        plot(y,fittedCurveResidual);        
        plot(startpoint+1:j,parameters(startpoint+1:end,end));
        pause(0.01);
        %saveas(f,strcat('VIDPIC\fitCurve',num2str(j)),'png');
    end    
else

x = startpoint:length(inputFunctionValues)+startpoint-1;

parameters = nlinfit(x, inputFunctionValues, modelfun, param0);

fittedCurve = modelfun(parameters,x);
mse = MSE(inputFunctionValues, fittedCurve);
if doPlot
    plot(x,inputFunctionValues,colorLetter);
    hold on
    plot(x,fittedCurve,colorLetter);
end
end

    
end

