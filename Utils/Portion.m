function [arrayout,arrayres] = Portion( arrayin, n, index )
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	Portion
%   
% It gives back a 'negative' slice of the arrayin in arrayout.
%   It is negative, because we just leave out one slice of the whole
%   arrayin. n is the number of slices we made from arrayin, and index is
%   the place of the gap. It is designed in the way that if you call it
%   with every index from 1:n then the gaps are nearly a classification
%   (there might be empty portions) of the arrayin elements.
%
%   INPUTS:
%       arrayin       A numerical vector which represents the set
%       n             An integer the number of portions
%       index         An integer between 1 and n to identify the left out
%                     portion
%
%   OUTPUTS:
%       arrayout      the negative slice of the arrayin
%       arrayres      the 'positive' slice, the residual, the part which we
%                     left out from the arrayout.
%
%   all are ROW vectors
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

arrayin = convert2RowVector(arrayin);

nofElements = size(arrayin,2);

%The bottom border of the gap.
if floor((index-1)/n*nofElements)==(index-1)/n*nofElements && index-1~=0
    gapDown = floor((index-1)/n*nofElements)-1;
else
    gapDown = floor((index-1)/n*nofElements);
end

%The top border of the gap.
gapTop = ceil((index)/n*nofElements);

if index==n
    arrayout = arrayin([1:gapDown gapTop+1:end]);
    arrayres = arrayin(gapDown+1:gapTop);
else
    arrayout = arrayin([1:gapDown gapTop:end]);
    arrayres = arrayin(gapDown+1:gapTop-1);
end

end

