function [wekaRegList] = listWekaRegressors()
% AUTHOR:   Abel Szkalisity
% DATE:     October 7, 2019
% NAME:     listWekaRegreesors
%
% Returns an cell-array with the list of available weka-regression methods
%
%   OUTPUT:
%       wekaRegList     An N by 2 cell-array with string entries. The first
%                       column is an easy, human readable and
%                       understandable name of the regressor, whilst the
%                       second column should contain the full package path
%                       and class name of the regressor in Weka. Each row
%                       represents a predictor.
%          
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
    

wekaRegList = {...
            'RandomForest','weka.classifiers.trees.RandomForest';...
            'NeuralNetwork','weka.classifiers.functions.MultilayerPerceptron';...
            'GaussianProcesses','weka.classifiers.functions.GaussianProcesses';...
            'LinearRegression','weka.classifiers.functions.LinearRegression';...
            'SMO_Regression','weka.classifiers.functions.SMOreg';...
           };

end

