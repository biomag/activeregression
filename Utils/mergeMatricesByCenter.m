function [ mergedMatrices ] = mergeMatricesByCenter( targetMatrix, sourceMatrix, position )
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	mergeMatricesByCenter
%   
% mergeMatricesByCenter given 2 matrices and a position in the first one we
% place the second matrix into the first in a way that the center of the
% second matrix will be the given position. (approximately)
%   position is an array with 2 elements the row and column position.
%   it works for at most 3 dimensional matrices.
%   we believe that there is enough space to place the matrix on the given
%   position!
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    position = round(position);   

    size1 = size(sourceMatrix,1);
    size2 = size(sourceMatrix,2);    
    
    mergedMatrices = targetMatrix;    
    sp1 = position(1)-floor(size1/2);
    sp2 = position(2)-floor(size2/2);
    if (0<sp1 && 0<sp2)        
        mergedMatrices(sp1:sp1+size1-1,sp2:sp2+size2-1,:) = sourceMatrix;
    end
        

end

