function [ trainedPredictor] = getPredictorMacro( predictor, data, predictorStore, trainingSet, isDistribution )
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	getPredictorMacro
%   
% This function is used as a macro. It gives back a predictor
% meeting the requirements written in trainingSet and isDistribution
% parameters.
%   If the predictor exists in the store then it gives back that (or
%   them) if the predictor does not stored in the predictorStore then it
%   trains one. (That is why we need a new predictor and the data too)
%   INPUTS:
%       predictor: an instance of predictor which can be trained
%       data: an instance of a Data class.
%       predictorStore: instance of a PredictorStore class.
%       trainingSet: an array with the indeces of objects in required
%       training set.
%       isDistribution: the parameter indicating whether we need a
%       distributive predictor or not. (It is optional, if you leave it
%       then it gives back all possible predictors)
%   OUTPUTS:
%       trainedPredictor: CELL ARRAY of the appropriate predictors.

%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

[input, target] = data.getLabels(trainingSet);

if nargin>4
   % The case when the distribution is important   
   [isAlreadyTrained, trainedPredictor] = predictorStore.getPredictor(trainingSet,isDistribution);
   if ~isAlreadyTrained
        trainedPredictor{1} = predictor.train(input, target);
        predictorStore.addPredictor(trainingSet, trainedPredictor{1});
   end   
else
   % The case when the distribution is not important   
   [isAlreadyTrained, trainedPredictor] = predictorStore.getPredictor(trainingSet);
   if ~isAlreadyTrained
        trainedPredictor{1} = predictor.train(input, target);
        predictorStore.addPredictor(trainingSet, trainedPredictor{1});
   end    
end
end

