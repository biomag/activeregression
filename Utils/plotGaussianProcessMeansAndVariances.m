function plotGaussianProcessMeansAndVariances( input,predictor )
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	plotGaussianProcessMeansAndVariances
%   
% We try to see how the GP and its variances go on during the AL cycle.
%   We require inputs, and a trained predictor which gives back means and
%   variances.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

[out,var] = predictor{1}.predict(input);

myEps = 10^-8;
sp = 1; %startPoint
ep = size(var,2)/2; %endPoint
var(sp:ep,sp:ep) = diag(diag(var(sp:ep,sp:ep))-min(diag(var(sp:ep,sp:ep))))./( max(diag(var(sp:ep,sp:ep))) - min(diag(var(sp:ep,sp:ep))) )./1000+myEps;
sp = size(var,2)/2+1;
ep = size(var,2);
var(sp:ep,sp:ep) = diag(diag(var(sp:ep,sp:ep))-min(diag(var(sp:ep,sp:ep))))./( max(diag(var(sp:ep,sp:ep))) - min(diag(var(sp:ep,sp:ep))) )./1000+myEps;


x1 = 0:.01:1; x2 = 0:.01:6;
[X1,X2] = meshgrid(x1,x2);
F = zeros(length(x2),length(x1));

for i=1:size(out,2)
    mu = [out(1,i) out(2,i)];
    Sigma = [var(i,i) var(i,i+size(out,2)); var(i+size(out,2),i) var(i+size(out,2),i+size(out,2))];    
    F2 = mvnpdf([X1(:) X2(:)],mu,Sigma);
    F2 = reshape(F2,length(x2),length(x1));
    F = F + F2;
end

surf(X1,X2,F);
pause(0.1);



end

