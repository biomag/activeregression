function [C,perDimension] = distRMSE(A,B)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	distRMSE
%   
%distRMSE 
%   Computing root mean squered error between A and B.
%   It considers Euclidesn distance as error. The dimensions of A and B must agree.
%   It handles the columns as observations.
%   The second output is an array length equal to the number of rows in A
%   or B. It contains the RMSE per dimension.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

X = zeros(size(A,2),1);
XperDim = zeros(size(A,1),size(A,2));

for i=1:size(A, 2)
    X(i) = pdist([A(:,i)';B(:,i)']);
    XperDim(:,i) = abs(A(:,i)-B(:,i));
end
  C = sqrt((mean(X.^2)));
  perDimension = sqrt(mean(XperDim.^2,2));

end

