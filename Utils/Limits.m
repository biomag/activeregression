function [ MIN, MIN_POS, MAX, MAX_POS ] = Limits( MATRIX)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	Limits
%   
% Limits Find total maximum in a 2D a matrix.
%   If there is more than one total maximum than one of them.
%   MIN and MAX are the concrete limit values, MIN_POS and MAX_POS are the
%   positions of the lims. The first is the row and the second is the
%   column.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.   

[MinCol, MinColInd] = min(MATRIX);
[MIN, MIN_POS(2)] = min(MinCol);
MIN_POS(1) = MinColInd(MIN_POS(2));

[MaxCol, MaxColInd] = max(MATRIX);
[MAX, MAX_POS(2)] = max(MaxCol);
MAX_POS(1) = MaxColInd(MAX_POS(2));



end

