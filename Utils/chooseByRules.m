function [ next_cell ] = chooseByRules(rank, realLabels, testLabels, resultSize)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	chooseByRules
%
% CHOOSEBYRULES This function helps us to choose the next cell based on the
% rank given us from a ranking algorithm (these ranking algorithms are the
% AL strategies, they are coded in the AL classes in the nextCell member
% function). We want to reserve a test set for pure testing 
% <= measuring our predictor's
% performance, from this test set we are not allowed to choose cells to
% label. Based on the ranks, maybe the best cell is an already labeled one,
% sometimes we'd like to allow to choose from these sometimes we would not.
% This setting could be changed in this function by adjusting the reSampling
% variable (bool).
%   INPUTS:       
%       rank: an array containing the labels in decreasing
%       importance-order. (The most 'interesting'/worth to label cell by
%       the AL algorithm is the first element of this array and so on)
%       realLabels: it is a vector (column or row) with the already labeled cells'
%       indeces.
%       testLabels: it is also a vector with the reserved for
%       testing cell's indeces. If you leave this array empty then none of
%       the cells will be 'protected' in this way.
%   OUTPUT: some best cell for labeling, which satisfies the requirements
%   written above. The number of these could be adjusted with the
%   resultSize variable.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


reSampling = 0;
if nargin<4
    resultSize = 1;
end

[~,ia] = setdiff(rank,testLabels);
rank = rank(sort(ia));
if ~reSampling
    %SOMETHING WRONG HERE
     [~,ia] = setdiff(rank,realLabels);
     rank = rank(sort(ia));
end
if length(rank)>=resultSize;
    next_cell = rank(1:resultSize);
else    
    baseException = MException('MYEXC:nomore','Sorry there is not enough cell to meet your requirements. Please make the Data Set bigger or adjust the nofNextCells to a lower value');
    throw(baseException);
end
    

end

