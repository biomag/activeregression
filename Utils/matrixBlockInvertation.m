function [ inverse ] = matrixBlockInvertation( A,B,C,D,fastening )
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	matrixBlockInvertation
%   
% matrixBlockInvertation helps to invert a matrice which is given in blocks.
%   If we have to evaluate inveration on lot of matrices which only
%   slightly differs then we can do it quite efficiently. The equations
%   used here comes from: https://en.wikipedia.org/wiki/Invertible_matrix#Blockwise_inversion
%   INPUTS:
%       A,B,C,D the block of the matrix which we wish to invert. A must be
%       a square matrix D?C*inv(A)*B must be non singular and possibly
%       small.
%       fastening is a boolean flag. If this is turned in (true), than we expect
%       instead of A the inverse of A. If A is a big part of the matrix
%       then this will fasten the full calculation.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

if fastening
    iA = A;
else
    iA = inv(A);
end
    CiA = C*iA;
    iDmCiAB = inv(D-CiA*B);
    iAB = iA*B;
    
    upperLeft = iA + iAB*iDmCiAB*CiA;
    upperRight = -iAB*iDmCiAB;
    lowerLeft = -iDmCiAB*CiA;
    lowerRight = iDmCiAB;
    inverse = [upperLeft upperRight; lowerLeft lowerRight];
    
end

