function p = estimateInitP(inputs,targets,k)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	estimateInitP
%   
% This function searching for good parameter initialization.
%    The sf parameters are the possible variances in the target space among
%    each dimension.
%    This function is written to find the p. The idea is that based on the
%    regression plane locations the characteristic length scale can be
%    estimated. This method has a parameter k.
%   Details: first we search for the k nearest neighbours (kNN) in the
%   target space(!) with traditional euclidean distance. Then seperately
%   for the input dimensions we calculate the mean distance for a point
%   from its kNN. Finally we take the mean over all the points. The result is an
%   array with input dimension elements which tells for every input
%   dimension what is the average of the average distance from the closest
%   points.
%   1. Find the k closest neighbours.
%   2. Meaure distances in each dimension for each cell from the k
%   corresponding closest neighbours.
%   3. Take the mean of the distances over the k closest for each cell.
%   4. take the mean over all the objects.
%
%
%   INPUTS:
%       inputs: inputs in format given back by the data objects (columns are observations)
%       targets: targets (columns are observations)
%       k: the input parameter for this algorithm.
%
%   OUTPUTS:
%       p: the estimated hyperparameters
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
        
%this is done here historically in a function
    p = calcP(inputs,targets,k);    

end

function ell = calcP(inputs,targets,k)
    d = pdist(targets','euclidean');
    %S is the distance matrix
    S = squareform(d);
    %preallocate space    
    neighboursDist = zeros(size(inputs,2),k,size(inputs,1));

    %over all the objects
    for i=1:size(inputs,2)
        %Sort the distance matrix for the ith element
        [~,indices] = sort(S(i,:));    
        %over all the dimensions
        for j=1:size(inputs,1)
            %over the most closest k neighbours in total distance
            for l=2:k+1
                %store the distance in the j dimension
                neighboursDist(i,l-1,j) = norm(inputs(j,i)-inputs(j,indices(l)));
            end        
        end
    end
    %compute the seperate mean in each dimension
    ell = reshape(mean(mean(neighboursDist,2),1),size(inputs,1),1)+10^-10;
end