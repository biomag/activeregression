function [ columnVector ] = convert2ColumnVector( vector )
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	convert2ColumnVector
%   
% convert2ColumnVector It is often a problem that a vector is not in right
% dimensions, and the programmer should remember whether the vector was a
% column or a row vector. (even knowing that in MatLab the vector is column by
% default, but that's not true for all the cases)
%   This function converts a vector to a Column vector whatever it was
%   before (column or row). The pair of this function is the
%   convert2RowVector converts to row from arbitrary vector.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

if (size(vector,1)<size(vector,2))
   columnVector = vector'; 
else
   columnVector = vector;
end

end

