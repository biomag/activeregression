function plotPredictionHistory( predictedPosition, selectedCells, trainingSet,firstPointTrainingSet, testSet, colorPrev, diffColor,maxIteration)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	plotPredictionHistory
%   
% By the historical records of the predictions we plot
% the predicted positions.
%   The objects are color-coded: blue means object from the test set green
%   is training and red is remaining. The function plots all the positions which are in the selected
%   cells. The predictions for cells in history are connected with
%   lines and their 'age' can be followed by their intensity. The less
%   intensive a point is the more older is. 
%   WARNING:
%   Be careful using diffColor do
%   not use many cells then, because the function will be very slow.
%
%   INPUTS:
%      predictedPositions: is a 3D array the same as described in
%      ALController.
%      selectedCells: we can filter from the cells because with huge amount
%      of cells the plot can be very crowded.
%      trainingSet: subset of indeces which cells are in the training set.
%      NOTE: the trainingSet at the end of the AL cycle stores the
%      historical information too, as the cells were selected into the
%      training set exactly in the order as they can be found in this
%      array. The training set information is used for color-coding. Anyway
%      we must know which is the first point of the training set where the
%      predictedPositions was recorded. (e.g. if there was 5 initial cells
%      when we didn't do AL then the start point is 5, because this is the
%      1st record in the predicted positions, at least if you use the
%      ALController.)
%      firstPointTrainingSet: the index of trainingset where from the
%      records start in predictedPositions.
%      testSet: objects in testSet also used for color coding.
%      colorPrev: this switch controls whether the connecting line gets the
%      previous or the following color. If it's 1 then it gets the prev if
%      it's zero it gets the following.
%      diffColor: 'boolean' if its zero then the function will ignore the
%      color codes and set up a new color code. It gives different colors
%      to different objects, but for easy differentiating it'll use only 7
%      different colors.
%      maxIteration is a number in the code which is a maximum limit for
%      the historical plot. (This is very important as we can reach easily
%      incredible amount of points to plot without this check. ~
%      incredible and not possible to plot is ~100000 points)
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
    
    color = zeros(1,3);
    testColor = zeros(1,3);
    trainColor = zeros(1,3);
    elseColor = zeros(1,3);            
    
    %as plot is very slow to call many times we must do the for loop
    %history-wise
    for j=max(1,size(predictedPosition,3)-maxIteration):size(predictedPosition,3)
        fadeColor = min(((size(predictedPosition,3)-j)./size(predictedPosition,3)),0.95);%min(1-1./(size(predictedPosition,3)-j+1.1),0.95);
        if diffColor
            for i=1:length(selectedCells)
                b = dec2bin(mod(i,7),3);
                for k=1:3
                    if b(k)=='0'
                        color(k) = fadeColor;
                    else
                        color(k) = 1;
                    end
                end
                
                plot(predictedPosition(1,selectedCells(i),j),predictedPosition(2,selectedCells(i),j),'*','Color',color);
                hold on;            
                if (colorPrev && j>1)
                    plot([predictedPosition(1,selectedCells(i),j), predictedPosition(1,selectedCells(i),j-1)],[predictedPosition(2,selectedCells(i),j),predictedPosition(2,selectedCells(i),j-1)],'Color',color);
                elseif (j<size(predictedPosition,3))
                    plot([predictedPosition(1,selectedCells(i),j), predictedPosition(1,selectedCells(i),j+1)],[predictedPosition(2,selectedCells(i),j),predictedPosition(2,selectedCells(i),j+1)],'Color',color);
                end                
            end
        else            
            trainColor(1) = fadeColor;
            trainColor(2) = 1;
            trainColor(3) = fadeColor;
            trainIndeces = intersect(selectedCells,trainingSet(1:firstPointTrainingSet+j));
            testColor(1) = fadeColor;
            testColor(2) = fadeColor;
            testColor(3) = 1;
            testIndeces = intersect(selectedCells,testSet);
            elseColor(1) = 1;
            elseColor(2) = fadeColor;
            elseColor(3) = fadeColor;                    
            elseIndeces = setdiff(selectedCells,union(testIndeces,trainIndeces));            
            
            plot(predictedPosition(1,trainIndeces,j),predictedPosition(2,trainIndeces,j),'*','Color',trainColor);
            hold on;
            plot(predictedPosition(1,testIndeces,j),predictedPosition(2,testIndeces,j),'*','Color',testColor);
            plot(predictedPosition(1,elseIndeces,j),predictedPosition(2,elseIndeces,j),'*','Color',elseColor);                    
            if (colorPrev && j>1)
                matrix1 = [predictedPosition(1,trainIndeces,j); predictedPosition(1,trainIndeces,j-1)];
                matrix2 = [predictedPosition(2,trainIndeces,j);predictedPosition(2,trainIndeces,j-1)];
                plot(matrix1,matrix2,'Color',trainColor);
                matrix1 = [predictedPosition(1,testIndeces,j); predictedPosition(1,testIndeces,j-1)];
                matrix2 = [predictedPosition(2,testIndeces,j);predictedPosition(2,testIndeces,j-1)];
                plot(matrix1,matrix2,'Color',testColor);
                matrix1 = [predictedPosition(1,elseIndeces,j); predictedPosition(1,elseIndeces,j-1)];
                matrix2 = [predictedPosition(2,elseIndeces,j);predictedPosition(2,elseIndeces,j-1)];
                plot(matrix1,matrix2,'Color',elseColor);                
            elseif (j<size(predictedPosition,3))
                plot([predictedPosition(1,trainIndeces,j); predictedPosition(1,trainIndeces,j+1)],[predictedPosition(2,trainIndeces,j);predictedPosition(2,trainIndeces,j+1)],'Color',trainColor);
                plot([predictedPosition(1,testIndeces,j);predictedPosition(1,testIndeces,j+1)],[predictedPosition(2,testIndeces,j);predictedPosition(2,testIndeces,j+1)],'Color',testColor);
                plot([predictedPosition(1,elseIndeces,j); predictedPosition(1,elseIndeces,j+1)],[predictedPosition(2,elseIndeces,j);predictedPosition(2,elseIndeces,j+1)],'Color',elseColor);
            end                
        end
    end
    
    hold off;


end

