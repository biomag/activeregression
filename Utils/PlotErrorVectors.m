function PlotErrorVectors( data,predictor )
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	PlotErrorVectors
%   
% we plot as lines the error between ground truth and prediction
% DIFFICULTY: the ground truth are not given for all types of data!
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

input = data.inputs;
groundTruth = data.targets;
%[input,groundTruth] = data.calcFeatures(1:data.sizeOfLabelingPool);
out = predictor{1}.predict(input);

plot([0 0 1 1 0],[0 5 5 0 0],'k--');
hold on;

for i=1:size(input,2)
    plot([groundTruth(1,i), out(1,i)],[groundTruth(2,i), out(2,i)],'b');
    plot(groundTruth(1,i),groundTruth(2,i),'g*');
    if ismember(i,data.realLabels)
        plot(out(1,i),out(2,i),'r*');
    else
        plot(out(1,i),out(2,i),'*','Color',[148/255 0 211/255]);
    end    
    hold on;
end
pause(1);

hold off;


end

