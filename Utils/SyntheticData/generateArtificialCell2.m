function [ cells, image ] = generateArtificialCell2(nofSamples)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	generateArtificialCells2
%
% This function will generate only some circles to
% be able to plot and organize them in the future.
%   The circles will all be grey but with different intensity, and they'll
%   be different in size. So they'll have only 2 features:
%       1st feature: radius between 0-1      
%       2nd feature: intensity between 0-1.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


doPlot = 1;

cells = cell(1,nofSamples);
image = cell(1,nofSamples);

for i=1:nofSamples
    a = rand();
    b = rand();

    if doPlot
        alfa = linspace(0,2*pi,300);

        cColorR = b;
        cColorG = b; 
        cColorB = b;

        sizeOfBackground = 2;    
        fill([-sizeOfBackground sizeOfBackground sizeOfBackground -sizeOfBackground -sizeOfBackground], [sizeOfBackground sizeOfBackground -sizeOfBackground -sizeOfBackground sizeOfBackground], 'w');    
        hold on;
        h = fill(a*cos(alfa),a*sin(alfa),[cColorR, cColorG, cColorB]);         
    end
       
    cells{i}.r = a;
    cells{i}.c = b;
    
    if doPlot
        %get out the image
        F = getframe(gca);        
        img = frame2im(F);
        imgCenterX = round(size(img,1)/2);
        imgCenterY = round(size(img,2)/2);
        cutOffHalfSizeX = round((size(img,1))./4);
        cutOffHalfSizeY = round((size(img,2))./4);
        img = img(imgCenterX-cutOffHalfSizeX:imgCenterX+cutOffHalfSizeX,imgCenterY-cutOffHalfSizeY:imgCenterY+cutOffHalfSizeY,:);
        image{i} = img;

        %pause(0.1);
        hold off;
    end
end
end

