function [ cells, image ] = generateArtificialCell1( nofSamples )
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	generateArtificialCells1
%   
% We generate some numbers which will symbolize
% cells.
%   The abstract of the cell:
%       The boundary of a cell is an ellipse, which is it's cytoplasm.
%       Somewhere inside the cell there is it's nucleus which is another
%       ellipse. For other cell particles there will be circles which
%       represents them. They can be scattered in the cell or accumulated
%       somewhere.
%   The features based on the description above:
%       - 1-2. major and minor axis for the cytoplasm.
%       - 3-4. relative position of the center of the nucleus to the cytoplasm.
%       - 5-6. major and minor axis for the nucleus.
%       - 7. offangle: the rotation of the nucleus relativily to the
%       cytoplasm in positive direction.
%       - 7. number of other parts.
%       - 8. average distance from each other for the other parts.
%       - 9. color for the cytoplasm.
%       - 10. color for the nucleus
%       - 11. color for the other parts.
%   
%   We give back cells which is a cellarray of structures. Every structure
%   represents a biological cell. With the upeer features extendet by the
%   position of the dots, and the colors are not encoded, they are in RGB.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

doPlot = 1;

cells = cell(1,nofSamples);
image = cell(1,nofSamples);

for i=1:nofSamples
    a = rand();
    b = rand();

    alfa = linspace(0,2*pi,300);

    cColorR = rand/2;
    cColorG = 0.7; 
    cColorB = rand/2;
    
    if doPlot
        h = fill(a*cos(alfa),b*sin(alfa),[cColorR, cColorG, cColorB]);
        hold on;
    end

    phi = rand*pi;
    rotate = [cos(phi),-sin(phi);
              sin(phi), cos(phi)];

    %distance of the center and the perimeter of the cytoplasm ellipsis in
    %phi direction. This is the direction to which c shows. (c is the
    %'major' axis of the nucleus)
    xCoordInPhiDir = sqrt( (a^2*b^2) / (b^2+a^2*(tan(phi))^2));
    yCoordInPhiDir = sqrt( b^2 - (b^2/a^2)*xCoordInPhiDir^2);
    distInPhiDirection = sqrt(xCoordInPhiDir^2 + yCoordInPhiDir^2);
    
    %Distance of the center and the perimeter of cytoplasm ellipsis in
    %phi+pi/2 direction. This is the direction to which d shows.
    xCoordInPhiPlusDir = sqrt( (a^2*b^2) / (b^2+a^2*(tan(phi+pi/2))^2));
    yCoordInPhiPlusDir = sqrt( b^2 - (b^2/a^2)*xCoordInPhiPlusDir^2);
    distInPhiPlusDirection = sqrt(xCoordInPhiPlusDir^2 + yCoordInPhiPlusDir^2);    

    c = rand();
    d = b*rand();

    c = c * distInPhiDirection*2/3;
    d = d * distInPhiPlusDirection*2/3;
    
    % To ensure that the nucleus is in the cytoplasm we should determine
    % the distance in phi and phi+pi/2 direction. And then rescale d and c
    % with these distances.

    rOffset = (rand*2-1)*(min((distInPhiDirection - c),(distInPhiPlusDirection-d)));

    xOffset = rOffset*cos(phi);
    yOffset = rOffset*sin(phi);

    rotatedEllipse = rotate*[c*cos(alfa);d*sin(alfa)];

    nColorR = rand /4;
    nColorG = rand;   
    nColorB = 1;
    if doPlot
        fill(rotatedEllipse(1,:)+xOffset,rotatedEllipse(2,:)+yOffset, [nColorR, nColorG, nColorB]);   
    end
       
    %Put dots on the cells which are the 'other particles'
    nofDots = floor(rand*30);
    dotX = zeros(1,nofDots);
    dotY = zeros(1,nofDots);
    dotR = zeros(1,nofDots);    
    
    oColorR = rand;
    oColorG = rand;
    oColorB = rand;
    
    %Possibly good target feature:
    % How far are the dots from the center.
    dotAccumulation = rand;    
    
    for j=1:nofDots
        randAngle = rand*2*pi;
        dotX(j) = (a)*cos(randAngle)*dotAccumulation;
        dotY(j) = (b)*sin(randAngle)*dotAccumulation;
        dotR(j) = b*rand/10;
        
        if doPlot
            fill(dotR(j)*cos(alfa)+dotX(j), dotR(j)*sin(alfa)+dotY(j),[oColorR, oColorG, oColorB]);
        end
    end
    
    if doPlot
        plot([1 1 -1 -1 1], [-1 1 1 -1 -1],'k');
    
        filename = strcat('SyntheticCellPictures\ArtificialCell',num2str(i));
        F = getframe(gca);

        img = frame2im(F);
        imgCenterX = round(size(img,1)/2);
        imgCenterY = round(size(img,2)/2);
        cutOffHalfSizeX = round((size(img,1).*b)./2);
        cutOffHalfSizeY = round((size(img,2).*a)./2);
        img = img(imgCenterX-cutOffHalfSizeX:imgCenterX+cutOffHalfSizeX,imgCenterY-cutOffHalfSizeY:imgCenterY+cutOffHalfSizeY,:);
        image{i} = img;        
      % saveas(h,filename,'jpg');        
        hold off;
    end
       
    cells{i}.cytoA = a;
    cells{i}.cytoB = b;
    cells{i}.cytoColorR = cColorR;
    cells{i}.cytoColorG = cColorG;
    cells{i}.cytoColorB = cColorB;
    cells{i}.nuclA = c;
    cells{i}.nuclB = d;
    cells{i}.nuclAng = phi;
    cells{i}.nuclOffset = rOffset;
    cells{i}.nuclColorR = nColorR;
    cells{i}.nuclColorG = nColorG;
    cells{i}.nuclColorB = nColorB;
    cells{i}.nofParticles = nofDots;
    cells{i}.dotX = dotX;
    cells{i}.dotY = dotY;
    cells{i}.dotR = dotR;
    cells{i}.dotColorR = oColorR;
    cells{i}.dotColorG = oColorG;
    cells{i}.dotColorB = oColorB;
    cells{i}.dotAccumulation = dotAccumulation;
    
end
end

