function [ inputs, targets ] = CH2NNspec( CommonHandles, classIdx, selectedCells)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	CH2NNspec
%   
% Transform the needed (specified) data for neural networks, from CommonHandles
% 'which' is an array which contains the indexes of the cells we'd like to
% transform to the appropriate form. The which should be an N by 1 vector.
% The nntoolbox works with columns.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

%preallocation for speed
inputs = zeros(size(CommonHandles.Regression{classIdx}.Features{selectedCells(1)},2),size(selectedCells,1));
targets = zeros(size(CommonHandles.Regression{classIdx}.ImagePosition{selectedCells(1)},2),size(selectedCells,1));


for i=1:size(selectedCells,1)

    inputs(:, i) = CommonHandles.Regression{classIdx}.Features{selectedCells(i)};

    targets(:, i) = CommonHandles.Regression{classIdx}.ImagePosition{selectedCells(i)};
        
end

end

