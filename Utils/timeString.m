function [ timeInString ] = timeString(precision)
% AUTHOR:   Abel Szkalisity
% DATE:     Dec 16, 2016
% NAME:     timeString
%
% Transform time point to string.
%
% INPUT:
%   precision       An integer which defines how many 'fields' do we want
%                   to include in the final string. Fields: YYYYMMDDHHMMSS
%                   all in together at most 6.
%
% OUTPUT:
%   timeInString    A string with the current time.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

timestamp = clock;

timeInString = num2str(timestamp(1));
if precision>1
    timeInString = [timeInString num2str(timestamp(2),'%02d')];
    if precision>2
        timeInString = [timeInString num2str(timestamp(3),'%02d')];
        if precision>3
            timeInString = [timeInString '_' num2str(timestamp(4),'%02d')];
            if precision>4
                timeInString = [timeInString num2str(timestamp(5),'%02d')];
                if precision>5
                    timeInString = [timeInString num2str(floor(timestamp(6)),'%02d')];
                end
            end
        end
    end
end

end

