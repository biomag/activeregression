function [ inputs, targets ] = getDataFromCH( CommonHandles, originalIndices, selectedCells)
% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	getDataFromCH
%   
% %Selects out the neccessary data from CommonHandles.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

originalSelectedIndices = originalIndices(selectedCells);

inputs = cell2mat(CommonHandles.TrainingSet.Features(originalSelectedIndices))';
targets = reshape(cell2mat(CommonHandles.TrainingSet.RegPos(originalSelectedIndices)),2,[]);

end

