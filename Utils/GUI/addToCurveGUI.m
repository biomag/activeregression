function varargout = addToCurveGUI(varargin)
% ADDTOCURVEGUI MATLAB code for addToCurveGUI.fig
%      ADDTOCURVEGUI, by itself, creates a new ADDTOCURVEGUI or raises the existing
%      singleton*.
%
%      H = ADDTOCURVEGUI returns the handle to a new ADDTOCURVEGUI or the handle to
%      the existing singleton*.
%
%      ADDTOCURVEGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ADDTOCURVEGUI.M with the given input arguments.
%
%      ADDTOCURVEGUI('Property','Value',...) creates a new ADDTOCURVEGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before addToCurveGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to addToCurveGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help addToCurveGUI

% Last Modified by GUIDE v2.5 22-Jan-2016 14:53:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @addToCurveGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @addToCurveGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before addToCurveGUI is made visible.
function addToCurveGUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to addToCurveGUI (see VARARGIN)

% Choose default command line output for addToCurveGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes addToCurveGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = addToCurveGUI_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on selection change in typePopup.
function typePopup_Callback(hObject, ~, handles)
% hObject    handle to typePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns typePopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from typePopup

if get(hObject,'Value') ~=1
    set(handles.selectedSingle,'Enable','off');
    set(handles.text5,'Enable','off');
else
    set(handles.selectedSingle,'Enable','on');
    set(handles.text5,'Enable','on');
end
    
    


% --- Executes during object creation, after setting all properties.
function typePopup_CreateFcn(hObject, ~, ~)
% hObject    handle to typePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

global watchGUIData;

%types is the variabel which stores what to do with the 'multiple' variable
%before displaying. Single means we select only one curve and display that,
%mean displays the mean. The action stores the function name which should
%be evaluated to achieve the correct result.
watchGUIData.GUIControls.types.names = {'Single','Mean'};
watchGUIData.GUIControls.types.actions = {'','mean'};

set(hObject, 'String', watchGUIData.GUIControls.types.names);


function selectedSingle_Callback(~, ~, ~)
% hObject    handle to selectedSingle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of selectedSingle as text
%        str2double(get(hObject,'String')) returns contents of selectedSingle as a double


% --- Executes during object creation, after setting all properties.
function selectedSingle_CreateFcn(hObject, ~, ~)
% hObject    handle to selectedSingle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in addCurveButton.
function addCurveButton_Callback(~, ~, handles)
% hObject    handle to addCurveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%initialization of the colors which are easily distinguisible from the
%internet.
colorArray = [0 0 0 ;...
213 255 0 ;...
255 0 86 ;...
158 0 142 ;...
14 76 161 ;...
255 229 2 ;...
0 95 57 ;...
0 255 0 ;...
149 0 58 ;...
255 147 126;...
164 36 0;...
0 21 68;...
145 208 203;...
98 14 0;...
107 104 130;...
0 0 255;...
0 125 181;...
106 130 108;...
0 174 126;...
194 140 159;...
190 153 112;...
0 143 156;...
95 173 78;...
255 0 0;...
255 0 246;...
255 2 157;...
104 61 59;...
255 116 163;...
150 138 232;...
152 255 82;...
167 87 64;...
1 255 254;...
255 238 232;...
254 137 0;...
189 198 255;...
1 208 255;...
187 136 0;...
117 68 177;...
165 255 210;...
255 166 254;...
119 77 0;...
122 71 130;...
38 52 0;...
0 71 84;...
67 0 44;...
181 0 255;...
255 177 103;...
255 219 102;...
144 251 146;...
126 45 210;...
189 211 147;...
229 111 254;...
222 255 116;...
0 255 120;...
0 155 255;...
0 100 1;...
0 118 255;...
133 169 0;...
0 185 23;...
120 130 49;...
0 255 198;...
255 110 65;...
232 94 190];

global watchGUIData;

colorArray = colorArray./255;
curveStruct.color = colorArray(mod(watchGUIData.GUIControls.maxCurveNumber,size(colorArray,1))+1,:);
watchGUIData.GUIControls.maxCurveNumber = watchGUIData.GUIControls.maxCurveNumber+1;

curveStruct.origin.project = get(watchGUIData.GUIControls.handles.projectList,'Value');
curveStruct.origin.curve = get(watchGUIData.GUIControls.handles.curvesInProjectList,'Value');
curveStruct.type = watchGUIData.GUIControls.types.names{get(handles.typePopup,'Value')};
if get(handles.typePopup,'Value') == 1
    curveStruct.singleIndex = str2double(get(handles.selectedSingle,'String'));
else
    curveStruct.singleIndex = 0;
end

splittedProject = strsplit(watchGUIData.projects{curveStruct.origin.project}.name,'_');
splittedCurve = strsplit(watchGUIData.projects{curveStruct.origin.project}.listOfCurves(curveStruct.origin.curve,:),'m');
curveStruct.name = [splittedProject{2} '_' splittedProject{4} '_' splittedCurve{2}(1:end-1) '_' curveStruct.type '-' num2str(curveStruct.singleIndex)];
curveStruct.typeIndex = get(handles.typePopup,'Value');
watchGUIData.curves{end+1} = curveStruct;

close(gcf);
uiresume(watchGUIData.GUIControls.handles.figure1);
