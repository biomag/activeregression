function varargout = watchCurvesGUI(varargin)
% WATCHCURVESGUI MATLAB code for watchCurvesGUI.fig
%      WATCHCURVESGUI, by itself, creates a new WATCHCURVESGUI or raises the existing
%      singleton*.
%
%      H = WATCHCURVESGUI returns the handle to a new WATCHCURVESGUI or the handle to
%      the existing singleton*.
%
%      WATCHCURVESGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WATCHCURVESGUI.M with the given input arguments.
%
%      WATCHCURVESGUI('Property','Value',...) creates a new WATCHCURVESGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before watchCurvesGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to watchCurvesGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help watchCurvesGUI

% Last Modified by GUIDE v2.5 08-May-2016 15:12:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @watchCurvesGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @watchCurvesGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before watchCurvesGUI is made visible.
function watchCurvesGUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to watchCurvesGUI (see VARARGIN)

% Choose default command line output for watchCurvesGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes watchCurvesGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global watchGUIData;
% % *** DOCUMENTATION *** % %
%{
watchGUIData is a global variable used to share all the neccesarry data
for the operation of this GUI. It is a structure with the following fields.
    projects: cellarray containing the loaded projects. Every project is a
    structure with the fields:
        name: the name of the project which is the filename
        folder: the absolute path for the folder of the project
        listOfCurves: the list of the filenames which stores the 'multiple'
        fieds from ALController and thus a curve can be loaded from them.
    curves: cellarray containing the curves which are currently displayed
    on the axes. Every curve is a struct:
        origin: struct with 2 fields project and curve which identifies the
        origin place of that curve. Both project and the curve are
        identified by numbers which are the index for them in the project
        list (and index for the curve inside the project).
        AUC: the area under curve
        type: enum which can be 'mean' or 'single'. If this is single then
        we plot only one curve from the multiple fields else we plot the
        mean of them, this can be extended later with other combinations of
        the single curves.
        typeIndex: the index for the given type in the type array stored
        in addToCurveGUI (the drop down type list in the GUI)
        singleIndex = if we plot single then this is the index number
        name: this is the name which identifies the curve on the curve
        list.
        color a 1-by-3 array which identifies the color for the curve this
        is changing by the length of the curves array. (meaning it is
        different for all entry of the curves array) 
    GUIControls is a struct containing different fields for the operation of
    the GUI like handles for GUIs which make the uiresume and uiwait work
    etc.
        handles: this is the handles struct for THIS GUI containing all the
        handles. It is refresshed before opening addToCurveGUI.
        types: is the variable which stores what to do with the 'multiple' variable
        before displaying. Single means we select only one curve and display that,
        mean displays the mean. 
            names stores the name of the type
            actions stores the function name which should
            be evaluated to achieve the correct result.            
        maxCurveNumber: the number of ever displayed curves. This is useful
        for the color codes. If you delete and load in new curves than this
        will grow.
        globalLineWidth: a positive value indicating the line width.
%}
watchGUIData.projects = [];
watchGUIData.curves = [];
watchGUIData.GUIControls = [];
watchGUIData.GUIControls.maxCurveNumber = 0;
watchGUIData.GUIControls.globalLineWidth = 1.0;

% --- Outputs from this function are returned to the command line.
function varargout = watchCurvesGUI_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on selection change in projectList.
function projectList_Callback(~, ~, handles) %#ok<*DEFNU> The callback functions are called from outside from the GUI.
% hObject    handle to projectList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns projectList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from projectList

global watchGUIData;
set(handles.curvesInProjectList,'String',watchGUIData.projects{get(handles.projectList,'Value')}.listOfCurves);

% --- Executes during object creation, after setting all properties.
function projectList_CreateFcn(hObject, ~, ~)
% hObject    handle to projectList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in curvesInProjectList.
function curvesInProjectList_Callback(~, ~, ~)
% hObject    handle to curvesInProjectList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns curvesInProjectList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from curvesInProjectList


% --- Executes during object creation, after setting all properties.
function curvesInProjectList_CreateFcn(hObject, ~, ~)
% hObject    handle to curvesInProjectList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in curveList.
function curveList_Callback(~, ~, handles)
% hObject    handle to curveList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns curveList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from curveList

refreshCurves(handles);



% --- Executes during object creation, after setting all properties.
function curveList_CreateFcn(hObject, ~, ~)
% hObject    handle to curveList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function openPushTool_ClickedCallback(~, ~, handles)
%In this GUI open means open the result folder of an experiment. You can
%specify here a location for the folder and after we add this to the
%project cellarray and it must appear on the projectList panel.
%
%The result folder contains in the root the explog file, the .mat with the
%full experiment, the single curve files which stores most of the multiple
%fields only, the folders for saved plots etc.
%
% hObject    handle to openPushTool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

selectedFolder = uigetdir('Specify location for project');
splittedSelectedFolder = strsplit(selectedFolder,filesep);
newstruct.name = splittedSelectedFolder{end};
newstruct.folder = selectedFolder;

list = ls([selectedFolder filesep '*.mat']);
list(1,:) = [];
newstruct.listOfCurves = list;


global watchGUIData;
watchGUIData.projects{end+1} = newstruct;
refreshProjectList(handles,watchGUIData.projects);


% --- Executes on button press in addCurveToFig.
function addCurveToFig_Callback(~, ~, handles)
% hObject    handle to addCurveToFig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global watchGUIData;
watchGUIData.GUIControls.handles = handles;
uiwait(addToCurveGUI);

refreshCurves(handles);

% --- Executes on button press in removeCurveFromFig.
function removeCurveFromFig_Callback(~, ~, handles)
% hObject    handle to removeCurveFromFig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global watchGUIData;
watchGUIData.curves(get(handles.curveList,'Value')) = [];
if get(handles.curveList,'Value')>1
    set(handles.curveList,'Value',get(handles.curveList,'Value')-1);    
else
    set(handles.curveList,'Value',1);
end
refreshCurves(handles);

% --- local help function which puts the projects data to the list
function refreshProjectList(handles,projectArray)

listOfProjectNames = char(length(projectArray),1); 

for i=1:length(projectArray)
    listOfProjectNames(i,1:length(projectArray{i}.name)) = projectArray{i}.name;
end

set(handles.projectList,'string',listOfProjectNames);
set(handles.projectList,'Value',length(projectArray));
projectList_Callback(handles.projectList,0,handles);

% --- local help function to put the curves onto the axes.
function refreshCurves(handles)

global watchGUIData;

cla(handles.curveAxes);
axes(handles.curveAxes);
whitebg(handles.figure1,'w');

curveString = cell(1,length(watchGUIData.curves));
for i=1:length(watchGUIData.curves)
    curveString{i} = [ '<HTML><BODY color = #' reshape(dec2hex(watchGUIData.curves{i}.color.*255,2)',1,6) '>' watchGUIData.curves{i}.name];
    
    var = load([watchGUIData.projects{watchGUIData.curves{i}.origin.project}.folder filesep watchGUIData.projects{watchGUIData.curves{i}.origin.project}.listOfCurves(watchGUIData.curves{i}.origin.curve,:)]);
    fields = fieldnames(var);
    curveData = eval(['var.' fields{1}]);    
    
    colorToPlot = watchGUIData.curves{i}.color;
    if (i==get(handles.curveList,'Value'))
        widthToPlot = watchGUIData.GUIControls.globalLineWidth*1.5;                
    else
        widthToPlot = watchGUIData.GUIControls.globalLineWidth;                
    end
        
    if (strcmp(watchGUIData.curves{i}.type,'Single'))
        dataToPlot = curveData(watchGUIData.curves{i}.singleIndex,:);                
    else
        dataToPlot = feval(watchGUIData.GUIControls.types.actions{watchGUIData.curves{i}.typeIndex},curveData);                    
    end    
    watchGUIData.curves{i}.AUC = sum(dataToPlot(2:end-1))+(dataToPlot(1)+dataToPlot(end))/2;
    plot(dataToPlot,'Color',colorToPlot,'LineWidth',widthToPlot);
    hold on;
end

set(handles.curveList,'String',curveString);


% --------------------------------------------------------------------
function increaseLineWidthPushTool_ClickedCallback(~, ~, handles)
%Increase the line width on all curves by 30%.

global watchGUIData;
watchGUIData.GUIControls.globalLineWidth = watchGUIData.GUIControls.globalLineWidth*1.3;
refreshCurves(handles);

% --------------------------------------------------------------------
function decreaseLineWidthPushTool_ClickedCallback(~, ~, handles)
%Decrease the line width on all curves by 30%.
global watchGUIData;
watchGUIData.GUIControls.globalLineWidth = watchGUIData.GUIControls.globalLineWidth/1.3;
refreshCurves(handles);


% --------------------------------------------------------------------
function saveAxes_ClickedCallback(~, ~, handles)
% hObject    handle to saveAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global watchGUIData;

fi = figure();
SPN = strsplit(watchGUIData.projects{end}.name,'_');
SPNR = strsplit(watchGUIData.projects{1}.name,'_');
copyobj(handles.curveAxes,fi);
legend([SPNR{2} ' ' SPNR{3} ' ' SPNR{4} ' - ' num2str(watchGUIData.curves{1}.AUC)],[SPN{2} ' ' SPN{3} ' ' SPN{4} ' - ' num2str(watchGUIData.curves{2}.AUC)]);
%xlabel('# of actively selected cells');
%ylabel('Mean of RMSEs');
set(fi,'Units','Inches');
pos1 = get(fi,'Position'); 
pos = pos1;
pos(3) = 8.0;
set(fi,'Position',pos);
set(fi,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
%print(fi,name,'-dpdf','-r0')
saveas(fi,[SPN{2} '_' SPN{3} '_' SPN{4} '.pdf']);
delete(fi);
