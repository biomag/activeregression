function [ TestLabels ] = determineTestSet(fullSize)
% AUTHOR:   Abel Szkalisity
% DATE:     August 25, 2017
% NAME:     determineTestSet
%
%This function is helps us to determine the set which
%we'll use for testing.
%   The TEST SET is the set of objects on which we make our measurements,
%   validating our data. This set is optimal if it contains samples from
%   all types of data, but it can be implemented in other ways too. (e.g.: choose cells randomly)
%   INPUTS:
%       fullSize is the size of the labeling pool, the maximum value of the
%       indeces. The determineTestSet function choose cells from 1 to
%       fullSzie.
%
%   portion variable: this number between ]0,1[ determines the ratio beteen the
%   TEST SET and the FULL SET.
%   More than one implementation could be done: to choose from these adjust
%   the testSetType variable.
%       Values:     1: RandomChoosing 
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

portion = 1/3;
testSetType = 1;

testSize = floor(fullSize*portion);

TestLabels = zeros(1,testSize);

switch testSetType
    case 1
        for i=1:testSize
            nextRand = ceil(rand*fullSize);
            while ismember(nextRand, TestLabels)                
                nextRand = ceil(rand*fullSize);
            end
            TestLabels(i) = nextRand;            
        end        
end


end

