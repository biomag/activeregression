%script to see how independent the predictive variance from the actual
%positions of the cells.
%beforehand you should initialize an X which will be the feature vector,
%and olso Y(1,:) which we do not modify.
%
% with this test we wish to test whether the predictive variance is really
% independent from the location of that cell.

% Procedure: 
%   1. Make some random inputs and store it to X. The columns refers to
%   observations.
%   2. Make some Y variable which will be one of the targets. The second
%   target will be random.
%   3. set i to one.
%   4. run this script.
%   On the plot you can see how the variances changed by different random
%   targets. (every line refers to a new test case, on X you can see the
%   index of the cell, on Y the variance observed on the 2nd coordinate.
%   (The first is not interesting as it is always the same))

clear model o v va

while (i<30);

Y = sum(X);
Y(2,:) = rand(1,size(X,2));

model = TrainGPML(X,Y);
[o,v] = PredictGPML(model,X);
va(i,:) = diag(v);

%we plot only the variances corresponding to the second coordinate as that
%is the one which we choose randomly.
plot(va(i,size(X,2)+1:end)./max(va(i,size(X,2)+1:end)));
hold on;
corrmatrix = corrcoef(va');
i=i+1;

end

