% Script for testing new implementations.

CD = CommonHandlesData(CommonHandles,1);
CL = CommonHandlesLabeling(CommonHandles,1);
for i=1:200
    CD = CD.label(i,CL.label(i));
end

PS = PredictorStore();
pred = GPMLPredictor();

for i=0:9
[inputs, targets] = CD.getLabels([i*20+1:i*20+20]);
pred = pred.train(inputs, targets);
PS = PS.addPredictor([i*20+1:i*20+20],pred);
end