% AUTHOR:	Abel Szkalisity
% DATE: 	December 24, 2015
% NAME: 	runTests
%   
% script to fasten up runs.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
%{

%AC = ALController('GPML',2,ACD,'OverAllUncertaintySampling',{},10,100);
%AC.runExperiment(25);

%timestamp = clock;  
%try
%    movefile(['..' filesep 'lastExperimentResults'],['..' filesep num2str(timestamp(1)) num2str(timestamp(2),'%02d') num2str(timestamp(3),'%02d') '_OUC_AD_GP']);
%catch
%end
%clear AC;

%%%%%%%%%%%%%%

AC = ALController('WekaRandomForest',1,CommonHandles,1,'QueryByCommittee',{3},2,100);
AC.runExperiment(10);

try
movefile(['..' filesep 'lastExperimentResults'],['..' filesep timeString(3) '_CM3_ST9_RF']);
catch
end
clear AC;

%%%%%%%%%%%%%%
%AC = ALController('GPML',1,CommonHandles,1,'RandomChoice',{},2,100);
%AC.runExperiment(10);

%try
%movefile(['..' filesep 'lastExperimentResults'],['..' filesep timeString(3) '_RAN_ST9_GP']);
%catch
%end
%clear AC;

%%%%%%%%%%%%%%
%AC = ALController('GPML',1,CommonHandles,1,'OverAllUncertaintySampling',{},2,100);
%AC.runExperiment(10);
%try
%movefile(['..' filesep 'lastExperimentResults'],['..' filesep timeString(3) '_OUC_ST9_GP']);
%catch
%end
%clear AC;

%%%%%%%%%%%%%%
%AC = ALController('GPML',1,CommonHandles,1,'GreatestDistance',{},2,100);
%AC.runExperiment(10);
%try
%movefile(['..' filesep 'lastExperimentResults'],['..' filesep timeString(3) '_GD_ST9_GP']);
%catch
%end

%clear AC;

%}
% *************************************** %
%     RUN TESTS FOR BENCHMARKING DATA       
% *************************************** %
trainingSetSizes = 10:10:90;
nofSamples = 10;

predictorName = 'GPML';
AC = ALController(predictorName,1,CommonHandles,1,'RandomChoice',{},10,12);
AC.plots.closeall;
eval([predictorName ' = AC.benchmarkData(trainingSetSizes,nofSamples);']);
save(['..\BenchMarkData\' predictorName '.mat'],predictorName,'AC');
clear AC;

predictorName = 'WekaSMO';
AC = ALController(predictorName,1,CommonHandles,1,'RandomChoice',{},10,12);
AC.plots.closeall;
eval([predictorName ' = AC.benchmarkData(trainingSetSizes,nofSamples);']);
save(['..\BenchMarkData\' predictorName '.mat'],predictorName,'AC');
clear AC;
