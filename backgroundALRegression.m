function newData = backgroundALRegression(CommonHandles)
% AUTHOR:   Abel Szkalisity
% DATE:     Aug 10, 2016
% NAME:     backgroundALRegression
%
% To make a wrapper function which makes possible to implement active
% regression similar to active classification. It needs CommonHandles and
% the selected class the name of the class on which the active regresion works.
% Moreover this function is responsible for providing enough initial cell
% within the selected class. Here we note that Active Regression algorithms
% are giving back the total ranking order from which we can select out the
% required amount of data or extending it with zeros if there weren't
% enough cell in the sampling pool. This function must also convert back to
% newData format the simple indexing used in active regression.
%
% NOTES: Before using you should ensure the following things:
%       - The CommonHandles.ActiveRegression.selectedClass is a valid
%       regression class
%       - The valid reg class has a predictor field. (this can be reached by training a model on the selected regression field.)
%
% INPUT:
%  CommonHandles    The big structure, data source of all ACC related
%                   programs. (The selected class' name is stored in
%                   CommonHandles.ActiveRegression.selectedClass)
%
% OUTPUT:
%   newData         A structure with 3 fields: plate, image, cellNumber.
%                   Every field is a cellarray the corresponding entries
%                   stores data for a proposed cell.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    
%constant amount of proposals given back every time.
nofProposedCells = 50;
%constant amount of initial cells to avoid training with too small data
nofInitialCells = 5;

try
    %First of all reinit SAC, this might be required in case of bg
    %processing
    sacInit(CommonHandles.SALT.folderName);
    warning('off','MATLAB:Java:DuplicateClass');
    javaaddpath(fullfile(CommonHandles.ACCPATH(1:end-3),'LIBS','mulan','Clus.jar'));
    javaaddpath(fullfile(CommonHandles.ACCPATH(1:end-3),'LIBS','mulan','mulan.jar'));    
    warning('on','MATLAB:Java:DuplicateClass');
    
    %select the index of the selected regression class from its name.
    %NOTE: its not better solution to store the index instead of the name, as
    %the classes might be deleted.
    for i=1:length(CommonHandles.Classes)
        if strcmp(CommonHandles.ActiveRegression.selectedClass,CommonHandles.Classes{i}.Name)
            selectedRegressionClass = i;
            break;        
        end
    end
    
    if CommonHandles.LabeledInstances(selectedRegressionClass)<nofInitialCells
        baseException = MException('MYEXC:notenough',['Sorry you need at least ' num2str(nofInitialCells) ' cells to use active regression.']);
        throw(baseException);
    end

    %train a classifier to select only elements to regression proposal which
    %are classified to the selected class.
    if length(CommonHandles.Classes)>1
        CommonHandles = trainClassifier(CommonHandles);
        [ out, ~, CommonHandles ] = predictClassifier( CommonHandles.AllFeatures , CommonHandles);
    elseif length(CommonHandles.Classes) == 1
        out = ones(1,size(CommonHandles.AllFeatures,1));
    end
    
    if ~exist('selectedRegressionClass','var')
        errordlg('Attempt to use active regression on deleted class.');
        newData.plate = zeros(1,nofProposedCells);
        newData.image = cell(1,nofProposedCells);
        newData.cellNumber = zeros(1,nofProposedCells);
        return;
    end
    data = ACCData(CommonHandles,selectedRegressionClass, out);
    trainingSet = data.realLabels;

    pS = PredictorStore();

    [ranking]= CommonHandles.ALRegression.nextCell(CommonHandles.RegressionPlane{selectedRegressionClass}.Predictor,pS,data,trainingSet,[]);

    %choosyByRules throws an error if there is not enough cell to fulfill the request
    %we choose a cell which is not in the training set. (the test set is empty)
    newSamples = chooseByRules(ranking, trainingSet, [],nofProposedCells);
    %if the chooseByRules did not throw an exc, then there is
    %nofProposedCells within newSamples.
    [newData.image, newData.cellNumber, newData.plate] = data.map2ACC(newSamples(1:nofProposedCells));

catch e %catches both error cases and handles it in the same way.
    if strcmp(e.identifier,'MYEXC:notenough') || strcmp(e.identifier,'MYEXC:nomore')
        disp(getReport(e));
        %in the error case we give back zeros which indicates for
        %activeLearningStart there there must be a random selection
        newData.image = cell(1,nofProposedCells);
        newData.cellNumber = zeros(1,nofProposedCells);
        newData.plate = zeros(1,nofProposedCells);
    else                
        rethrow(e);
    end
end

end

