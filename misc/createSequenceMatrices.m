%This scripts creates csv files from the same cell-instances, i.e. matrix
%of the whole trajectory. Each row of the matrix refers to one time-point,
%while the columns are the different dimensions (multi-dimensional
%measurements).

%% Input vars
baseDir = 'Y:\beleon\ACC_Project\MitoCheck';
tgtBase = 'D:\szkabel\MitoCheck';

srcDir = fullfile(baseDir,'mitoMerged_3Chan');
trackFile = fullfile(baseDir,'mitoMerged_3Chan\Trackings\20200331_2138\20200331_2138_track.csv');   
tgtDir = fullfile(tgtBase,'sequenceMatrices');
tgtDirNorm = fullfile(tgtBase,'sequenceMatricesNormalized');
metaDataFolder = 'anal2v4_ez3_ez7';

%% Calc matrices
if ~isfolder(tgtDir), mkdir(tgtDir); end
if ~isfolder(tgtDirNorm), mkdir(tgtDirNorm); end

trackTable = readtable(trackFile);

uniTracks = unique(trackTable.TrackID);

sequenceMatrix = cell(length(uniTracks),1);

for i=1:length(uniTracks)
    currTrackID = uniTracks(i);
    relevantTable = trackTable(trackTable.TrackID == currTrackID,:);
    sequenceLength = size(relevantTable,1);
    sequenceMatrix{i} = cell(sequenceLength,1);
    for j=1:sequenceLength
        [~,imgNameExEx,~] = fileparts(relevantTable.ImageName{j});
        fileName = fullfile(srcDir,relevantTable.Plate{j},metaDataFolder,[imgNameExEx '.txt']);
        dataM = load(fileName);
        sequenceMatrix{i}{j} = dataM(relevantTable.ObjectID(j),:);
    end
    sequenceMatrix{i} = cell2mat(sequenceMatrix{i});
    
    %Do some pre-processing (remove the locations):
    idxSet = 1:size(sequenceMatrix{i},2);
    
    %Remove location features:
    idxSet = setdiff(idxSet,[1,2]);
    
    %Do the removal:
    sequenceMatrix{i} = sequenceMatrix{i}(:,idxSet);
    
    csvwrite(fullfile(tgtDir,sprintf('Sequence_%03d.csv',i)),sequenceMatrix{i});
    fprintf('%d/%d ready\n',i,length(uniTracks));
end

%Load one of the feature names
inF = fopen(fullfile(srcDir,trackTable.Plate{1},metaDataFolder,'featureNames.acc'),'r');
tline = fgetl(inF); featureNames = [];
while ischar(tline), featureNames{end+1} = tline; tline=fgetl(inF); end
fclose(inF);
featureNames(1:2) = []; %Remove locations

%Write out to tgtDir
outF = fopen(fullfile(tgtDir,'featureNames.acc'),'w');
for i=1:length(featureNames), fprintf(outF,'%s\n',featureNames{i}); end
fclose(outF);


%Do a normalization (zero mean and unit std) and removal of variables with smaller than 10^-10 std
%(this is negligible standard variation)

mergedSequences = cell2mat(sequenceMatrix);

colStd = std(mergedSequences);
colMean = mean(mergedSequences);

toRemoveIdx = colStd <10^-10;

colStd = colStd(~toRemoveIdx);
colMean = colMean(~toRemoveIdx);
featureNames = featureNames(~toRemoveIdx);

outF = fopen(fullfile(tgtDirNorm,'featureNames.acc'),'w');
for i=1:length(featureNames), fprintf(outF,'%s\n',featureNames{i}); end
fclose(outF);


for i=1:length(uniTracks)    
    normMatrix = (sequenceMatrix{i}(:,~toRemoveIdx)-colMean)./colStd;
    csvwrite(fullfile(tgtDirNorm,sprintf('Sequence_%03d.csv',i)),normMatrix);
    fprintf('%d/%d ready norm\n',i,length(uniTracks));
end