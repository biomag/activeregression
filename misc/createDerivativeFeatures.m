%This scripts creates csv files from the same cell-instances, i.e. matrix
%of the whole trajectory. Each row of the matrix refers to one time-point,
%while the columns are the different dimensions (multi-dimensional
%measurements).

%% Input vars
baseDir = 'G:\Abel\SZBK\Projects\MitoCheck\Data';

srcDir = fullfile(baseDir,'mitoMerged_3Chan');
trackFile = fullfile(baseDir,'mitoMerged_3Chan\RegResult\20191123_2036_Tracks.csv');
tgtDir = fullfile(baseDir,'derivativeFeatures');
featureFileName = 'featureNames.acc';

%% Calc matrices
if ~isfolder(tgtDir), mkdir(tgtDir); end

trackTable = readtable(trackFile);

uniTracks = unique(trackTable.TrackID);

sequenceMatrix = cell(length(uniTracks),1);

for i=1:length(uniTracks)
    currTrackID = uniTracks(i);
    relevantTable = trackTable(trackTable.TrackID == i,:);
    sequenceLength = size(relevantTable,1);
    sequenceMatrix{i} = cell(sequenceLength,1);
    for j=1:sequenceLength
        [~,imgNameExEx,~] = fileparts(relevantTable.ImageName{j});
        fileName = fullfile(srcDir,relevantTable.Plate{j},'anal2',[imgNameExEx '.txt']);
        dataM = load(fileName);
        sequenceMatrix{i}{j} = dataM(relevantTable.ObjectID(j),:);
    end
    %Save cell to local variable here
    seqMat = cell2mat(sequenceMatrix{i});    
    nofFeatures = size(seqMat,2); %NOTE: Location as well!
    
    %Add the derivatives (only for the non-index features)
    extSeqMat = zeros(sequenceLength,(nofFeatures-2)*2);
    extSeqMat(:,1:nofFeatures) = seqMat;
    
    %Do all derivatives in the middle
    for j=3:nofFeatures
        for k=2:sequenceLength-1
            extSeqMat(k,nofFeatures-2+j) = (extSeqMat(k+1,j) - extSeqMat(k-1,j))/2;
        end
    end
    %Handle case for the extremes
    for j=3:nofFeatures        
        extSeqMat(1,nofFeatures-2+j) = (extSeqMat(2,j) - extSeqMat(1,j))/2;
        extSeqMat(sequenceLength,nofFeatures-2+j) = (extSeqMat(sequenceLength,j) - extSeqMat(sequenceLength-1,j))/2;
    end
    
    sequenceMatrix{i} = extSeqMat;
    
    %Create feature names as well
    % -> First load them, from the first plate
    featureFile = fullfile(srcDir,relevantTable.Plate{1},'anal2',featureFileName);
    T = readtable(featureFile,'FileType','text','Delimiter',',','ReadVariableNames',false);    
    extFeatureNames = cell(2*nofFeatures-2,1);
    extFeatureNames(1:nofFeatures) = T.Var1;
    for j=3:nofFeatures, extFeatureNames{nofFeatures+j-2} = ['firstDer_' extFeatureNames{j}]; end
    
    
    %Write out the files in ACC format
    for j=1:sequenceLength
        [~,imgNameExEx,~] = fileparts(relevantTable.ImageName{j});
        fileName = fullfile(tgtDir,relevantTable.Plate{j},'anal2',[imgNameExEx '.txt']);
        if ~isfolder(fullfile(tgtDir,relevantTable.Plate{j},'anal2')), mkdir(fullfile(tgtDir,relevantTable.Plate{j},'anal2')); end
        csvwrite(fileName,sequenceMatrix{i}(j,:));
        if ~exist(fullfile(tgtDir,relevantTable.Plate{j},'anal2',featureFileName),'file')
            writecell(extFeatureNames,fullfile(tgtDir,relevantTable.Plate{j},'anal2',featureFileName),'FileType','text');
        end
    end
                           
        
    fprintf('%d/%d ready\n',i,length(uniTracks));
end